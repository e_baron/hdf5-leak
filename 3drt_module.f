* Time-stamp: <2014/01/16 15:18 baron>
***********************************************************************
* This module defines the data structures and some routines used in 
* 3DRT.
* version 3.0 of 23/Apr/2008 by PHH + EAB
*
* changes
* added DD for PHOENIX/3D
***********************************************************************
c
      module IEEE_prec
c--
c-- use this setup for double precision:
c--
      integer, parameter :: pve_data_type=8             ! size of floating point data in bytes
      real*8, parameter :: prec=1d-13                   ! this is a dummy to select IEEE 754 precisions!
      real(kind(prec)), parameter :: biggest=1d300
      real(kind(prec)), parameter :: smallest=1d-300
      real(kind(prec)), parameter :: one=1.d0,two=2.d0,half=0.5d0
      real(kind(prec)), parameter :: zero=0.0d0
      real(kind(prec)), parameter :: max_error=1d-12
c
c--
c-- use this setup for single precision:
c--
c      integer, parameter :: pve_data_type=4             ! size of floating point data in bytes
c      real*4, parameter :: prec=1e-5                    ! this is a dummy to select IEEE 754 precisions!
c      real(kind(prec)), parameter :: biggest=1e30
c      real(kind(prec)), parameter :: smallest=1e-30
c      real(kind(prec)), parameter :: one=1.e0,two=2.e0,half=0.5e0
c      real(kind(prec)), parameter :: zero=0.0e0
c      real(kind(prec)), parameter :: max_error=1e-7
c
      end module IEEE_prec
c--
c-- this version is more modern but the constants are not 
c-- optimal
c--
c
coff      module IEEE_prec
coffc--
coffc-- use this setup for double precision:
coffc--
coff      integer, parameter :: pve_data_type=8             ! size of floating point data in bytes
coffc--
coffc-- use this setup for single precision:
coffc--
coffc      integer, parameter :: pve_data_type=4             ! size of floating point data in bytes
coffc
coff      real (kind=pve_data_type), parameter :: ! this is a dummy to select IEEE 754 precisions!
coff     &   prec=0._pve_data_type
coff      real(kind(prec)), parameter :: biggest=huge(0._pve_data_type)
coff      real(kind(prec)), parameter :: smallest=tiny(0._pve_data_type)
coff      real(kind(prec)), parameter :: one=1._pve_data_type
coff      real(kind(prec)), parameter :: two=2._pve_data_type
coff      real(kind(prec)), parameter :: half=0.5_pve_data_type
coff      real(kind(prec)), parameter :: zero=0._pve_data_type
coff      real(kind(prec)), parameter :: max_error=epsilon(0._pve_data_type)
coffc
coff      end module IEEE_prec
c
c
c
      module structure
      use IEEE_prec
      implicit none
c
      integer, parameter :: layer=64
c
      real(kind(prec)) :: BETA(layer)
      real(kind(prec)) :: DBETDR(layer)
      real(kind(prec)) :: CHIARR(layer),chi_log(layer)
      real(kind(prec)) :: CHIRES(layer)
      real(kind(prec)) :: GAMMA(layer)
      real(kind(prec)) :: CAP(layer),cappa_log(layer)
      real(kind(prec)) :: PLANCK(layer),pl_log(layer)
      real(kind(prec)) :: R(layer),r_log(layer)
      real(kind(prec)) :: SIG(layer),sigma_log(layer)
      real(kind(prec)) :: TEMP(layer)
      real(kind(prec)) :: Pgas(layer),rho(layer)                 ! fake data for 3DRT
      real(kind(prec)) :: mu_H_fake=0.5d0,kappa_rho_fake=0.25d0  ! fake data for 3DRT
      real(kind(prec)) :: TSTD(layer)
      real(kind(prec)) :: VEL(layer)
c
      logical :: cylinder_mode = .false.
      real(kind(prec)) :: cyl_top = 0.d0
c
      end module structure
c
c
c
      module dddrt_module
      use IEEE_prec
      implicit none
      include 'param.inc'
      integer np2
      private layer, nvemx, nionmx, nmolmx, npdmx, nelmx, nopmax, nopc,
     & nrest, ntab, nkmax, nrall, nsrt, nvsrt, nthdim, npgdim, mist,
     & mxst, maxel, nbmax, nbmol, maxpol, muemax, lmax, np, iw, np2,
     & maxcm, maxobs, maxapp, ntl, nlimax,layerend,ndust_wave,
     & tma,nvemx1,nvionmx,ndust,npoints,ndust_opacities,z_max
c--
c-- parameter:
c
      logical :: use_hdf5_3D=.false.                                      ! do not use HDF5 I/O as default, .true. requires HDF5 compilation
      logical :: use_hdf5_collective=.false.                              ! core dump/suck I/O use collective calls (not by default).
      logical :: use_IOnodes=.false.                                      ! do not use dedicated I/O nodes as default
      logical :: use_Lstar_redo=.false.                                   ! true: recalculate Lstar if N_PPM changes by more than 10%
      logical :: use_Bezier=.false.                                       ! do not use the new Bezier interpolation by default, yet.
      logical :: use_Bezier_boost=.false.                                 ! after RT3D finished, it calls FS3D with use_bezier=.true. for a Bezier Boost.
      logical :: use_lambda_iteration=.false.                             ! set to true to use straight lambda iteration for testing
      logical :: use_q_storage=.false.                                    ! set to true to store partition functions in EOS data as single precision for line opacities
      logical :: use_smp=.false.                                          ! set to true to use the use_smp test statements -=ato
      integer :: smp_maxthreads=1                                         ! set to wanted value for OpenMP threads
      logical :: dbg_3drt=.false.
      logical :: verbose=.false.
      logical :: verbose_2lvl=.false.
      logical :: verbose_FS3D=.false.
      logical :: verbose_RT3D=.false.
      logical :: dump_chars=.false.
      logical :: large_IO=.true.                                         ! set to false if I/O libs cause read trouble.
      real(kind(prec))  :: taulin=1d-3                                   ! dtau < taulin -> linear coefficients
      real(kind(prec))  :: PPM_max_step=1d2                              ! PPM_max_step*S(local) < S(next) -> linear
      real(kind(prec))  :: PPM_min_step=-1.00d0                          ! PPM_min_step = min. rel. change in S for PPM method (uses linear if S is nearly constant)
      integer :: OS_Solver=4                                             ! 0: Diagonal, 1: Rapido, 2: Lapack, 3: SuperLU, 4: Jordan, 5: Gauss-Seidel
      logical :: save_intensities=.false.                                ! set to true to save the intensities for each angle 
      logical :: compute_fluxes=.false.                                  ! set to true to compute and save the flux vectors at each point
      logical :: use_mu=.true.                                           ! set to true to use \int d\mu rather than \int \sin(\theta) d\theta
      logical :: wide_Lstar=.true.                                       ! set to true to use dynamic and large Lstar
      logical :: Jordan_mpi=.false.                                      ! set to true to use wide Jordan MPI solver
      logical :: save_Lstar=.false.                                      ! set to true to write/read Lstar (new format) in write_pve & read_pve
      logical :: read_Lstar=.false.                                      ! set by read_pve if an Lstar was read successfully, used in 3D.f and 2lvl.f
      logical :: save_spectrum=.false.                                   ! set to true to save spectrum (.spectrum.dat) in 2lvl.f
      character*1024 :: save_dir = "./"                                  ! used to save spectrum etc. directly to the final target directory, saves time. A lot of it!
      logical :: Q_and_D=.false.                                         ! set to true to use char_norm = 1
      logical :: variable_step_size=.false.                              ! set to true to allow char path steps to vary
      logical :: use_spherical_grid=.false.                              ! set to true to use 3D spherical coordinates
      logical :: use_zmap=.false.                                        ! set to true to use 3D Cartesian coordinates with a z-map (PBCs, mostly)
c
      logical :: aniso_mode=.false.                                      ! this is for the anisotropic (Euler) mode. The old one is being removed.
      logical :: aniso_mode_2lvl=.false.                                 ! this is for the anisotropic (Euler) mode. The old one is being removed.
c- leons edit, vars nedded for eulerian mode 
      logical :: eulerian_mode=.false.
      logical :: eulerian_mode_2lvl=.false.
      logical :: euler_save_files = .false.
      logical :: euler_test_lstar = .false.
      logical :: euler_test_lstarBAR = .false.
      integer :: my_euler_angles
      real(kind(prec)) :: max_v_euler
      real(kind(prec)) :: euler_read_timer
      logical :: euler_show_thermal_velocities = .false.
      real(kind(prec)) :: thermal_line_data(2)
      real(kind(prec)) :: thermal_molec_data(2)
c- leons edit
      logical :: use_cylindrical_grid=.false.                            ! set to true to use 3D cylindrical coordinates
      logical :: homology=.false.                                        ! set to true to allow for homologous flows (only works in spherical mode)
      logical :: new_s=.false.                                           ! set to true to test new grid routines (only works in spherical moving mode)
      logical :: write_intensities=.false.                               ! set to true to write out surface I (only works in spherical moving mode)
      logical :: nm3d=.false.                                            ! set to true for arbitrary velocity mode (very preliminary)
      integer :: backtrack_limit=16                                      ! limit the number of steps to back track in cylindrical coords for Lstar, 16 seems OK....
      logical :: save_boundary_voxels=.false.                            ! only write the outer voxels for spectrum PVE file to save space
      logical :: FastTrack = .false.                                     ! use the SMP enabled tracker (sometimes faster, uses more ram)
      logical :: use_2pass = .false.                                     ! use the SMP enabled 2pass spherical coord tracker 
      logical :: compute_DeltaRad = .false.                              ! set to .true. to build DeltaErad for time dependent modes
      logical :: save_cmf_restart = .false.                              ! set to .true. to save CMF intensities for later restart (large files!)
      logical :: diffusion_inner_BC = .false.                            ! set to .true. to use diffusive inner BCs in Cartesian PBC mode
      logical :: FS3D_only = .false.                                     ! this is to select formal solutions only insolve_3DRT (testing)
c--
c-- these are for PHOENIX/3D settings:
c--
      logical :: use_core_dumper = .true.                                ! global switch to use the core dumpers in phx/3D: EOS, ni/bi, radrates
      logical :: use_low_impact_dumper = .true.                          ! switch to selected full (all procs) or low impact (1 wl cluster only) dumpers in MPI
      logical :: use_radrat_dumper = .true.                              ! dump radiative rates
      logical :: use_partial_bini_dumper = .true.                        ! dump partial results from rate equations solver
c--
c-- select periodic BCs (PBCs) in x and/or y:
c--
      logical :: x_pbc=.false.                                           ! true if BCs in x are periodic
      logical :: y_pbc=.false.                                           ! true if BCs in y are periodic
      logical :: z_pbc=.false.                                           ! true if BCs in z are periodic (used for phi in spherical and cylindrical coords)
      integer :: n_step_max=16                                            ! number of steps per z-cell that are 'allowed'
c--
c-- select reflective BCs (PBCs) in x and/or y:
c--
      logical :: x_rbc=.false.                                           ! true if BCs in x are reflective
      logical :: y_rbc=.false.                                           ! true if BCs in y are reflective
      logical :: z_rbc=.false.                                           ! true if BCs in z are reflective
c--
c-- select disk boundary conditions
c--
      logical :: diskgeo=.false.
      logical :: dbc=.false.
      real(kind(prec)) :: diskstar=0.d0
      integer :: interpts=10
      integer :: incexp=1                                                ! exponent needed for steps in r mapping
c--
c-- selector for LC_solvers:
c--
      integer :: LC_solver_method=0                                      ! codes: 0=LC_solver_wrkr
c--
c-- write pve and wl output in f95 unformatted files:
c--
      logical :: use_binary_files=.false.
c--
c-- write angle/wl depend opac files in binary form (eulerian mode)
c--
      logical :: use_binary_files_euler=.true.
c                                                                        !  2=LC_Lstar_general_wrkr
c-- for sunspot models:
c-- ABE: 13/April/2011
c--
      logical :: sunspot=.false.                                         ! set true to use a sunspot, only available in 2lvl pbc mode
      integer :: spotmode=1                                              ! select sunspot form, see generate_spot for available modes
      real(kind(prec)) :: r_spot=0                                       ! sunspot radius
      real(kind(prec)) :: t_spot=8.0d3                                   ! effective Temperature of sunspot
      real(kind(prec)) :: kappa_spot=0.0                                 ! opacity multiplier in spot center
      real(kind(prec)) :: dz_spot=0                                      ! vertical expansion
      real(kind(prec)) :: z_spot=0                                       ! lower boundary of sunspot (spot from z to z+dz)
      real(kind(prec)) :: scaledepth=0                                   ! temperature scaledepth for gaussian whirl model
c
c--
c-- for phx3D physical setup load to main
c-- ABE: 20/July/2011
c--
      logical   :: loadphysics=.false.                                   ! restart with phoenix/3D temperature structure and opacities
      real(kind(prec)), pointer :: lineopac(:,:,:)=>NULL()               ! array to contain line data from phoenix/3D pve file 
      real(kind(prec)), pointer :: contopac(:,:,:)=>NULL()               ! array to contain cont data from phoenix/3D pve file 
      real(kind(prec)), pointer :: conttemp(:,:,:)=>NULL()               ! array to contain temperature data from phoenix/3D pve file 
c
c--
c-- for eos3D species writeout
c-- ABE: 14/Dec/2011
c--
      integer :: prtspecid=-1                                             ! id of species for printout (replace with list of names soon(TM))
c--
c-- for the irradiation:
c--
      logical :: irradiated=.false.                                      ! set to true for irradiated mode
      integer :: z_irrad_direction=0                                     ! +1: irradiation in +z direction, from "bottom"
c                                                                        !      -1: irradiation in -z direction, from "top"
c                                                                        !       0: error!
      real(kind(prec)) :: irrad_str=0.0                                  ! multiplier for iBulb to set irradiation strength
      integer :: irrad_hit=0                                             ! counter for irradiation angles
      real(kind(prec)) :: mu_irrad=0.0,phi_irrad=0.0                     ! (mu,phi) coordinates of incoming radiation (spherical coordinates)
      real(kind(prec)) :: irrad_intensity=0.d0                           ! irradiating intensity (assumed to be constant over irradiated surface!)
      real(kind(prec)) :: dilution_factor=0.d0                           ! overall delution factor (is used in PHOENIX/3D to set incoming intensities)
      real(kind(prec)) :: T_irrad=0.d0                                   ! irradiaton radiation temperature for BB mode 
c--
c-- parameters for the iterative solvers:
c--
      integer :: sigma_Rap=7
      integer :: max_iter_Rap=100
      real(kind(prec))  :: max_error_Rap=max_error ! precision set to whatever is approriate for the var's size
c--
c-- parameter to control Ng acceleration:
c--
      logical :: use_ng=.true.
      integer :: nng=4
c
      integer :: ix_max_change
      integer :: iy_max_change
      integer :: iz_max_change
c
      integer :: ix_test,iy_test,iz_test  ! this is for testing of the Lstar
      integer :: iwl_test=-666  ! this is used in the NM case for Lstar testing
      integer :: rhs_bad=0 ! used in solve for I to count failures
c--
c-- indicates that a new Lstar is available (decompose matrices etc)
c--
      logical :: new_Lstar=.true.
c--
c-- this variables hold the max. and min. intensity at the outer
c-- boundary. They are updated only if save_intensities = .true.
c-- and work only with the 'tracker' LC solvers (regular and PBC).
c-- this is very useful for the imaager
c--
      real(kind(prec)) :: I_out_max=-biggest,I_out_min=biggest
c--
c-- counters etc.
c--
      integer*4 :: n_phoenix3D_iters = 0                                 ! global numbers of iterations in NLTE/3D
      integer*8 :: real_alloced = 0                                      ! total 'size' of all allocated 'real(kind(prec))'
      integer*8 :: w16bit_alloced = 0                                    ! total 'size' of all allocated 16bit numbers'
      integer*8 :: n_PLM = 0                                             ! counts points with linear source function integration
      integer*8 :: n_PPM = 0                                             ! counts points with parabolic source function integration
      real*8    :: FT_timer_setup = 0.d0                                 ! internal timers for the FastTrack routines 
      real*8    :: FT_timer_compute = 0.d0                               ! internal timers for the FastTrack routines 
      real*8    :: FT_timer_moments = 0.d0                               ! internal timers for the FastTrack routines 
c
      integer :: n_Lstar_pts = 0
      integer :: n_grid_points = 0
c--
c-- the (potential) light bulb at the some co-ordinates
c-- -1.0d0 means bulb is off
c--
      real(kind(prec)) :: iBulb = -1.d0
      integer :: ix_bulb=0,iy_bulb=0,iz_bulb=0                           ! default is bulb at grid center!
c--
c-- these are declarations for the MPI mode:
c-- design: a "WL cluster" works on a single wavelength, using
c-- "MPI_procs_per_cluster" processes. 
c--
      integer :: MyMPI_3DRT_GROUP_FS  = -1                               ! group for FS tasks
      integer :: MyMPI_3DRT_GROUP_WL  = -1                               ! group for WL clusters
      integer :: MyMPI_3DRT_GROUP_RMA = -1                               ! group for WL clusters
      integer :: MyMPI_3DRT_COMM_FS   = -1                               ! internal communicator for FS tasks
      integer :: MyMPI_3DRT_COMM_WL   = -1                               ! intra communicator for WL master tasks
      integer :: MyMPI_3DRT_COMM_RMA  = -1                               ! MPI communicator for group of RT tasks doing RMA (on a node)
      integer, allocatable :: MPI_GRP_3DRT_WRKRS(:)                      ! group for intra WL tasks
      integer, allocatable :: MPI_COMM_3DRT_WRKRS(:)                     ! intra communicator for WL tasks
      integer, allocatable :: n_DD_slots_used(:)                         ! domain decomposition: How many voxels does each DD process fill? Used in I/O etc.
c--
c-- use this to find how big to set I_old and I_new (for moving case)      
c--
      integer :: FS_WL_rank                                              ! WL rank of the master of my FS cluster
      integer :: WL_rank                                                 ! rank of this process in WL group
      integer :: FS_rank                                                 ! rank of this process in its FS cluster
      integer :: WL_size                                                 ! number of WL clusters
      integer :: FS_size                                                 ! number of processes per FS cluster
      integer :: RMA_rank                                                ! rank inside the local node RMA RT set
      integer :: RMA_size                                                ! number of tasks in local RMA RT (within node)
      integer :: WL_comm_rank                                            ! rank of this process in WL group
      integer :: FS_comm_rank                                            ! rank of this process in its FS cluster
      integer :: WL_comm_size                                            ! number of WL clusters
      integer :: FS_comm_size                                            ! number of processes per FS cluster
      integer :: RMA_comm_rank                                           ! rank inside the local node RMA RT set
      integer :: RMA_comm_size                                           ! number of tasks in local RMA RT (within node)
      integer :: n_angles = -1                                           ! number of solid angles for a process (data allocation)
      integer :: My_MPI_i_angle                                          ! counter into I_old I_new array          
      integer :: cmf_restart_unit=39                                     ! unit number for aync I/O CMF intensities for restart
c--
c-- MPI declarations for the PHOENIX domain decomposition 
c-- working together with the 3DRT solver:
c--
      integer :: nRT_per_wl_cluster                  ! number of RT tasks per node
      integer :: ID_in_node                          ! rank inside its node
      integer :: size_of_node                        ! number of task inside node, for checking
      integer :: opac_set_comm_rank=0                ! rank inside the local node opacity set
      integer :: opac_set_comm_size=1                ! number of tasks in opac_set (within node)
      integer :: MyMPI_GROUP_NODE                    ! MPI group for a node
      integer :: MyMPI_COMM_NODE                     ! MPI communicator inside a node
      integer :: MyMPI_COMM_NODE_OPAC_SET            ! MPI communicator for group of opacity tasks for single RT task (master)
      integer :: DD_all_comm_rank=0                  ! rank in global domain decomposition set for single wavelength cluster
      integer :: DD_all_comm_size=1                  ! number of tasks in global domain decomposition for single wavelength cluster
      integer :: MyMPI_COMM_ALL_DD                   ! MPI communicator for group of all domain decomposition tasks within a wavelength cluster
c
      integer :: MyMPI_COMM_WL_CLUSTER,MyMPI_GROUP_WL_CLUSTER            ! group/communicator for my wavelength cluster
      integer :: ID_in_wl_cluster,size_of_wl_cluster                     ! ID and size within my wavelength cluster
      integer :: IO_comm_rank,IO_comm_size                               ! ID and size within my IO cluster, if used
      integer :: my_wl_cluster                                           ! the number of the wavelength cluster this process belongs to
      integer :: procs_per_wl_cluster                                    ! number of cores per wavelength cluster.
c
      integer :: DD_WL_comm_rank=0                 ! rank in comm of all wl tasks within a domain decomposition domain
      integer :: DD_WL_comm_size=1                 ! number of tasks in comm of all wl tasks within a domain decomposition domain
      integer :: MyMPI_COMM_DD_WL                  ! MPI communicator for group of all wl tasks within a domain decomposition domain
      integer :: MyMPI_GROUP_DD_WL                 ! MPI group of all wl tasks within a domain decomposition domain
      integer :: MyMPI_COMM_IO                     ! MPI communicator for I/O nodes
      integer :: MyMPI_GROUP_IO                    ! MPI group of I/O nodes
c--
c-- MPI tag and command definitions for the I/O node design.
c-- these tags are used to send commands to the
c-- I/O nodes.
c--
      integer, parameter :: IO_COMMAND=10000                        ! the tag for sending commands to I/O processes
      integer, parameter :: IO_CMD_EOS_DUMP           = 10000       ! receive EOS dump data (implies write)
      integer, parameter :: IO_CMD_EOS_SUCK           = 10000+01    ! send EOS sucked data (implies read)
      integer, parameter :: IO_CMD_END                = 10000+02    ! stop, it calls stop_exit(0,...)
      integer, parameter :: IO_CMD_EOS_CLOSE          = 10000+03    ! close EOS dump data file
      integer, parameter :: IO_CMD_BINI_DUMP          = 10000+04    ! receive bini dump data (implies write)
      integer, parameter :: IO_CMD_BINI_SUCK          = 10000+05    ! send bini sucked data (implies read)
      integer, parameter :: IO_CMD_BINI_CLOSE         = 10000+06    ! close bini dump data file
      integer, parameter :: IO_CMD_RADRAT_DUMP        = 10000+07    ! receive radrates dump data (implies write)
      integer, parameter :: IO_CMD_RADRAT_SUCK        = 10000+08    ! send radrates sucked data (implies read)
      integer, parameter :: IO_CMD_RADRAT_CLOSE       = 10000+09    ! close radrates dump data file
      integer, parameter :: IO_CMD_BINI_PARTIAL_DUMP  = 10000+10    ! receive bini partial dump data (implies write)
c--
c-- we need to have a few MPI requests globally available
c-- to be able to wait for MPI Isend's for the I/O processes
c--
      integer :: req_radrates
c--
c-- an buffer that may be used for MPI:
c--
      integer :: MyMPI_buffer_size=64*1014*1024    ! default buffer size = 64MB
      character, allocatable :: MyMPI_buffer(:) 
c--
c-- data type and helpers needed for MPI/IO based output
c-- of CMF intensities
c--
      type I_output
      sequence
      integer :: itc,ipc,it,ip
      real (kind=kind(prec)) :: wl,intensity
      end type i_output
c
      integer :: MPI_I_Type,intensities_file
c--
c-- for use with ifort array assigment bug
c--
      integer :: jx,jy,jz
c--
c-- for the 2D mode:
c-- location of the center of symmetry in the grid:
c--
      integer :: ix_center=0,iy_center=0,iz_center=0
c--
c-- testing the phoenix_3D interfaces:
c--
      logical :: testing_3D=.false.
      logical :: testing_NLTE3D=.false.
      real*8, allocatable :: t_test_3d(:),pg_test_3d(:)
c--
c-- for LTE load balance info
c--
      integer (kind=8),allocatable :: lin_load_balance(:)
c--
c-- definition for the characteristics:
c--
      integer :: alloc_char_counter=0                                    ! used to debug char. memory leaks!
      integer :: ID_counter=0                                            ! used to debug char. memory leaks!
      integer, parameter :: char_size=2+1+3+6+2+3                        ! estimated size in doubles of char structure
c
      type characteristic
       type(characteristic), pointer :: next_char  => NULL()             ! pointer to next char of current voxel's list
       type(characteristic), pointer :: prev_char  => NULL()             ! pointer to prev char of current voxel's list
       real(kind(prec))              :: intensity                        ! intensity at the current point (updated continuously)
       real(kind(prec))              :: x,y,z                            ! current space coordinates 
       real(kind(prec))              :: prev(3,0:1)=0.d0                 ! list of previous spatial coordinates (1:3,length)
       real(kind(prec))              :: Lstar_parts(0:1)=0.d0            ! used to compute Lstar
       integer :: prev_voxel                                             ! index to previous voxel to traverse list
c                                                                        ! prev_voxel+1 is current voxel
       integer :: ID                                                     ! ID counter for debug
coff       real(kind(prec)), pointer     :: Lstar_wide(:)   => NULL()    ! used to compute Lstar
coff       integer :: length                                             ! length of the characteristic
      end type
c--
c-- used to point to linked lists of chars:
c--
      type charp
       type(characteristic), pointer :: char => NULL()                   ! pointer to a characteristic
      end type
c--
c-- this is used for characteristics in SMP mode 
c--
      type char_data
       integer, pointer            :: ixt(:) => NULL()
       integer, pointer            :: iyt(:) => NULL()
       integer, pointer            :: izt(:) => NULL()                   ! indices of target voxel in local coordinate system (Cart,Sph,Cyl)
       real(kind(prec)), pointer :: sminus(:) => NULL()                  ! distance to the starting point 
      end type
c--
c-- structure to hold SMP char data for all angles:
c--
      type char_list
       integer                  :: n_chars=-1                            ! number of char starting points stored
       integer, pointer            :: ixt(:) => NULL()
       integer, pointer            :: iyt(:) => NULL()
       integer, pointer            :: izt(:) => NULL()                   ! indices of target voxel in local coordinate system (Cart,Sph,Cyl)
       real(kind(prec)), pointer :: sminus(:) => NULL()                  ! distance to the starting point 
      end type
c
      integer, parameter :: char_team_size=4+3+2+3                       ! estimated size in doubles of following structure
      type char_team
       real(kind(prec))              :: intensity=0.d0                   ! intensity at the current point (updated continuously)
       real(kind(prec))              :: x=0.d0,y=0.d0,z=0.d0             ! current space coordinates 
       real(kind(prec))              :: prev(3)=0.d0                     ! list of previous spatial coordinates (1:3,length)
       real(kind(prec))              :: Lstar_parts(0:1)=0.d0            ! used to compute Lstar
       real(kind(prec))              :: x0=0.d0,y0=0.d0,z0=0.d0          ! point of origin
       real(kind(prec))              :: s0=0.d0,s=0.0d0                  ! step to reach (x,y,z) from (x0,y0,z0)
       integer                       :: ir,it,ip                         ! cell index of current point (used in spherical)
       integer                       :: ir0,it0,ip0                      ! cell index of starting point (used in spherical)
      end type
c
      type char_SOA
       real(kind(prec)), pointer    :: intensity(:)      => NULL()       ! intensity at the current point (updated continuously)
       real(kind(prec)),pointer     :: x(:)              => NULL()
       real(kind(prec)),pointer     :: y(:)              => NULL()
       real(kind(prec)),pointer     :: z(:)              => NULL()       ! current space coordinates 
       real(kind(prec)),pointer     :: x_m(:)            => NULL()       
       real(kind(prec)),pointer     :: y_m(:)            => NULL()       
       real(kind(prec)),pointer     :: z_m(:)            => NULL()       ! list of previous spatial coordinates (1:3,length)
       real(kind(prec)),pointer     :: Lstar_parts(:,:)  => NULL()       ! used to compute Lstar
      end type
c--
c-- data structure for an arbitrarily wide Lstar:
c--
      type Lstar
       real(kind(prec)), pointer :: Lstar(:,:,:) => NULL()               ! components of the Lambda operator
       real(kind(prec)) :: diag_diag = 0.d0                              ! diagonal Lstar for use with NLTE/3D!
      end type
c--
c-- data structure for the EOS data (partial pressures)
c--
      type EOS
       real(kind(prec)), pointer :: number_densities(:) => NULL()        ! the n_i for all species
       real*4,           pointer :: partition_fcts(:) => NULL()          ! (optional) Q's for the species (for line opacities)
       real(kind(prec)), pointer :: T_electron=>NULL()                   ! electron temperature
       real(kind(prec)), pointer :: P_gas=>NULL()                        ! gas pressure
       real(kind(prec)), pointer :: rho=>NULL()                          ! mass density
       real(kind(prec)), pointer :: velocity(:)=>NULL()                  ! velocity field for Euler mode (vx,vy,vz)
      end type
c--
c-- data structure for the NLTE related data (bi, ni, ...)
c--
      type NLTE
c--
c-- occupation numbers etc:
c--
       real(kind(prec)), pointer :: bitrue(:)         ! the (Mihalas) departure coefficients for all level (maxlevel)
       real(kind(prec)), pointer :: ocnlte(:)         ! the NLTE n_i for all level (maxlevel)
       real(kind(prec)), pointer :: ocstar(:)         ! the NLTE n^*_i for all level, this may NOT be needed! (maxlevel)
c--
c-- radiative rates, operators:
c--
       real(kind(prec)), pointer :: Rabs(:)           ! line radiative rates for absorption (0:maxtrnb-1)
       real(kind(prec)), pointer :: Rem(:)            ! line radiative rates for emission (0:maxtrnb-1)
       real(kind(prec)), pointer :: Rabsc(:)          ! continua radiative rates for absorption (0:maxtrnbc-1)
       real(kind(prec)), pointer :: Remc(:)           ! continua radiative rates for emission (0:maxtrnbc-1)
       real(kind(prec)), pointer :: Rstabs(:)         ! line radiative rate operator for absorption (0:maxtrnb-1)??
       real(kind(prec)), pointer :: Rstem(:)          ! line radiative rate operator for emission (0:maxtrnb-1)??
       real(kind(prec)), pointer :: Rstabsc(:)        ! continua radiative rate operator for absorption (0:maxtrnbc-1)??
       real(kind(prec)), pointer :: Rstemc(:)         ! continua radiative rate operator for emission (0:maxtrnbc-1)??
c--
c-- helpers:
c--
       real(kind(prec)), pointer :: etavek(:)         ! helper for the operator construction (maxlevel)
       real(kind(prec)), pointer :: prfem(:)          ! line profiles (CRD) (0:maxtrnb-1)
       real(kind(prec)), pointer :: prfnrm(:)         ! line profile norms (CRD) (0:maxtrnb-1)
      end type
c--
c-- data structure for wavelength dependent parts of NM3D
c--
      type NM3D_type
       type(Lstar), pointer :: Lambda(:,:,:) =>NULL()        ! voxel coordinates (ix,iy,iz)
       real(kind(prec)), pointer :: J(:,:,:) => NULL()       ! NM J
       real(kind(prec)), pointer :: F(:,:,:,:) => NULL()     ! NM flux
       real(kind(prec)), pointer :: J_old(:,:,:) => NULL()   ! NM J_old
       real(kind(prec)), pointer :: J_ng(:,:,:,:) => NULL()  ! NM J_ng
       real(kind(prec)), pointer :: B(:,:,:) => NULL()       ! wavelength dependent Planck function
       real(kind(prec)), pointer :: S(:,:,:) => NULL()       ! wavelength dependent Source function
       real(kind(prec)), pointer :: opac(:,:,:) => NULL()    ! wavelength dependent opacity
       real(kind(prec)), pointer :: cappa(:,:,:) => NULL()   ! wavelength dependent opacity
       real(kind(prec)), pointer :: sigma(:,:,:) => NULL()   ! wavelength dependent opacity
       real(kind(prec)), pointer :: epsilon(:,:,:) => NULL() ! wavelength dependent epsilon
       real(kind(prec)), pointer :: etanlt(:,:,:) => NULL()  ! wavelength dependent eta for NLTE/3D calculations
c--
c-- this section is for (2lvl) NLTE lines:
c--
       real(kind(prec)), pointer :: profile(:,:,:)=>NULL()  ! line profile data 
       real(kind(prec)), pointer :: pnorm(:,:,:)=>NULL()    ! line profile normlization data 
       real(kind(prec)), pointer :: eta_nlte(:,:,:)=>NULL() ! line profile data 
       integer :: nng

      end type
c--
c-- talking to the OpenCL + C routines:
c--
      integer :: selected_device = -1  ! -1 means 'use fortran', >=0 means "use this OpenCL device"
c--
c-- integers must always be in pairs as F90 seems to  
c-- align to 64bit.
c--
      type pve_grid_c
       sequence
       integer :: nx,ny,nz
       integer :: z_start
       integer :: do_lstar=0,n_step_max=8                  ! =1 to compute Lstar, max. number of z-steps in PBC mode. 
       integer :: ntheta,nphi
       integer :: max_char_length,FullInit
       integer :: CL_OS_step_iters=10,C_OS_Step_iters=100  ! used to set OS_step runs in CL and C driver, respectively
       integer :: use_ng=1, use_ng_OSstep=1                ! set to 1 to allow Ng in RT3D overall iterations and OS_step, respectively.
       integer :: nng=10,nng_OSstep=4                      ! intial Ng values to delay Ng in RT3D overall iterations and OS_step, respectively.
       integer :: itacc,itdone                             ! iteration limiter, number of iterations actually done
       real :: max_OSstep_error=1d-7                       ! error limit for OS_step Jordan solver (C driver), default set to single precision
       real :: max_J_overall_error=1d-3                    ! error limit for rt3d_cl iterations, default set to single precision
       real :: taulin=1d-1                                 ! same as in Fortran
       real :: x_step,y_step,z_step
       real :: xs_inv,ys_inv,zs_inv
       real :: theta,phi
       real :: error_reached                               ! the error reached in the iterations
      end type
      type(pve_grid_c),save :: pve_grid_ocl
!
!--
!-- list of points along the characteristic
!-- 
      type tracker_indices
      integer :: ix,iy,iz
      real (kind(prec)) :: udotpsq
      end type tracker_indices
      type local_char_ind
      type (tracker_indices), pointer :: p(:)=>NULL()
      end type local_char_ind
!
!      
c
c--
c-- these are data structures for the physical volume elements (PVEs):
c--
      type pve_data
c     -------------
c--
c-- this is for a regular grid. The centers of the volume
c-- elements (voxels) are at regular intervals, the physical quantities
c-- are averages over the volume elements. Note that (0,0,0)
c-- always is one of the voxel centers!
c--
       type(pve_data), pointer :: link => NULL()                         ! pointer to another PVE grid
c--
c-- opacity data etc.
c--
       real(kind(prec)), pointer :: epsilon(:,:,:)=>NULL()               ! thermal fraction (in cappa and sigma)
       real(kind(prec)), pointer :: opac(:,:,:)=>NULL()                  ! total extinction coefficient
       real(kind(prec)), pointer :: cappa(:,:,:)=>NULL()                 ! LTE absorption coefficient
       real(kind(prec)), pointer :: sigma(:,:,:)=>NULL()                 ! LTE scattering coefficient
       real(kind(prec)), pointer :: chi_nlte(:,:,:)=>NULL()              ! NLTE emissivities
       real(kind(prec)), pointer :: eta_nlte(:,:,:)=>NULL()              ! NLTE emissivities
c
       real(kind(prec)), pointer :: B(:,:,:)=>NULL()                     ! Planck-function 
       real(kind(prec)), pointer :: J(:,:,:)=>NULL()                     ! mean intensities
       real(kind(prec)), pointer :: Flux(:,:,:,:)=>NULL()                ! Flux (1:x, 2:y, 3:z)
       real(kind(prec)), pointer :: J_old(:,:,:)=>NULL()                 ! old mean intensities for iterations
       real(kind(prec)), pointer :: source(:,:,:)=>NULL()                ! source function
c
       real(kind(prec)), pointer :: velocity(:,:,:,:)=>NULL()            ! Velocity field (1:3,:,:,:), 1:vx, 2:vy, 3:vz
       real(kind(prec)), pointer :: J_bar_parts(:,:,:)=>NULL()           ! for testing 2lvl
       real(kind(prec)), pointer :: Lstar_test(:,:,:)=>NULL()            ! for testing
c--
c-- these fields are required for the anisotropic mode (e.g., Euler system)
c--
       real(kind(prec)), pointer :: cappa_aniso(:,:,:,:)=>NULL()         ! line (better: anisotropic) absorption coeffs
       real(kind(prec)), pointer :: sigma_aniso(:,:,:,:)=>NULL()         ! line (better: anisotropic) scattering coeffs
       real(kind(prec)), pointer :: chi_nlte_aniso(:,:,:,:)=>NULL()      ! anisotropic NLTE emissivities
       real(kind(prec)), pointer :: eta_nlte_aniso(:,:,:,:)=>NULL()      ! anisotropic NLTE emissivities
c
       real(kind(prec)), pointer :: cappa_avg(:,:,:)=>NULL()             ! solid angle averaged cappa (for epsilon computation)
       real(kind(prec)), pointer :: opac_avg(:,:,:)=>NULL()              ! solid angle averaged total extinction coefficient
       real(kind(prec)), pointer :: source_avg(:,:,:)=>NULL()            ! solid angle averaged source function
c
       real(kind(prec)), pointer :: norm(:,:,:)=>NULL()                  ! normalization factors for d\Omega integral
c
       real(kind(prec)), pointer :: n_quad(:,:,:)=>NULL()                ! MC integration hit counter 
c
       real(kind(prec)), pointer :: DeltaErad(:,:,:)=>NULL()             ! net radiative energy gain (negative: loss)
c--
c-- for Ng acceleration:
c--
       real(kind(prec)), pointer :: J_ng(:,:,:, :)=>NULL()               ! scratch storage for Ng acceleration
       integer :: nng = 100000                                           ! counter for Ng acceleration
c--
c-- these are used in the SC variant:
c--
       real(kind(prec)), pointer :: intensity(:,:,:)=>NULL()             ! intensities for one set of chars
c--
c-- these are used in the LC variant:
c--
       real(kind(prec)), pointer :: x_char(:,:,:)=>NULL()                ! x-coordinates of a char
       real(kind(prec)), pointer :: y_char(:,:,:)=>NULL()                ! y-coordinates of a char
       real(kind(prec)), pointer :: z_char(:,:,:)=>NULL()                ! z-coordinates of a char
       integer,          pointer :: im_char(:,:,:,:)=>NULL()             ! pointer to the previous voxel of a char
c--
c-- data structure for new LC char handling. These give the head and the tail 
c-- of the linked list of chars that pass thru any voxel:
c--
       integer, pointer     :: n_chars(:,:,:)=>NULL()                    ! counts the number of chars that pass thru a voxel
       type(charp), pointer :: head(:,:,:)=>NULL()                       ! first on the list
       type(charp), pointer :: tail(:,:,:)=>NULL()                       ! last on the list
c--
c-- data structures for the Lstar operator.
c-- for each voxel it stores the effects of *this* voxel on itself and the 
c-- previous and next voxels (integrated over all chars and solid angles)
c-- this can be build up with local quantities for good data locality during construction
c-- may need to be revised to improve data locality during acceleration step phase!
c--
c--                                       V nearest neighbors: (-1:1,-1:1,-1:1)
       real(kind(prec)), pointer :: Lstar(:,:,:,  :,:,:)=>NULL() !
c--                                               ^ voxel coordinates (ix,iy,iz)
c--
       real(kind(prec)), pointer :: Lstar_parts(:,:,:)=>NULL()           ! used to store temp. data during integration of Lstar
       real(kind(prec)), pointer :: Lstar_wide(:,:,:)=>NULL()            ! used to store temp. data during integration of Lstar
c--
c-- this is used for a larger Lstar operator and/or optimized memory usage:
c--
       type(Lstar), pointer :: Lambda(:,:,:)=>NULL()                     ! voxel coordinates (ix,iy,iz)
       type(Lstar), pointer :: Lambda_l(:,:,:)=>NULL()                   ! local version for tracker 
c--
c-- these are used in the LC variant for periodic BCs in
c-- the Lstar construction:
c--
       integer,          pointer :: im_char_periodic(:,:,:,:)=>NULL()    ! pointer to the previous voxel of a char
c--                                                                      ! 1:x, 2:y, 3:z index of next/previous voxel
       real(kind(prec)), pointer :: Lstar_parts_periodic(:,:,:)=>NULL()  ! used to store temp. data during integration of Lstar
c--
c-- to check the chars (debug):
c--
       logical, pointer :: done(:,:,:)=>NULL()                           ! .true. if intensity not set yet.
c--
c-- this section is for NLTE lines:
c--
       real(kind(prec)), pointer :: profile(:,:,:)=>NULL()               ! line profile data at ONE wavelength
       real(kind(prec)), pointer :: pnorm(:,:,:)=>NULL()                 ! line profile normlization data at ONE wavelength
c--
c-- this is for spherical/cylindrical coordinates with a prescribed Rmap:
c--
       real(kind(prec)), pointer :: Rmap(:)=>NULL()                      ! Map of R  (or rho for cylinder coordinates)
       real(kind(prec)), pointer :: zmap(:)=>NULL()                      ! Map of z  (height, for cylinder coordinates)
c--
c--  velocity field in units of c
c--  
       real(kind(prec)), pointer :: beta(:,:,:,:)=>NULL()               ! Velocity field
       real(kind(prec)), pointer :: dbetdr(:)=>NULL()                   ! Velocity field gradient in homologous case
c--
c--  I at particular point on characteristic
c--
       real(kind(prec)), pointer :: I_old(:,:,:,:)=>NULL() ! at previous wavelength point  
       real(kind(prec)), pointer :: I_new(:,:,:,:)=>NULL() ! at current wavelength point
c--
c--  NM Version: The d/d\lambda coefficients at a particular point of a particular characteristic
c--
       real(kind(prec)), pointer :: pl(:,:,:)=>NULL() 
       real(kind(prec)), pointer :: diag_diag(:,:)  => NULL() !diagonal of diagonal block
       real(kind(prec)), pointer :: diag_sub(:,:)   => NULL() !sub-diagonal of diagonal block
!knop       real(kind(prec)), pointer :: diag_super(:,:,:,:) => NULL() !super-diagonal of diagonal block
       real(kind(prec)), pointer :: super_sub(:,:)  => NULL() !sub-diagonal of super-diagonal block
       real(kind(prec)), pointer :: super_diag(:,:) => NULL() !diagonal of super-diagonal block
       real(kind(prec)), pointer :: super_super(:,:)=> NULL() !super-diagonal of super-diagonal block
       real(kind(prec)), pointer :: sub_sub(:,:)    => NULL() !sub-diagonal of sub-diagonal block
       real(kind(prec)), pointer :: sub_diag(:,:)   => NULL() !diagonal of sub-diagonal block
       real(kind(prec)), pointer :: sub_super(:,:)  => NULL() !super-diagonal of sub-diagonal block
       real(kind(prec)), pointer :: nm_alpha(:,:) => NULL() !short characteristics interp. coeff. alpha
       real(kind(prec)), pointer :: nm_beta(:,:)  => NULL() !short characteristics interp. coeff. beta
       real(kind(prec)), pointer :: nm_gamma(:,:) => NULL() !short characteristics interp. coeff. gamma
       real(kind(prec)), pointer :: nm_alambda(:) => NULL() ! clamb in NM3D
       real(kind(prec)), pointer :: I_bc_last_lambda(:)  => NULL() ! NM BC
       real(kind(prec)), pointer :: I_bc_first_lambda(:)  => NULL() ! NM BC
       real(kind(prec)), pointer :: I_bc_spatial(:,:)  => NULL() ! NM BC
!--
!-- wavelength dependent NM quantities such as Lstar, J, F
!-- 
       type (NM3D_type), pointer :: NM_WL(:) => NULL()  ! index labels wavelength
c----------------------------------------------------
c--Bin's stuff (added two arrays, and one scalar)
c---
       real(kind(prec)), pointer :: perturb(:) => NULL() ! this stores coefficients c_n of Legendre polynomial  Leg_n(x) 
       real(kind(prec)), pointer :: legend(:) => NULL() ! the total angular part, p(\theta)
       real(kind(prec)), pointer :: legendp(:) => NULL() ! derivatives of the legender polynomial.
       real(kind(prec)) :: ratio = 0.d0 ! this is simply beta/r 
c-----------------------------------------------------
c-- 
c--
c-- in disks each voxel has a 2 dim. velocity vector!
c--
       real(kind(prec)), pointer :: kepvel(:,:,:)=>NULL()
       real(kind(prec)), pointer :: relvel(:,:,:)=>NULL()
c--
c-- this is used to store spherical/cylindrical chars starting points 
c-- and targets to speed up processing and aid openMP modes. This is
c-- stored for all angles that a (MPI) task may process.
c--
       type(char_list), pointer :: my_chars(:) => NULL()                 ! list of starting points for all solid angles
       integer :: angle_id = -1                                          ! current solid angle index
       integer :: n_angles = -1                                          ! number of solid angles
c-- 
c-- scalars:
c--
c--
c-- step sizes in the (positive) axes directions for cartesian grids
c--
       real(kind(prec)) :: x_step,y_step,z_step                          ! step sizes in x,y,z
       real(kind(prec)) :: t_step,p_step                                 ! step sizes in theta and phi
       real(kind(prec)) :: xs_inv,ys_inv,zs_inv                          ! 1/step sizes
       real(kind(prec)) :: ts_inv,ps_inv                                 ! 1/step sizes in theta and phi
c--
c-- physical parameters for spherical grids:
c--
       real(kind(prec)) :: Rin,Rout,Rscale,Rexp
c--
c-- physical parameters for moving spherical grids:
c--
       integer :: i_theta,i_phi
c--
c-- this is for a cartesian grid:
c--
c-- number of points in each direction
c-- this is for positive and negative axes, so 
c-- the total number of edge points is (2nx+1)*(2ny+1)*(2nz+1)
c--
       integer :: nx=-1                                                  ! number of points in x
       integer :: ny=-1                                                  ! number of points in y
       integer :: nz=-1                                                  ! number of points in z
c--
c-- number of neighbors in each direction of the Lstar operator (wide version)
c--
       integer :: nnlx = 1                                               ! number of Lstar neighbors in x
       integer :: nnly = 1                                               ! number of Lstar neighbors in y
       integer :: nnlz = 1                                               ! number of Lstar neighbors in z
c--
c-- this is for a spherical 3D grid:
c-- the coordinates used are (r,phi,theta) where
c-- r is the distance from the center, phi the azimuth and
c-- theta is the polar angle
c--
c-- number of points in each direction
c-- this is for positive and negative axes, so 
c-- the total number of edge points is (2nx+1)*(2ny+1)*(2nz+1)
c--
       integer :: nr=-1                                                  ! number of points in r
       integer :: np=-1                                                  ! number of points in phi
       integer :: nt=-1                                                  ! number of points in theta
!--
!-- wavelength min and max indices for wavelength domain decomposition
!--
       integer :: lb_wl,ub_wl
!--
!--n_chars_max at first an estimate of the total number of chars
!-- which will then be set as the true total number of chars at each direction
!-- 
       integer,pointer :: n_chars_max(:) =>NULL()
!--
!-- max_char_length at first an estimate of the max char_length
!-- which will then be set as the true max char length at each direction
!-- 
       integer,pointer :: max_char_length(:) =>NULL()
       type (local_char_ind), pointer :: char_list=>NULL()
      end type ! pve_data
c
      type real_world_voxel_data
c--
c-- these variables are needed to store structure & EOS information in real world applications:
c-- -------------------------------------------------------------------------------------------
c--
       type(EOS), pointer :: EOS(:,:,:)=>NULL()                          ! of all species at all points
       type(NLTE), pointer :: NLTE(:,:,:)=>NULL()                        ! NLTE data
c--
c-- this is to store the NLTE opacities
c--
       real(kind(prec)), pointer :: capnlt(:,:,:)=>NULL()                ! NLTE opacities (all voxels)
       real(kind(prec)), pointer :: etanlt(:,:,:)=>NULL()                ! NLTE emissivities (all voxels)
c--
c-- helpers
c--
       real(kind(prec)), pointer :: pertvdW(:,:,:)                       ! hold the total effective perturber density, see cas_damp_const_3D.f(:,:,:)
       integer, pointer :: listel_2_numbdens(:)=>NULL()                  ! map from listel to number density array for atomic lines
       integer, pointer :: ifacod_2_numbdens(:)=>NULL()                  ! map from ifacod to number density array for molecular lines
      end type ! real_world_voxel_data
c--   
c-- disk parameters
c--   
      integer, parameter :: maxwlpts = 1000
      real(kind(prec)), dimension(maxwlpts) :: wl_disk
      real(kind(prec)) :: rstar_disk = 0.d0
      real(kind(prec)) :: mstar_disk = 0.d0
      real(kind(prec)) :: I_star = 0.d0
      real(kind(prec)), pointer :: vfrac(:,:,:)=>NULL()
      real(kind(prec)), pointer :: j_wl(:,:,:,:)=>NULL()
      real(kind(prec)), pointer :: c_wl(:,:,:,:)=>NULL()
c
      end module dddrt_module
c--
c-- procedure interfaces
c--
      module dddrt_interfaces
      interface
c
      subroutine add_char_2_voxel(pve_grid,ix,iy,iz,char)
c     --------------------------------------------------- 
      use dddrt_module
      implicit none
c
      type(pve_data), intent(inout) :: pve_grid
      integer, intent(in) :: ix,iy,iz                                    ! voxel coordinates
      type(characteristic), pointer :: char                              ! this variable is used to store char data temporarily
      end subroutine add_char_2_voxel
c
c
c
      subroutine allocate_char(char)
c     ------------------------------
      use dddrt_module
      implicit none
      type(characteristic), pointer :: char
      end subroutine allocate_char
c
c
c
      subroutine deallocate_char(char)
c     --------------------------------
      use dddrt_module
      implicit none
      type(characteristic), pointer :: char                              ! characteristic to remove from linked list
      end subroutine deallocate_char
c
c
c
      subroutine del_char_from_voxel(pve_grid,char,ix,iy,iz)
c     ------------------------------------------------------
      use dddrt_module
      implicit none
c
      type(pve_data), intent(inout) :: pve_grid                          ! the voxel grid itself
      integer, intent(in) :: ix,iy,iz                                    ! voxel coordinates
      type(characteristic), pointer :: char                              ! characteristic to remove from linked list
      end subroutine del_char_from_voxel
c
      subroutine gauss_euler_3D(wl,wl0,vdpi,vx,vy,vz,prf)
c     ---------------------------------------------------
      real*8 ,intent(in) :: wl,wl0,vdpi                   ! Euler lambda, rest wavelngth of the line, 1/v(Doppler)
      real*8 ,intent(in) :: vx,vy,vz                      ! velocity components
      real*8 ,intent(out) :: prf(:,:)                     ! output profile value
c
      end subroutine gauss_euler_3D
c
      end interface
      end module dddrt_interfaces
c
      module chrp15d
      use dddrt_module
c
      implicit none
      integer :: ndp 
      real(kind(prec)), allocatable :: tdis(:,:),fdis(:,:)
c
      end module chrp15d
c
c
c
***********************************************************************
* This module defines the wavelength dependent things needed in 
* 3DRT.
* version 1.0 of 20/Jul/2005 by PHH+EAB
*
* changes
***********************************************************************
c
      module wl3d_module
      use IEEE_prec
      implicit none
c
      integer, parameter :: nmax=10000
c--
c--   wavelength dependent stuff
c--
      integer :: nwl                                                     ! total number of wavelength points
c
      real(kind(prec)) :: wl(0:nmax),prf(nmax)
      real(kind(prec)) :: weilmb(nmax)
      real(kind(prec)) :: pnorm
c--
c-- stuff needed for moving atmospheres
c--
      real(kind(prec)) :: wlk,wlkm1,dwl
c
      contains
c
c
c
      subroutine lingrd(wline,width,npkt,wdhstp,wdhstp_out)
c     -----------------------------------------------------
      use IEEE_prec
      implicit none
      include 'physconst.inc'
      integer, intent(in) :: npkt
      real(kind(prec)), intent(in) :: wline,width,wdhstp,wdhstp_out
************************************************************************
*  build the grid for the line transfer test calculations
*             version 1.0 of 20/Jul/2005 by phh
*-- input:
*  wline: wavelength of the line
*  width: width of the line
*  npkt: number of lambda points within the line -1
*  wdhstp: step size for lambda array in width units
*-- output:
* goes into module
*-- notes:
************************************************************************
c--
c-- local variables:
c--
      integer :: i,itot
      real(kind(prec)) :: hlp
c--
c-- statement function for the Gauss profile
c--
      real(kind(prec)) :: dl,dgi,gauss
      gauss(dl) = exp(-(dl*dgi)**2)*dgi*(1.d0/(sqrt(pi)))
c
c-- construct wavelength array
c
c-- continuum point
c
      wl(0) = wline-100*width
c
c-- the line itself
c
      wl(1) = wline
      nwl = 1
      do i=1,npkt/2
       wl(nwl+1) = wl(1)+i*(wdhstp*width)
       wl(nwl+2) = wl(1)-i*(wdhstp*width)
       nwl = nwl+2
      enddo
      do i=1,npkt/2
       wl(nwl+1) = wl(1)+i*(wdhstp_out*width)
       wl(nwl+2) = wl(1)-i*(wdhstp_out*width)
       nwl = nwl+2
      enddo
      call sort(nwl,wl(1))
      itot = nwl+1
      do i=0,nwl
       if(wl(i) .eq. wl(i+1)) then
        itot = itot-1
        wl(i+1:itot) = wl(i+2:itot+1)
       endif
       if(i .ge. itot-1) exit
      enddo
      nwl = itot-1
      wl(0) = wl(1)-(wl(2)-wl(1))*1.0d0
c--
c-- nwl should be even for easier parallelization:
c--
      if(mod(nwl,2) .ne. 0) then
       wl(nwl+1) = wl(nwl)+(wl(nwl)-wl(nwl-1))*1.0d0
       nwl = nwl+1
      endif
c
c-- compute lambda integration weights
c
      do 20 i=2,nwl-1
       weilmb(i) = 0.5d0*(wl(i+1)-wl(i-1))
   20 continue
      weilmb(1) = 0.5d0*(wl(2)-wl(1))
      weilmb(nwl) = 0.5d0*(wl(nwl)-wl(nwl-1))
c
c-- build opacity arrays
c
      dgi = 1.d0/width
      pnorm = 0.d0
      do 100 i=1,nwl
       hlp = gauss(wl(i)-wline)
       prf(i) = hlp
       pnorm = pnorm+weilmb(i)*hlp
  100 continue
      pnorm = 1.d0/pnorm
      do 110 i=1,nwl
       prf(i) = prf(i)*pnorm
  110 continue
c
c-- print the results
c
      write(*,*)
      write(*,*) ('*',i=1,130)
      write(*,*) 'lingrd results:'
      write(*,9110) 'wline:',wline,'width:',width,
     * 'wdhstp:',wdhstp,'pnorm:',pnorm
      write(*,9100) 'npkt:',npkt,'nwl:',nwl
      write(*,*) ('*',i=1,130)
c
c-- print the grid:
      write(*,*)
      write(*,9000) 'n','wl','profile','weilmb'
      do 200 i=1,nwl
       write(*,9010) i,wl(i),prf(i),weilmb(i)
  200 continue
      write(*,*) ('*',i=1,130)
      if(nwl .ge. nmax) call stop_exit(1,"nmax too small")
c
 9000 format(1x,a3,10a12)
 9010 format(1x,i3,1p,10d12.4)
 9100 format(1x,5(a10,i6,' '))
 9110 format(1x,5(a10,1p,d12.4,' '))
      return
      end subroutine lingrd
      end module wl3d_module
c---
c--- module to define auxiliary variables neeeded in eulerian 2lvl mode 
c--- 
      module  eulerian_2lvl_module
      implicit none

      real(kind(1.d0)), pointer :: J_old_module(:,:,:)=>NULL()
      real(kind(1.d0)), pointer :: B_module(:,:,:)=>NULL()
      real(kind(1.d0)) :: strlin_module      
      real(kind(1.d0)) :: epslin_module
      real(kind(1.d0)) :: width_module
      real(kind(1.d0)) :: wline_module
      integer :: iwl_module

      end module eulerian_2lvl_module
