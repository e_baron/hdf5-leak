.SUFFIXES: .for .f95 .f90 .f .c .cc .cpp .o 

#################################################################################################################
#
# Flags for Monk (Mac Pro Trashcan) with GNU Compiler Suite (Debug Flags)
#
#################################################################################################################
# FC=h5pfc
# FFLAGS=-g -Og -Wall -fno-automatic  -ffixed-form  -fno-second-underscore -gdwarf-2 -Wline-truncation  -Wcharacter-truncation  -Wsurprising  -Waliasing  -Wimplicit-interface  -Wunused-parameter  -fwhole-file  -fcheck=all -ffpe-trap=zero,overflow  -pedantic  -fbacktrace

# CC=gcc
# CFLAGS=-g -DLITTLE_ENDIAN -DGOODF90 -DSUN -DO_LARGEFILE=0 -O0 -unroll

# LDFLAGS=-lc

#################################################################################################################
#
# Flags for Edison with Intel Compiler Suite (Optimized)
# NB: You must do:
# $ module load cray-hdf5-parallel
#
#################################################################################################################

# FC=ftn
# FFLAGS=-O3 -ip -unroll -xHost -ftz -save -fpconstant -heap-arrays
# 
# CC=cc
# CFLAGS=-O3 -ip -unroll -xHost -DLITTLE_ENDIAN -DSUN -DGOODF90 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE
# 
# LDFLAGS=-lstdc++
# 

#################################################################################################################

#################################################################################################################
#
# Flags for Edison with GNU Compiler Suite (Debug)
# NB: You must do:
# $ module unload cray-hdf5-parallel
# $ module swap PrgEnv-inte PrgEnv-gnu
# $ module load cray-hdf5-parallel
#
#################################################################################################################

# FC=ftn
# FFLAGS=-Og -g -march=native -fno-automatic -ffixed-form -fno-second-underscore
# 
# CC=cc
# CFLAGS=-Og -g -march=native -DLITTLE_ENDIAN -DSUN -DGOODF90 -DO_LARGEFILE=0
# 
# LDFLAGS=-lstdc++
# 


#################################################################################################################
#
# Flags for Carver Intel Suite (Optimized)
#
#################################################################################################################

# FC=h5pfc
# FFLAGS=-O3 -ftz -ip -save -IPF_fma -IPF_fltacc -stack_temps -fpconstant -heap-arrays
# 
# CC=mpicc
# CFLAGS=-O3 -ip -unroll -DLITTLE_ENDIAN -DSUN -DGOODF90 -D_FILE_OFFSET_BITS=64 -DLARGEFILE64_SOURCE -DO_LARGEFILE=0
# 
# LDFLAGS=-lstdc++
# 

#################################################################################################################
#
# Flags for Hobbes with Intel Compiler Suite (Optimized)
#
#################################################################################################################

# FC=h5pfc
# FFLAGS=-O3 -ip -unroll -xHost -ftz -save -fpconstant -heap-arrays

# CC=icc
# CFLAGS=-O3 -ip -unroll -xHost -DLITTLE_ENDIAN -DSUN -DGOODF90 -DO_LARGEFILE=0

# LDFLAGS=-lc -lstdc++

#################################################################################################################



#################################################################################################################
#
# Rest of Makefile
#
#################################################################################################################

MODULES=3drt_module.o casscom.o linecom.o hdftagsmod.o

FILES=phx1d_mpi_setup.o mpi_helper_routines.o stop_exit.o sort.o meminfo.o read_atm_v_h5.o cio.o

find_hdf5_leak: $(MODULES) $(FILES) find_hdf5_leak.o 
	$(FC) $(FFLAGS) $(LDFLAGS) -o $@ $^

find_hdf5_leak.o: $(MODULES)

clean:
	-rm *.mod *.o find_hdf5_leak
