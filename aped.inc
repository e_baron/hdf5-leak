************************************************************
*changes:
************************************************************
c
      type aped_energy_level_data
       sequence
       real*8, dimension(20) :: phot_par      ! Photoionisation cross section of outermost electron
       real*8  :: s_quan                      ! Spin quantum number of level
       real*8  :: energy                      ! Energy of level (ground state = 0 eV)
       real*8  :: e_error                     ! Error in energy level
       integer :: n_quan                      ! Principal quantum number of outermost electron
       integer :: l_quan                      ! Angular momentum of level
       integer :: level_deg                   ! Statistical weight of level
       integer :: phot_type                   ! Type of photoionization data
       character :: reference*80              ! Reference
      end type
c
      type aped_rad_trans_data
       sequence
       real*8  :: wavelen                     ! Theoretical wavelength
       real*8  :: wave_obs                    ! Observed wavelength
       real*8  :: wave_err                    ! Error on observed wavelength
       real*8  :: einstein_a                  ! Transition rate
       real*8  :: ein_a_err                   ! Error on transition rate
       integer :: upper_lev                   ! Transition's upper level, from energy level file
       integer :: lower_lev                   ! Transition's lower level, from energy level file
       character :: reference*80              ! Reference
      end type
c
      type aped_coll_excit_data
       sequence
       real*8  :: min_temp                    ! Minimum temperature of applicability
       real*8  :: max_temp                    ! Maximum temperature of applicability
       real*8, dimension(20) :: temp          ! Temperature array
       real*8, dimension(20) :: ecollpar      ! Effective collision strength parameter
       integer :: upper_lev                   ! Transition's upper level, from energy level file
       integer :: lower_lev                   ! Transition's lower level, from energy level file
       integer :: coeff_type                  ! Type of parameterization (see paper)
       character :: reference*80              ! Reference
       character :: pad*4                     ! to align to 8 byte words 
      end type
c
      type aped_dielec_recomb_data
       sequence
       real*8  :: wavelen                     ! Theoretical wavelength
       real*8  :: wave_obs                    ! Observed wavelength
       real*8  :: wave_err                    ! Error on observed wavelenth
       real*8  :: e_excite                    ! Excitation energy for DR satellite
       real*8  :: eexc_err                    ! Error on excitation energy
       real*8  :: satelint                    ! Satellite rate
       real*8  :: satinterr                   ! Error on satellite rate
       real*8, dimension(10) :: params        ! To be used by non-ISA data
       integer :: upper_lev                   ! Transition's upper level, from daugter ion's energy levels
       integer :: lower_lev                   ! Transition's lower level, from daugter ion's energy levels
       integer :: dr_type                     ! Type of DR data (=1 for IRA)
       character :: reference*80              ! Reference
       character :: pad*4                     ! to align to 8 byte words 
      end type

      integer, parameter :: APED=4
