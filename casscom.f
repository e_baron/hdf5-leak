c Time-stamp: <2014/07/22 13:51 baron>
************************************************************************
* this is Cassandra, the alo nlte multi-level part of phoenix and s3r2t
*
*                      version 5.4.0 of 19/Jul/2006
*                      by Peter H. Hauschildt (phh)
*                      &  Edward A. Baron (eab)
*                      &  Alexander Petz (ap)
*                      &  Derek Homeier (dh)
*                      &  Christine Johnas (cj)
*
*-- notes:
* 19/jul/2006 insert new perturber type 4, H_2 collision          (cj)
* 05/Apr/2006 added dummy_cont flag for LTE line selection        (phh)
* 22/jan/2006 added allocated data size output to cas_alloc       (phh)
* 09/aug/2005 added 2-photon continuum emission from CHIANTI4     (ap)
* 25/jun/2004 added use_log_bi_interpol flag                      (phh)
* 08/jun/2004 added support for Voigt profiles in cas_profiles    (dh)
* 04/apr/2003 insert types for APED                               (ap)    
* 28/nov/2002 insert types for CHINATI Ver. 4                     (ap)    
* 22/mar/2003 added special profile functions for NLTE lines      (phh)
* 03/feb/2003 update treatment of NLTE/LTE levels                 (phh)
* 06/may/2002 update logicals for line ids                        (eab)
* 22/Mar/2002: totally reworked version: fully allocatable, 
*              multi-database capable, input file datastructure,
*              improved collisional rates, vastly improved solver (phh)
* 27/feb/98: added logicals for line ids                          (eab)
* 03/dec/96: added support to split ions to diff. groups          (phh)
* 29/aug/96: added Fe I, Fe III, Co I, Co III                     (eab)
* 21/may/96: added support for F90 modules, adapted code.         (phh)
* 29/apr/96: fancy H I Profiles, gf threshold in casvoi...        (phh)
* 12/dec/95: cleaned up and add non-thermal rate handling         (eab)      
* 03/dec/95: streamling, optimization                             (phh)
* 27/nov/95: added N_i solver method                              (phh)
* 04/aug/95: do not insert continua for Ca II, Fe II, Ti I+II
*            and Co II if they are NOT in NLTE (to save time)     (phh)
* 01/aug/95: added optimized b-f cs calls to casabs and casvoi    (phh)
* 31/jul/95: added Ti II and Co II NLTE                       (eab+phh)
* 26/jul/95: added new Ca II model atom                       (eab+phh)
* 27/jun/95: added Ti I NLTE                                      (phh)
* 26/jun/95: improved vdW broadening for icasprf=3                (phh)
* 15/mar/95: optimized inline Voigt profiles                      (phh)
* 14/mar/95: included blocked and blocked+cached versions     (eab+phh)
* 24/jan/95: included windowed versions of routines               (phh)
* 16/sep/94: fixed problem in ratupd for continua                 (phh)
* 14/sep/94: Fe II and some optimizations                     (eab+phh)
* 24/aug/94: added support for Voigt profiles                     (phh)
* 20/aug/94: adapted radrat to use subset of lambda points        (phh)
* 25/apr/94: added tri-diagonal/diagonal switch                   (phh)
* 22/apr/94: added Ne I NLTE                                      (eab)
* 13/dec/93: added include file for species common blocks         (eab)
* 29/aug/93 reversed order of calls to kapcal in caseos           (phh)
* 13/aug/93 incorporated changes of eab and pen (He II, Na I)     (phh)
* 28/jul/93  added routines for nonthermal ionization             (eab)
* 28/jul/93  added routines circumstellar x-ray flux              (eab)
* 28/jul/93  added lapack calls                               (pen+eab)
* 21/jul/93  added routines for na i nlte                         (eab)
* 23/feb/93  added routines for he ii nlte                        (eab)
* 21/jul/93 added /casutl/, casprt and term names                 (phh)
* 06/jul/93 added istrns for easier maser check                   (phh)
* 18/jun/93 updated for use with France's EOS (caseos,bi2ni)      (phh)
* 24/mar/93 fixed he 2 statistical weight error in bi2ni       (eb+phh)
* 11/oct/92 approximation for weak NLTE lines added               (phh)
* 26/aug/92 remove bug in emissi (etavek initialization)          (phh)
* 25/aug/92 this is the version used to produce the jqsrt paper   (phh)
************************************************************************
c
c
c
c
*=============================================================================
*                      atomic data storage common blocks
*                                (description)
*
*-- parameters for allocatables:
* lvpion: maximum number of levels per ion
* maxlvl: maximum number of levels (total)
* maxion: maximum number of atoms and ions (total)
* maxtrn: maximum number of nlte transitions (total, line+continuum)
* maxstr: maximum number of transition-level couplings
*         treated in the alo scheme 
* maxgrp: maximum number of groups
* maxlpg: maximum number of level per group
*
*-- atomic data: (atoms)
* gi: statistical weight
* energy: energy above the ground state (1/cm=ky)
* eion: ionization energy, from ground state to ground state (ky)
* wltrn: wavelength of transition (lines)
*        wavelength of the threshold energy (continua)
* bij: einstein bij coefficient for absorption (lines)
*      photoionization cross section (continua, wavelength dependent!!!)
* gfnlte: statistical weight * oscillator strength for nlte lines
*
*-- administration: (admin)
* itrans(maxlvl,6): identifications of transitions:
*          (?,1): ion-code (as in levelid) (upper level)
*          (?,2): number of the level in the model atom (upper level)
*          (?,3): abolute number of the level (upper) 
*          (?,4-6): as 1-3 but for the lower level
*          (?,7): index into cas_prf_list or -1
* lcont: true, if the transition is a continuum transition.
* istrns: true, if there is a transition between absolute level i and j
* lvlid: identification of the levels:
*          (?,1): ion-code (atomic number*100+ionization stage, 2401=mg ii)
*          (?,2): level number within the model atom
*          (?,3): ion number
*          (?,4): group number
* ioncod: ion-code and total number of levels in the model atom for ion #x
*          (?,1): ion-code
*          (?,2): number of levels in the model atom
*          (?,3): index to EOS partial pressure array for this ion
* lvlnr: gives abolute level number for ion/level combination:
*        (nl,io): absolute # of level #'nl' of ion #'io'
* idgrp: group number corresponding to the ion number
* idmat: rate-matrix index corresponding to absolute level number
*  (the previous 2 arrays are used to identify the elements of each
*   rate-matrix)
* mp2lvl: gives the absolute level number for matrix entry mp in group
*         igrp: absolute level number = mp2lvl(mp,igrp)
* lvpgrp: number of NLTE levels in group 
* nions: total number of actual ions
* nlevel: total number of levels actually in use
* ntrans: total number of transitions (lines+continua) actually in use
* ngroup: number of groups actually in use
*
*-- idenfitication of levels: (catoms)
* cterm: character representation of the level designation:
*        (*,1): species ('H I', 'Mg II')
*        (*,2): term ('2s (1S)')
*
*-- variable data: (nltdat) 
* ocnlte: occupation numbers, nlte (1/cm**3)
* ocstar: occupation numbers, 'lte' (1/cm**3)
* prfem: emission profile (lines only)
* pnrem: norm of emission profile
* prfabs: absorption profile (lines only) (prd only)
* pnrabs: norm of absoprtion profile (prd only)
*
*-- occupation numbers and depature coeeficients for 
*--  cassandra internal usage: (depdat)
* ocinv: 1/ocnlte
* ocold: ocs of actual iteration, used in ALI procedure
* btnew: new btilde computed by ALI procedure
* bitrue: true departure coefficients, i.e. normalized to next 
*          ionization stage (ground level)
*
*-- rates: (rates)
* rabs: upward rates (lines and continua)
* rabs: downward rates (lines and continua)
*
*-- Jbars: (casutl)
* cjbar: normalized Jbars for line transitions, undef for cont. 
*
*-- coefficients for the alo-rate operator: (ratstr)
* rstabs: coefficients for upward rates (lines and continua)
* rstem: coefficients for downward rates (lines and continua)
* rstema,rstemc: as rstem, but for the upper and lower diagonal
* rstaba,rstabc: as rstabs, but for the upper and lower diagonal
* idrstr: identification of transitions and coupled levels
*         (?,1): transition number pointer (itrans array)
*         (?,2): absolute number of the level coupling to this transition
* ncoupl: actual number of transition-level couplings considered
* 
*=============================================================================
      module casscom
      use iso_c_binding
      implicit none
      include 'param.inc'
      private layer, nvemx, nionmx, nmolmx, npdmx, nelmx, nopmax, nopc,
     & nrest, ntab, nkmax, nrall, nsrt, nvsrt, nthdim, npgdim, mist,
     & mxst, maxel, nbmax, nbmol, maxpol, muemax, lmax, np, iw, 
     & maxcm, maxobs, maxapp, ntl, nlimax, layerend,z_max,
     & tma,nvemx1,nvionmx,ndust,ndust_opacities,npoints,ndust_wave
c--
      include 'chianti.inc'
      include 'aped.inc'
c--
c-- 0. type declarations (legacy):
c--
      real*8 caststop
      real*8 hok
      real*8 wlcmmaxpoint
      real*8 wlcmminpoint
      integer iapp
      integer iccwins
      integer ichan
      integer iclwine
      integer iclwins
      integer icwins
      integer ilwine
      integer ilwins
      integer iswtch
      integer lpfcurblk
      integer lpfunit
      integer lpncurblk
      integer lpnunit
      integer lrccurblk
      integer lrcunit
      integer lrlcurblk
      integer lrlunit
      integer lrsccurblk
      integer lrscunit
      integer lrslcurblk
      integer lrslunit
      integer lvpion
      integer maxgrp
      integer maxion
      integer maxlpg
      integer maxlvl
      integer maxstr
      integer maxtri
      integer maxtrn
      integer ncoupl
      integer nlteh
      integer nsave
c--
c-- parameter and common for cassandra.
c--
c-- 1. parameter:
c--
c      parameter(lvpion=620,maxlvl=13000,maxion=120,maxtrn=131000)
c     parameter(lvpion=620,maxlvl=12000,maxion=120,maxtrn=120000)
caddnlte maxion --> insert code for new species before this line
c      parameter(maxstr=131000,maxgrp=102,maxlpg=1680)
c     parameter(maxstr=120000,maxgrp=100,maxlpg=1680)
c     -- maximum number of levels for tri-diagonal solver
c     --   (will use diagonal if greater)
      parameter(maxtri=40)
c
c--- WARNING!! Different block sizes for lines and continua
c IS implemented--- In order to implement it the rate and rstar
c arrays must distinguish between lines and continua
c
c the arrays run from 0:maxtrnb so that the pointer to the
c current position within a block is simply given by
c mod(i,maxtrnb)
c--
c-- maxtrnb: blocksize for lines
c-- maxtrnbc: blocksize for continua
c--
      integer maxtrnb,maxtrnbc
CSMALL      parameter (maxtrnb=1000,maxtrnbc=maxlvl)
CBIG      parameter (maxtrnb=maxstr,maxtrnbc=maxlvl)
c--
c-- the sizes of the various caches for Cassandra.
c-- experimentd with them for optimium performance.
c-- we recommend that they are just big enough so that
c-- every IO operation is done only once during the
c-- wavelength integration, e.g.:
c
      integer ncache_prf,ncache_pnr,ncache_rl,ncache_rc,
     &        ncache_rsl,ncache_rsc
c
c--   parameter(ncache_prf=2,ncache_pnr=2,ncache_rl=2,ncache_rc=2)
c--   parameter(ncache_rsl=2,ncache_rsc=2)
c--
      parameter(ncache_prf=1,ncache_pnr=1,ncache_rl=1,ncache_rc=1)
      parameter(ncache_rsl=1,ncache_rsc=1)
CBIG      parameter(ncache_prf=1,ncache_pnr=1,ncache_rl=1,ncache_rc=1)
CBIG      parameter(ncache_rsl=1,ncache_rsc=1)
c--
c-- definition and blocks for the caching alg
c-- NOTE: if maxtrnbc = maxlvl, you can reduce the mem. req.
c-- by a HUGE factor by using the uncommented parameter
c-- definition below. If maxtrnbc < maxlvl, you MUST use the
c-- standard definition, otherwise ... disaster in progress!
c--
c-- l_lc1,l_lc2: starting and ending layer index for line caches
c-- l_cc1,l_cc2: starting and ending layer index for continuum caches
c-- mb_cache: blocksize of the line cache
c-- mbc_cache: blocksize of the continuum cache
c-- if mb_cache = 1, the line caches cannot be used
c-- if mbc_cache = 1, the continuum caches cannot be used
c-- if caches in use, you MUST set mb_cache=maxtrnb and mbc_cache=maxtrnbc
c-- l_lc1,l_lc2 and l_cc1,l_cc2 MUST be set to lc1,lc2 to use caches
c--
      integer l_lc1,l_lc2,mb_cache,l_cc1,l_cc2,mbc_cache
c
cstd      parameter(l_lc1=1,l_lc2=layer,mb_cache=maxtrnb)
cstd      parameter(l_cc1=1,l_cc2=layer,mbc_cache=maxtrnbc)
CSMALL      parameter(l_lc1=1,l_lc2=layer,mb_cache=maxtrnb)
CSMALL      parameter(l_cc1=1,l_cc2=layer,mbc_cache=1)
CBIG      parameter(l_lc1=1,l_lc2=1,mb_cache=1)
CBIG      parameter(l_cc1=1,l_cc2=1,mbc_cache=1)
c--
c-- ranges of depths points this process will allocate and work with:
c-- MUST be set to lc1=1, lc2=layer for serial code
c--
      integer lc1,lc2
c--
c-- flag for verbosity level, normally 0:
c--
      integer :: verbose = 0
c--
c-- counters for Voigt function statistics:
c-- this is nonstandard, change for compilers that do not
c-- allow 64bit integers
c--
      integer*8 :: n_nlte_lorentz=0 ! Lorentz wing counter, atomic lines
      integer*8 :: n_nlte_polynom=0 ! polynomial branch counter, atomic lines
      integer*8 :: n_nlte_complex=0 ! complex branch counter, atomic lines
c--
c-- general setup options:
c--
c-- use a simple approx. correction for NLTE if we have also
c-- negative ions in zusum and dozusu_general. This will work
c-- up to element Z=100 or so, otherwise we have array trouble
c-- this should be replaced by proper NLTE treatment of negative ions
c-- of course. PHH, 21/dec/2003
c--
      logical :: nlte_neg_ion_approx = .true. 
c--
c-- flag to select interpolation mode for departure
c-- coefficients in interpolate_bi:
c-- .true.: interpolate log(bi)
c-- .false.: interpolate bi in linear mode
c--
      logical :: use_log_bi_interpol=.true.
c
      logical :: no_rad_rates=.false.  ! for convenient debug!
      logical :: no_coll_rates=.false. ! for convenient debug!
      real*8 :: alo_line_factor = 1.0d0
      real*8 :: alo_cnt_factor = 1.0d0
      real*8 :: kill_line_alo_thres = 1d300 ! remove ALO if abs(rem/rabs-1.d0) is greater than...
c--
c-- this switches from old style (ion-by-ion) to new all-ions-for-one-element NLTE grouping
c--
      logical :: use_element_groups=.false.  
c--
c--
c-- flag for cas_*_lines line compute mode:
c-- 0: strict LTE mode (useful for 100% compatibility with LTE modes)
c-- 1: compatibility (old) mode (NLTE)
c-- 2: classic formula 
c-- 3: Mihalas NLTE formula also good for wide lines (best)
c--
      integer :: nlte_line_mode = 3
c--
      
        !changes due tio time dependent rate equation
        ! this code does not allow one to do use tde in the same timestep if the restart is not tde since it needs t-1, one can do tde with next time step if the restart has tde
 

      logical :: time_dep_flag =.false. !want to do THIS calculation time dep cal?
      logical :: read_from_tde =.false. ! if reading from tde output
      real*8  :: delta_t = 1.0d0*24.0d0*3600.0d0 !increment in seconds from       !last dy used on restart
      integer :: next_day=1 ! if one is going to next day or not
      integer :: prev_index=0
       !next day=0,same day but needs more iteration
      real*8,allocatable :: ocnlte_prev_tstep(:,:), 
     &bitrue_prev_tstep(:,:)
        !prev time step ocnlte and bi. bi is read from restart if 
        ! if read_from_tde is true

c-- flag/factor for ionization potential lowering 
c-- dchi is multiplied by dchi_factor. 
c-- set to 0.d0 to turn ionization limit lowering off
c-- set to 1.d0 to use standard values.
c--
      real*8 :: dchi_factor=1.d0
c--
c-- flag for ionization potential lowering for negative ions
c-- .true. will use dchi for negative ions
c-- .false. will not use ionization potential reduction
c--
       logical :: no_dchi_for_neg_ions=.true. ! new!!
c--
c-- flag to toggle use of new zusum routine (default)
c-- or the old routine (set value to .true.) 
c-- this is use for sahnlt to switch back to 
c-- the old zusum version.
c--
      logical :: use_zusu_old=.false.
c--
c-- set which H- method to use:
c-- 0: John (1988) data, uses implicit Saha factor
c-- 1: old data, use H- pressure for b-f
c-- results are (in strict LTE) nearly the same.
c--
      integer :: hminus_meth=0
c--
c-- set which CIA version to use:
c-- .true. : new CIA data
c-- .false.: old CIA data
c--
      logical :: newcia=.true.
c--
c-- flag to identify type of the O I model atom:
c-- false: new PHOENIX model atom from Kurucz data.
c-- true: old internal model atom from parts unknown.
c--
      logical :: o1_old=.false.
c--
c-- flags to switch between internal and external phoenix 
c-- model atoms. The internal ones are handcoded versions
c-- that are some times much smaller than the external 
c-- model atoms but may have better collisional and cross-section
c-- data. 
c--
      logical :: h1_internal=.true.
      logical :: he1_internal=.true.
      logical :: he2_internal=.true.
      logical :: ne1_internal=.true.
c--
c-- 2. common
c--
c-- block with occupation numbers and departure coeff's
c--
      real*8, allocatable :: ocinv(:,:),ocold(:,:),
     &                       bitrue(:,:),bitrue_inv(:,:),
     &                       ocnlte(:,:),ocstar(:,:)
      logical,allocatable :: bisolved(:,:)
c
c-- flags etc for nlte:
      common
     &/param / hok,iswtch,nsave,ichan,iapp,nlteh
c--
c-- nlte data commons used here:
c--
      real*8, allocatable,target :: gi(:),energy(:),eion(:)
      real*8, allocatable :: wltrn(:),bij(:),gfnlte(:)
      real*8, allocatable :: wltrn_inv(:),gi_inv(:)
c
      logical*4, allocatable,target :: lcont(:)
      integer*4, allocatable,target :: itrans(:,:)
      integer*4, allocatable,target :: ioncod(:,:),lvlnr(:,:),lvlid(:,:)
      integer*4, allocatable,target :: idgrp(:),idmat(:),lvpgrp(:)
      integer*4, allocatable,target :: mp2lvl(:,:)
      integer*4 nions,nlevel,ntrans,ngroup
      integer*4 lastcont,lastcntcoup
c--
c-- checks for detailed profiles
c--
      logical, allocatable :: doprf_init(:), doprf(:), dovprf(:)
c--
c-- the are used for Voigt lines in NLTE:
c--
      real*8,allocatable :: gam2(:)
      real*8,allocatable :: gam6(:)
      real*8,allocatable :: gam4(:)
      real*8,allocatable :: gamnat(:)
c--
c-- set the max. doppler width (set in modcon):
c--
      real*8 :: dopmax(maxel)
c--
c--
c-- this array is to identify the source of the model atom
c-- data so that coll. rates etc can be computed consistently
c--
      integer, allocatable,target :: data_source(:)
c--
c-- array with proton-electron ration
c--
      real*8 :: pe_ratio(layer)
c--
c-- this array of pointer to structures is used to store the
c-- CHIANTI4 bb electron collision data for ions
c--
      type bb_e_coll_data4
       type(bb_collision_data4), pointer :: bbcolldat4(:) => NULL()
       integer n_trans
      end type bb_e_coll_data4
c
      type(bb_e_coll_data4), allocatable :: chianti4_bb_e_coll_data(:)
c--
c-- this array of pointer to structures is used to store the
c-- CHIANTI4 bb proton collision data for ions
c--
      type bb_p_coll_data4
       type(bb_collision_data4), pointer :: bbcolldat4(:) => NULL()
       integer n_trans
      end type bb_p_coll_data4
c
      type(bb_p_coll_data4), allocatable :: chianti4_bb_p_coll_data(:)
c--
c-- this array of pointer to structures is used to store the
c-- CHIANTI4 fb energy level data
c--
      type fb_el_data4
       type(fb_energy_level_data), pointer :: fb_el_dat(:) => NULL()
       integer n_fb_level
      end type fb_el_data4
c
      type(fb_el_data4), allocatable :: chianti4_fb_el_data(:)
c--
c-- this array is for bremsstrahlung data from CHIANTI4
c--
      real*8, allocatable :: sutherland_data4(:,:),itoh_data4(:,:)
      real*8, dimension(layer) :: elecdens
      real*8, dimension(layer,30) :: sumions
      logical b_abs_c4
c--
c-- this array is for klgfb data from CHIANTI4
c--
      real*8, allocatable :: klgfb_data4_pe(:)
      real*8, allocatable :: klgfb_data4_gf(:,:,:)
c--
c-- this arrays are for 2 photon data from CHIANTI4
c--
      real*8, allocatable :: twoph_y0(:,:)
      real*8, allocatable :: twoph_avalue(:,:)
      real*8, allocatable :: twoph_asum(:)
      real*8, allocatable :: twoph_psi0(:,:,:)
      real*8, allocatable :: twoph_wldiff(:,:,:)
c--
c-- this array of pointer to structures is used to store the
c-- CHIANTI bb electron collision data for ions
c--
      type bb_e_coll_data
       type(bb_collision_data), pointer :: bbcolldat(:) => NULL()
       integer n_trans
      end type bb_e_coll_data
c
      type(bb_e_coll_data),allocatable :: chianti_bb_e_coll_data(:)
c--
c-- this array of pointer to structures is used to store the
c-- APED bb electron collision data for ions
c--
      type aped_e_coll_data
       type(aped_coll_excit_data), pointer :: bbcolldat(:) => NULL()
       integer n_trans
       real*8, pointer :: De(:) => NULL()
      end type aped_e_coll_data
c
      type(aped_e_coll_data),allocatable :: aped_bb_e_coll_data(:)
c--
c-- this array of pointer to structures is used to store the
c-- APED bb proton collision data for ions
c--
      type aped_p_coll_data
       type(aped_coll_excit_data), pointer :: bbcolldat(:) => NULL()
       integer n_trans
       real*8, pointer :: De(:) => NULL()
      end type aped_p_coll_data
c
      type(aped_p_coll_data),allocatable :: aped_bb_p_coll_data(:)
c--
c-- new style ID for cassandra
c--
      integer, parameter :: z_ion_max = 10000         ! that's up to Es IC ...
      integer :: id_ion_array(z_ion_max) = -1         ! internal number for each ion
      integer :: n_level_array(z_ion_max) = -1        ! number of level of each NLTE ion
      integer :: n_nlte_level_array(z_ion_max) = -1   ! number of actual nlte level of each NLTE ion
      integer, allocatable :: n_line_array(:)         ! number of b-b transitions for each ion
      logical :: dummy_cont(z_ion_max) = .false.      ! .true. indicates that an ion is a dummy place holder
c                                                     ! for the continuum. In this case, it's LTE lines should
c                                                     ! be used, see cas_setup and grelcmb*
c--
c-- this array of pointers to integers is used to locate all
c-- lines of a given ion to be used in approximating collional rates with 
c-- the van Regemorter formula
c-- example: line_locator(ion)%index(5) gives the index into itrans(,) for
c--          the 5th lines of ion 'ion' which has the code ioncod(ion,1)!
c--
      type line_locator_struc
       integer, pointer :: index(:) => NULL()
      end type line_locator_struc
c
      type(line_locator_struc),allocatable :: line_locator(:)
c-- 
c-- these are the new style arrays for partition functions etc:
c--
      real*8 cas_q(z_ion_max),cas_dq(z_ion_max),cas_d2q(z_ion_max)
      real*8 :: biground_dozusu(z_ion_max)
      real*8, allocatable :: cas_bi(:)
      integer*8 :: cas_bi_mod_counter = 0 ! counts how often cas_bi was changed (for dozusu_general)
      integer*8 :: dozusu_hit=0,dozusu_miss=0 ! counters for dozuso_general
      integer*8 :: dozusu_flush=0,dozusu_call=0
      integer*8 :: dozusu_cmpte=0
c-- 
c-- here's a flag that allows to set the cassandra version to use:
c-- 04: the old version, used in phoenix versions <= 11
c-- 05: the new version, still under development
c--
      integer :: cassandra_version = 05
c
      character*25, allocatable :: cterm(:,:),cgrp(:)
c
      real*8, allocatable :: etavek(:,:)
c
      real*8 rabs,rem
      real*8 rabsc,remc
      allocatable
     & rabs(:,:),rem(:,:),
     & rabsc(:,:),remc(:,:)
c
      integer, allocatable, target :: idrstr(:,:)
      real*8 rstabs,rstem,rstema,rstemc,rstaba,rstabc
      real*8 rstcabs,rstcem,rstcema,rstcemc,rstcaba,rstcabc
      allocatable
     & rstabs(:,:),rstaba(:,:),
     & rstem(:,:),rstema(:,:),
     & rstemc(:,:),rstabc(:,:),
     & rstcabs(:,:),rstcaba(:,:),
     & rstcem(:,:),rstcema(:,:),
     & rstcemc(:,:),rstcabc(:,:)
c
c
      real*8 prfabs,pnrabs,prfem,pnrem
      allocatable
     & prfem(:,:),pnrem(:,:)
CRD  &,prfabs(:,:),pnrabs(:,:)
c--
c-- search windows for optimized version of Cassandra:
c-- icnwins: start of continuum search window (initially 1)
c-- ilwins: start of line search window (initially lastcont+1)
c-- ilwine: end of line search window (initially lastcont+1)
c-- iccwins,iclwins,iclwine: same but for coupled transitions
c--
      common
     &/cassearch/ icwins,ilwins,ilwine,iccwins,iclwins,iclwine
     &/cassblk/ lpfunit,lpnunit,lrcunit,lrlunit,lrscunit,lrslunit
     &/cassblk/ lpfcurblk,lpncurblk,lrccurblk
     &/cassblk/ lrlcurblk,lrsccurblk,lrslcurblk
c--
c-- set level id array for fuzz lines
c-- idfuzz(2,maxlvl) 1-species code (as in cassandra)
c--                  2-level id code as read in by fuzzb
c--
      integer, allocatable :: idfuzz(:,:)
c
      logical lpf_dirty(0:ncache_prf-1)
      logical lpn_dirty(0:ncache_pnr-1)
      logical lrl_dirty(0:ncache_rl-1)
      logical lrc_dirty(0:ncache_rc-1)
      logical lrsl_dirty(0:ncache_rsl-1)
      logical lrsc_dirty(0:ncache_rsc-1)
      integer lpf_buf(0:ncache_prf-1)
      integer lpn_buf(0:ncache_pnr-1)
      integer lrl_buf(0:ncache_rl-1)
      integer lrc_buf(0:ncache_rc-1)
      integer lrsl_buf(0:ncache_rsl-1)
      integer lrsc_buf(0:ncache_rsc-1)
c--
c-- new dynamically allocated version
c--
      real*8, allocatable :: prfem_buf(:,:,:)
CRD   real*8, allocatable :: prfabs_buf(:,:,:)
      real*8, allocatable :: pnrem_buf(:,:,:)
CRD   real*8, allocatable :: pnrabs_buf(:,:,:)
      real*8, allocatable :: rem_buf(:,:,:)
      real*8, allocatable :: rabs_buf(:,:,:)
      real*8, allocatable :: rstem_buf(:,:,:)
      real*8, allocatable :: rstema_buf(:,:,:)
      real*8, allocatable :: rstemc_buf(:,:,:)
      real*8, allocatable :: rstabs_buf(:,:,:)
      real*8, allocatable :: rstaba_buf(:,:,:)
      real*8, allocatable :: rstabc_buf(:,:,:)
c
c-- continuum arrays
c
      real*8, allocatable :: remc_buf(:,:,:)
      real*8, allocatable :: rabsc_buf(:,:,:)
      real*8, allocatable :: rstcem_buf(:,:,:)
      real*8, allocatable :: rstcema_buf(:,:,:)
      real*8, allocatable :: rstcemc_buf(:,:,:)
      real*8, allocatable :: rstcabs_buf(:,:,:)
      real*8, allocatable :: rstcaba_buf(:,:,:)
      real*8, allocatable :: rstcabc_buf(:,:,:)
c
      common
     &/cas_cache1/ lpf_buf,lpn_buf,lrl_buf,lrc_buf,lrsl_buf,lrsc_buf
     &/cas_cache1/ lpf_dirty,lpn_dirty,lrl_dirty,lrc_dirty,lrsl_dirty
     &/cas_cache1/ lrsc_dirty
c--
      integer fancy_hprof
      real*8 cas_gf_thres,cas_fe_gf_thres
      common
     &/cas_profile_flags/ cas_gf_thres,cas_fe_gf_thres,fancy_hprof
c--
c-- general flags
c--
c--
c-- pb_h_coll_rates flag added for new H collision rates (jpa 18/apr/05)
c--
      logical twogam,twogam_line,outer_point_fix,pb_h_coll_rates
c
      logical :: use_van_reg=.true.
c
      common
     &/cas_flags/twogam,twogam_line,outer_point_fix,pb_h_coll_rates
c--
c-- These are tuning parameters for cas_solve... to 
c-- optimize numerical performance:
c--
c-- solver_flag_select: to select the solver that you 
c-- want in cas_solv... 
c--
c-- for cas_solve_stateq_element_groups:
c-- 0: qd HSL (not recommended)
c-- 1: MP Lapack
c-- 2: MP Linpack
c-- 3: MP HSL 
c-- default: out of range (for testing)
c
      integer :: solver_flag_select = 1
c--
c-- enable mpack/gmp based solver, this 
c-- appears to be faster, but is not available
c-- on all machines, so the default is .false.
c-- for now.
c--
      logical :: enable_mpack = .false.
c--
c-- select the MP linpack solver in cas_solve_stateq.f: 
c-- set to .false. for compatibility.
c--
      logical :: use_MP_solver = .true. ! now uses MP Linpack.
c--
c-- override switch to use the HSL solver and the 
c-- method it is supposed to use:
c-- method 1: standard m48
c-- method 4: m48 with iterative refinement.
c--
      integer :: method_4_HSL = 1 
c--
c-- here we set the limits for the QD package. If the dynamic range
c-- is larger than 1/switch... the code will use the next higher  
c-- precision arithmetic. 
c--
      real*8 :: switch_dd=1.d-08       ! then we'll go for 128bit arithmetic
      real*8 :: switch_qd=1.d+300      ! then we'll go for 256bit arithmetic
      real*8 :: switch_256bit=1d-30   ! less than this is means 256bit! 
      real*8 :: switch_game_over=1d-50 ! Game Over, man!
      real*8 :: switch_tilt=1d-050     ! less than this is zero!
c--
c-- this is an additional insanity check: if the estimated b_i are 
c-- larger or smaller than this, don't accept them!
c--
      real*8 :: max_bi_allowed=1d300
      real*8 :: min_bi_allowed=1d-300
      real*8 :: max_log_bi_change=200.d0
c--
c-- to talk to the C++ mpack/gmp driver we use this 
c-- structure to transfer the data:
c--
      type, bind(c) :: cass_data_f
c-- program control:
       integer(c_int) :: use_van_reg
       integer(c_int) :: no_rad_rates
       integer(c_int) :: no_coll_rates
       integer(c_int) :: decay
       real*8 :: alo_cnt_factor
       real*8 :: alo_line_factor
c-- these data are Cassandra group and local size info
       integer(c_int) :: igrp
       integer(c_int) :: nnlte
       integer(c_int) :: nord
       integer(c_int) :: nphys
       real*8 :: dntot
c-- these data are Cassandra setup data, pointers etc.
       integer(c_int) :: maxtrn
       integer(c_int) :: maxion
       integer(c_int) :: lvpion
       integer(c_int) :: maxlvl
       integer(c_int) :: maxgrp
       integer(c_int) :: maxlpg
       integer(c_int) :: maxstr
       integer(c_int) :: nions
       integer(c_int) :: nlevel
       integer(c_int) :: ntrans
       integer(c_int) :: ncoupl
       integer(c_int) :: ngroup
       integer(c_int) :: lastcont
       integer(c_int) :: lastcntcoup
       integer(c_int) :: p_coll_chianti4
       integer(c_int) :: p_coll_aped
       type(c_ptr) :: lcont
       type(c_ptr) :: itrans
       type(c_ptr) :: idrstr
       type(c_ptr) :: mp2lvl
       type(c_ptr) :: lvlid
       type(c_ptr) :: lvlnr
       type(c_ptr) :: ioncod
       type(c_ptr) :: idmat
       type(c_ptr) :: data_source
       type(c_ptr) :: energy
       type(c_ptr) :: eion
c-- these data are voxel dependent:
       real*8 ::  ed,T_elec,pe_ratio
       real*8 ::  ep,npdot
       type(c_ptr) :: rstcabs
       type(c_ptr) :: rstcem
       type(c_ptr) :: rstem
       type(c_ptr) :: rstabs
       type(c_ptr) :: rabsc
       type(c_ptr) :: remc
       type(c_ptr) :: rem
       type(c_ptr) :: rabs
       type(c_ptr) :: ocnlte
       type(c_ptr) :: ocstar
       type(c_ptr) :: bitrue
       type(c_ptr) :: numbdens
      end type cass_data_f
c
      type(cass_data_f) :: cas_data_f
c
      save
c
      contains
c
      subroutine cas_alloc
c     --------------------
      implicit none
      include 'param.inc'
      include 'phx-mpi.inc'                                         !MPI
***********************************************************************
* allocate all arrays that cassandra uses.
* version 1.0 of 13/may/96 by phh
*
* this routines needs to know layer and lc1,lc2!
***********************************************************************
c
      integer*8 :: total_alloced=0
c--
c-- block with switch for tri-diagonal or diagonal
c-- operator in Cassandra:
c--
      logical docastri
      common /casoptype/ docastri
c
      save
c
      total_alloced = 0
      write(*,*) 'cas_alloc: starting allocation procedure'
c
      if(id_nlte_rates .eq. MPI_UNDEFINED) then                     !MPI
       write(*,*) 'cas_alloc: no NLTE task, returning!'             !MPI
       return                                                       !MPI
      endif                                                         !MPI
c
      if(.not. allocated(bitrue)) then
c@      write(*,*) 'cas_alloc: allocating population arrays'
c
      allocate(ocinv(layer,maxlvl))
      allocate(ocold(layer,maxlvl))
      allocate(bitrue(layer,maxlvl),bitrue_inv(layer,maxlvl))
      allocate(bisolved(layer,maxlvl))
      allocate(ocnlte(layer,maxlvl),ocstar(layer,maxlvl))
      allocate(cas_bi(maxlvl))
      if(time_dep_flag) then      
       allocate(ocnlte_prev_tstep(layer,maxlvl))
       allocate(bitrue_prev_tstep(layer,maxlvl))
      endif
c
      total_alloced = total_alloced+
     &   8.d0*(layer*maxlvl*6.d0+1.0d0*maxlvl)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 population arrays:',
     & 8.d0*(layer*maxlvl*6.d0+1.0d0*maxlvl)/1024.d0, ' KB',
     & 8.d0*(layer*maxlvl*6.d0+1.0d0*maxlvl)/1024.d0**2, ' MB',
     & 8.d0*(layer*maxlvl*6.d0+1.0d0*maxlvl)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
c@      write(*,*) 'cas_alloc: resetting population arrays'
c
      ocinv(:,:)      = 0.d0
      ocold(:,:)      = 0.d0
      bitrue(:,:)     = 1.d0
      bitrue_inv(:,:) = 1.d0
      bisolved(:,:)   = .true.
      ocnlte(:,:)     = 0.d0
      ocstar(:,:)     = 0.d0
      cas_bi(:)     = -1.d0
      if(time_dep_flag) then
       bitrue_prev_tstep(:,:) =1.0d0
       ocnlte_prev_tstep(:,:) =0.0d0      
      endif
      endif
c
c@      write(*,*) 'cas_alloc: lc1,lc2:',lc1,lc2
c
      allocate(etavek(lc1:lc2,maxlvl))
c
      total_alloced = total_alloced+
     &    8.d0*(1.d0*(lc2-lc1+1)*maxlvl)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 etavek array:',
     & 8.d0*(1.d0*(lc2-lc1+1)*maxlvl)/1024.d0, ' KB',
     & 8.d0*(1.d0*(lc2-lc1+1)*maxlvl)/1024.d0**2, ' MB',
     & 8.d0*(1.d0*(lc2-lc1+1)*maxlvl)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
      allocate(rabs(lc1:lc2,0:maxtrnb-1),rem(lc1:lc2,0:maxtrnb-1))
c
      total_alloced = total_alloced+
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 line rate arrays:',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0, ' KB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**2, ' MB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
      allocate(rabsc(lc1:lc2,0:maxtrnbc-1),remc(lc1:lc2,0:maxtrnbc-1))
c
      total_alloced = total_alloced+
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 cont rate arrays:',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0, ' KB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0**2, ' MB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
      allocate(rstabs(lc1:lc2,0:maxtrnb-1))
      allocate(rstem(lc1:lc2,0:maxtrnb-1))
c
      total_alloced = total_alloced+
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 line rate* arrays:',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0, ' KB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**2, ' MB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
      allocate(rstcabs(lc1:lc2,0:maxtrnbc-1))
      allocate(rstcem(lc1:lc2,0:maxtrnbc-1))
c
      total_alloced = total_alloced+
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & 'cas_alloc: total size of real*8 cont rate* arrays:',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0, ' KB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0**2, ' MB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnbc)/1024.d0**3, ' GB'
     &,total_alloced/1024.d0**2,' MB total'
c
      if(docastri) then
       allocate(rstaba(lc1:lc2,0:maxtrnb-1))
       allocate(rstema(lc1:lc2,0:maxtrnb-1))
       allocate(rstemc(lc1:lc2,0:maxtrnb-1),rstabc(lc1:lc2,0:maxtrnb-1))
       allocate(rstcabs(lc1:lc2,0:maxtrnbc-1),
     &          rstcaba(lc1:lc2,0:maxtrnbc-1))
       allocate(rstcema(lc1:lc2,0:maxtrnbc-1))
       allocate(rstcemc(lc1:lc2,0:maxtrnbc-1),
     &          rstcabc(lc1:lc2,0:maxtrnbc-1))
      endif
c
c
      allocate(prfem(lc1:lc2,0:maxtrnb-1),pnrem(lc1:lc2,0:maxtrnb-1))
CRD   allocate(prfabs(lc1:lc2,0:maxtrnb-1),pnrabs(lc1:lc2,0:maxtrnb-1))
c
      total_alloced = total_alloced+
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)
      write(*,'(1x,a,t55,4(1p,d12.4,a))')
     & "cas_alloc: total size of real*8 line profile arrays:",
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0, ' KB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**2, ' MB',
     & 8.d0*(2.d0*(lc2-lc1+1)*maxtrnb)/1024.d0**3, ' GB',
     & total_alloced/1024.d0**2,' MB total'
c
c@      write(*,*) 'cas_alloc: resetting allocated variables:'
      etavek = 0.d0
      rabs = 0.d0
      rem = 0.d0
      rabsc = 0.d0
      remc = 0.d0
      rstabs = 0.d0
      rstem = 0.d0
      rstcem = 0.d0
      rstcabs = 0.d0
      if(docastri) then
       rstaba = 0.d0
       rstema = 0.d0
       rstemc = 0.d0
       rstabc = 0.d0
       rstcaba = 0.d0
       rstcema = 0.d0
       rstcemc = 0.d0
       rstcabc = 0.d0
      endif
      prfem = 0.d0
      pnrem = 0.d0
CRD   prfabs = 0.d0
CRD   pnrabs = 0.d0
c--
c-- allocate the caches, if required:
c--
      if(maxtrnb .lt. ncoupl) then
       l_lc1 = lc1
       l_lc2 = lc2
       mb_cache = maxtrnb
       allocate(prfem_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_prf-1))
CRD    allocate(prfabs_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_prf-1))
       allocate(pnrem_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_pnr-1))
CRD    allocate(pnrabs_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_pnr-1))
       allocate(rem_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rl-1))
       allocate(rabs_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rl-1))
       allocate(rstem_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       allocate(rstema_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       allocate(rstemc_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       allocate(rstabs_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       allocate(rstaba_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       allocate(rstabc_buf(l_lc1:l_lc2,0:mb_cache-1,0:ncache_rsl-1))
       write(*,*) 'cas_alloc: allocated line caches'
       write(*,*) ' l_lc1,l_lc2,mb_cache=',l_lc1,l_lc2,mb_cache
      endif
c
c-- continuum arrays
c
      if(maxtrnbc .lt. nlevel) then
       l_cc1 = lc1
       l_cc2 = lc2
       mbc_cache = maxtrnbc
       allocate(remc_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rc-1))
       allocate(rabsc_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rc-1))
       allocate(rstcem_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       allocate(rstcema_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       allocate(rstcemc_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       allocate(rstcabs_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       allocate(rstcaba_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       allocate(rstcabc_buf(l_cc1:l_cc2,0:mbc_cache-1,0:ncache_rsc-1))
       write(*,*) 'cas_alloc: allocated continuum caches'
       write(*,*) ' l_cc1,l_cc2,mbc_cache=',l_cc1,l_cc2,mbc_cache
      endif
c
      write(*,*) 'cas_alloc: done!'
      return
      end subroutine cas_alloc
c
c
      subroutine cas_dealloc
c     ----------------------
      implicit real*8 (a-h,o-z)
      include 'param.inc'
      include 'phx-mpi.inc'                                         !MPI
***********************************************************************
* deallocate all arrays that cassandra uses.
* version 1.0 of 13/may/96 by phh
*
* this routines needs to know layer and lc1,lc2!
***********************************************************************
c--
c-- block with switch for tri-diagonal or diagonal
c-- operator in Cassandra:
c--
      logical docastri
      common /casoptype/ docastri
c
      save
c
      write(*,*) 'cas_dealloc: starting allocation procedure'
c
      if(id_nlte_rates .eq. MPI_UNDEFINED) then                     !MPI
       write(*,*) 'cas_dealloc: no NLTE task, returning!'           !MPI
       return                                                       !MPI
      endif                                                         !MPI
c
      write(*,*) 'cas_dealloc: lc1,lc2:',lc1,lc2
c
      deallocate(etavek)
c
      deallocate(rabs,rem)
      deallocate(rabsc,remc)
c
      deallocate(rstabs)
      deallocate(rstem)
      deallocate(rstcabs)
      deallocate(rstcem)
c
      if(docastri) then
       deallocate(rstaba)
       deallocate(rstema)
       deallocate(rstemc,rstabc)
       deallocate(rstcabs,
     &          rstcaba)
       deallocate(rstcema)
       deallocate(rstcemc,
     &          rstcabc)
      endif
c
      deallocate(prfem,pnrem)
CRD   deallocate(prfabs,pnrabs)
c
c--
c-- deallocate the caches, if required:
c--
      if(maxtrnb .lt. ncoupl) then
       deallocate(prfem_buf)
CRD    deallocate(prfabs_buf)
       deallocate(pnrem_buf)
CRD    deallocate(pnrabs_buf)
       deallocate(rem_buf)
       deallocate(rabs_buf)
       deallocate(rstem_buf)
       deallocate(rstema_buf)
       deallocate(rstemc_buf)
       deallocate(rstabs_buf)
       deallocate(rstaba_buf)
       deallocate(rstabc_buf)
       write(*,*) 'cas_dealloc: deallocated line caches'
       write(*,*) ' l_lc1,l_lc2,mb_cache=',l_lc1,l_lc2,mb_cache
      endif
c
c-- continuum arrays
c
      if(maxtrnbc .lt. nlevel) then
       deallocate(remc_buf)
       deallocate(rabsc_buf)
       deallocate(rstcem_buf)
       deallocate(rstcema_buf)
       deallocate(rstcemc_buf)
       deallocate(rstcabs_buf)
       deallocate(rstcaba_buf)
       deallocate(rstcabc_buf)
       write(*,*) 'cas_dealloc: allocated continuum caches'
       write(*,*) ' l_cc1,l_cc2,mbc_cache=',l_cc1,l_cc2,mbc_cache
      endif
c
      write(*,*) 'cas_dealloc: done!'
      return
      end subroutine cas_dealloc
c
c
c
      subroutine cas_level_alloc
c     --------------------------
      implicit none
      include 'param.inc'
      include 'phx-mpi.inc'                                         !MPI
***********************************************************************
* allocate most level arrays that cassandra uses.
* version 1.0 of 21/Mar/2002 by phh
*
* maxlvl, maxion, maxgrp have to be set before entering this routine!
* this routines needs to know layer!
***********************************************************************
c
      write(*,*)
      write(*,*) "cas_level_alloc: allocating level arrays with:"
      write(*,*) "layer=",layer
      write(*,*) "maxlvl=",maxlvl
      write(*,*) "maxstr=",maxstr
      write(*,*) "maxion=",maxion
      write(*,*) "maxtrn=",maxtrn
      write(*,*) "maxlpg=",maxlpg
      write(*,*) "maxgrp=",maxgrp
c
      allocate(gi(maxlvl),energy(maxlvl),eion(maxion))
      allocate(wltrn(maxtrn),bij(maxtrn),gfnlte(maxtrn))
      allocate(wltrn_inv(maxtrn),gi_inv(maxlvl))

      gi(:) = 0.d0
      energy(:) = 0.d0
      eion(:) = 0.d0
      wltrn(:) = 0.d0
      bij(:) = 0.d0
      wltrn_inv(:) = 0.d0
      gi_inv(:) = 0.d0
c
      allocate(doprf_init(maxtrn),doprf(maxtrn),dovprf(maxtrn))
      allocate(lcont(maxtrn))
      allocate(itrans(maxtrn,7))
      allocate(ioncod(maxion,3),lvlnr(lvpion,maxion),lvlid(maxlvl,4))
      allocate(idgrp(maxlvl),idmat(maxlvl),lvpgrp(-1:maxgrp))
      allocate(mp2lvl(maxlpg,maxgrp))
      allocate(idrstr(maxstr,2))
      allocate(idfuzz(2,maxlvl))
c
      doprf_init(:) = .false.
      doprf(:) = .false.
      lcont(:) = .false.
      itrans(:,:) = -1
      ioncod(:,:) = -1
      lvlnr(:,:) = -1
      lvlid(:,:) = -1
      idgrp(:) = -1
      idmat(:) = -1
      lvpgrp(:) = -1
      mp2lvl(:,:) = -1
      idrstr(:,:) = -1
      idfuzz(:,:) = -1
c--
c-- checks for detailed profiles
c--
c--
c-- the are used for Voigt lines in NLTE:
c--
coff  allocate(gam2(maxtrn))
      allocate(gam6(maxtrn))
      allocate(gam4(maxtrn))
      allocate(gamnat(maxtrn))
c
coff  gam2(maxtrn) = 0.d0
      gam6(maxtrn) = 0.d0
      gam4(maxtrn) = 0.d0
      gamnat(maxtrn) = 0.d0
c
      allocate(data_source(maxion))
      data_source(:) = -1
c
      allocate(cterm(maxlvl,2))
      allocate(cgrp(maxgrp))
c
      cterm(:,:) = ' '
      cgrp(:) = ' '
c
      return
c
      end subroutine cas_level_alloc
c
      end module casscom
c
c
      module cas_profiles
c     -------------------
      implicit none
c
      type profile_data                            ! profiles for the far wings
       real*8, pointer :: temp(:) => NULL()        ! temperature grid for profile
       real*8, pointer :: rho(:) => NULL()         ! density grid for profile
       real*8, pointer :: wl(:) => NULL()          ! normalized wavelength grid for profile
       real*8, pointer :: stretch_blue(:) => NULL()! wavelength conversion factor for blue profile
       real*8, pointer :: stretch_red(:) => NULL() ! wavelength conversion factor for red profile
       real*8, pointer :: prf_blue(:,:) => NULL()  ! blue profile as function of T and lambda
       real*8, pointer :: prf_red(:,:)  => NULL()  ! red profile as function of T and lambda
       integer :: type=-1                ! type of this profile:
                                         ! 0: coll. with He
                                         ! 1: coll. with H_2, Cinv
                                         ! 2: coll. with H_2, C2v
                                         ! 3: coll. with electron
c cj                                     ! 4: coll. with H_2
       integer :: nwl=0                  ! number of wavelength points in profile
       integer :: ntemp=0                ! number of temperature points for profile
       integer :: nrho=0                 ! number of density points for profile
       character :: ident_string*255     ! just a string for identification
      end type profile_data
c
      type near_wing_profile_data                  ! Lorentzian profile for the near wing
       real*8, pointer :: temp(:) => NULL()        ! list of temperatures
       real*8, pointer :: w2_star(:) => NULL()     ! damping constant factor (mult. with number dens. of perturber)
       real*8, pointer :: d2_star(:) => NULL()     ! wavelength shift factor (mult. with number dens. of perturber)
       integer :: database                         ! level numbers depend on the dataset used!
       integer :: lvl_low                          ! index of lower level
       integer :: lvl_up                           ! index of upper level
       integer :: type                             ! type of this profile:
                                                   ! 0: coll. with He
                                                   ! 1: coll. with H_2, Cinv
                                                   ! 2: coll. with H_2, C2v
c cj additional perturber type       
                                                   ! 4: coll. with H_2
       integer :: ntemp                            ! number of temperature points for profile
       character :: ident_string*255               ! just a string for identification
      end type near_wing_profile_data
c
      type profile_database
       type(near_wing_profile_data), pointer ::
     &    Lorentz(:) => NULL()            ! list of the different Lorentz near wing profiles for these transitions
       type(profile_data), pointer ::
     &    profile(:) => NULL()            ! list of the different types of data for this transition
       integer, pointer :: databases(:) => NULL() ! level numbers depend on the dataset used!
       integer, pointer :: lvl_low(:) => NULL()   ! index of lower level
       integer, pointer :: lvl_up(:) => NULL()    ! index of upper level
       integer :: nprofiles=0           ! number of different profile types for this line
       integer :: nlorentz=0            ! number of different Lorentz near wing profile types for these lines
       integer :: n_transitions=0       ! number of different lines this profile serves
       integer :: z_ion_code=0          ! code for the ion this profile serves
       character :: ident_string*255    ! just a string for identification
      end type profile_database
c
      type cas_line_prf
       type(profile_database), pointer :: prf => NULL() ! list of the different types of data for this transition
       type(near_wing_profile_data), pointer :: Lorentz(:) => NULL() ! list of the different types of data for this transition
       real*8, pointer :: phi(:,:) => NULL()            ! combined profiles for this line
       real*8, pointer :: wl(:) => NULL()               ! wavelength array for this line
       real*8, pointer :: blue_edge(:) => NULL()        ! wavelength of blue edge for all layers
       real*8, pointer :: red_edge(:) => NULL()         ! wavelength of red edge for all layers
       real*8, pointer :: dllrz(:) => NULL()            ! total Lorentz width for all layers
       real*8, pointer :: ddlrz(:) => NULL()            ! total Lorentz shift for all layers
       real*8, pointer :: lrzwgt(:) => NULL()           ! renormalisation factor of Lorentz core for all layers
       real*8, pointer :: dgi(:) => NULL()              ! Doppler width for all layers
       real*8, pointer :: alpha(:) => NULL()            ! Voigt parameter for all layers
       real*8, pointer :: xlor(:) => NULL()             ! transition point to dampen out near wing (in dllrz)
       integer :: nwl=0                                 ! number of wavelength points 
       integer :: nprofiles=0                           ! number of different profile types for this line
       integer :: nLorentz=0                            ! number of different Lorentz near wing profiles for this line
       integer :: transition=0                          ! index into itrans() array to locate transition
       logical :: no_far_wings = .false.                ! flag to use only Lorentz part of the profile
       logical :: voigt_near_wings = .true.             ! flag to use Voigt near wing profile
      end type cas_line_prf
c
      type structure_data
       real*8, pointer :: temp(:) => NULL()       ! temperatures 
       real*8, pointer :: PertDens(:,:) => NULL() ! Perturber densities in 1d19/cm^3 for the different profile types
       integer :: layer = 0                       ! number of data points (i.e., layers)
      end type structure_data
c
      type(profile_database), pointer :: cas_prof(:) => NULL()
      type(structure_data), save :: cas_structure_data
      type(cas_line_prf), allocatable,target :: cas_prf_list(:)
c
      integer :: n_cas_prfs=0       ! number of actual transitions in the files 
      integer :: n_prfs_assigned=0  ! number of lines that have special profiles
      integer :: n_wl_4_prfs=0      ! number of wavelength points per profile
c
c cj change for new perturber 
      integer, parameter :: high_type=5 ! largest type code number (for cas_profile_combine)
c
      logical :: profile_extrap=.true. ! set to true to extrapolate (log) profile toward red/blue
      logical :: no_lorentz_prf=.false.! set to true to ignore all Lorentz profile data
      integer :: prf_combine=1 ! use Derek's method (0: primitive method)
c
      end module cas_profiles
