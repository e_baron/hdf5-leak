************************************************************
*changes:
*Implementation of 5- and 9-point splines with
*    type bb_collision_data4, xs5 and xs9
*type fb_energy_level_data for fblvl files
*new number 3 for CHIANTI4
************************************************************
c
      type model_atom_data
       sequence
       real(kind(0.0D0))  :: Ecm               ! observed energy in cm^-1
       real(kind(0.0D0))  :: Eryd              ! observed energy in Rydbergs
       real(kind(0.0D0))  :: Ecmth             ! theoretical energy in cm^-1
       real(kind(0.0D0))  :: Erydth            ! theoretical energy in Rydbergs
       real(kind(0.0D0)) :: jj                 ! J, multiplicity is 2J+1
       integer :: l1                ! level index
       integer :: ss                ! 2S+1
       integer :: ll                ! L
       integer :: mult              ! multiplicity: 2J+1
       integer :: conf              ! configuration index
       integer :: fuzz              ! index of original level numer (phoenix data files only)
!--
!-- highest level of Ground State Configuration 
!-- used for Charge Exchange
!-- (should also be used for non-thermal, but not yet implemented)
!--
       integer :: nground           
       character :: desig*15        ! configuration description
       character :: spd*3           ! spectroscopic  description
       character :: ref*80          ! reference to model atom
       character :: pad*2           ! to align to 8 byte words 
      end type
c
      type line_data
       sequence
       real(kind(0.0D0))  :: wvl               ! wavelength in Angstroem
       real(kind(0.0D0))  :: gf                ! gf-value of transition (obvious, eh?)
       real(kind(0.0D0))  :: A_value           ! total radiative transition probability (s^-1)
       real(kind(0.0D0))  :: Bij               ! Einstein Bij for absorption (i < j)
       integer :: lvl1              ! indices of the lower level (starting at 1)
       integer :: lvl2              ! indices of the upper level (starting at 1)
       character :: ref*80          ! reference to data
      end type
c
      type bb_collision_data
       sequence
       real(kind(0.0D0))  :: gf                ! weighted oscillator  strengths gf
       real(kind(0.0D0))  :: De                ! energy difference between levels in Rydbergs
       real(kind(0.0D0))  :: C_ups             ! Burgess and Tully scaling parameter c
       real(kind(0.0D0))  :: Splups(5)         ! spline fits to the scaled Upsilons
       real(kind(0.0D0))  :: y2(5)             ! output from NR natural spline fit
       integer :: lvl1              ! indices of the lower level (starting at 1)
       integer :: lvl2              ! indices of the upper level (starting at 1)
       integer :: T_typ             ! transition type (1-4)
       character :: ref*80          ! reference to data
       character :: pad*4           ! to align to 8 byte words 
      end type
c
      type bb_collision_data4
       sequence
       real(kind(0.0D0))  :: gf                ! weighted oscillator  strengths gf
       real(kind(0.0D0))  :: De                ! energy difference between levels in Rydbergs
       real(kind(0.0D0))  :: C_ups             ! Burgess and Tully scaling parameter c
       real(kind(0.0D0))  :: Splups(9)         ! spline fits to the scaled Upsilons
       real(kind(0.0D0))  :: y2(9)             ! output from NR natural spline fit
       integer :: lvl1              ! indices of the lower level (starting at 1)
       integer :: lvl2              ! indices of the upper level (starting at 1)
       integer :: T_typ             ! transition type (1-4)
       integer :: nspl              ! number of splines (5 or 9)
       character :: ref*80          ! reference to data
      end type
c
      type fb_energy_level_data
       sequence
       real(kind(0.0D0))  :: Ecm               ! observed energy in cm^-1
       real(kind(0.0D0))  :: Ecmth             ! theoretical energy in cm^-1
       integer :: l1                ! level index
       integer :: pqn               ! principal quantum number of electron being ionized
       integer :: ll                ! L-value of ionized electron
       integer :: mult              ! multiplicity
       character :: desig*4         ! configuration description
       character :: spd*3           ! 'S', 'P', etc to denote L value
       character :: ref*80          ! reference to data
       character :: pad*1           ! to align to 8 byte words 
      end type
c
      real(kind(0.0D0)), dimension(5), parameter :: xs =
     &                      (/ 0.0d0, 0.25d0, 0.50d0, 0.75d0, 1.0d0 /)
c
      real(kind(0.0D0)), dimension(9), parameter :: xs5 =
     &                      (/ 0.0d0, 0.25d0, 0.50d0, 0.75d0, 1.0d0,
     &                         0.0d0,  0.0d0, 0.0d0, 0.0d0 /)
      real(kind(0.0D0)), dimension(9), parameter :: xs9 =
     &                      (/ 0.0d0, 0.125d0, 0.25d0, 0.375d0, 0.5d0,
     &                         0.625d0, 0.75d0, 0.875d0, 1.0d0 /)
c
cv1.0 integer, parameter:: PHOENIX=1,CHIANTI=2
c
      integer, parameter:: PHOENIX=1,CHIANTI=2,CHIANTI4=3
c
      integer io1bf_pointer
      common /o1_bf_pointer/ io1bf_pointer(36)
