/*

   This collection of routines does the line list read in C,
   used for 64bit files when the f90 compiler doesn't know them
   and/or when the f90 structures are brain-dead (Solaris, Cray).

   written by Peter H. Hauschildt (PHH)

   version 1.0 of 15/Jan/1999

revision history:

1.0: first operational version (phh, 15/jan/1999)
0.0: initial test version to verify concept (phh, 14/jan/1999)

*/

/*************************************************************/
/* includes, definitions, and declarations:                  */
/*************************************************************/

#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#if defined(T3E)
#include <ffio.h>
#endif

#ifdef MACOSX
#undef LITTLE_ENDIAN
#endif

#define BLOCKSIZE 16384
#define DEBUG 0
#define DEBUG1 0


/*************************************************************/
/* for Sun's and possibly other boxes:                       */
/*************************************************************/

#if defined(SUN) && !defined(LITTLE_ENDIAN)

/*************************************************************/
/* this type is for the data as they are on the disk         */
/*************************************************************/

typedef struct {
 int iwl;
 short ielion,ielo,igflog,igr,igs,igw;
} spectral_line_data;

/*************************************************************/
/* The f90 compiler stores them line this                    */
/*************************************************************/

#if defined(GOODF90)    /* HPUX 10 stores the lines in the correct way! */
	    /* Tru64 Unix OSF1 V4.0 works this way too!          */
	    /* Use this for CIO on alphas. Alphas just like      */
	    /* SUN otherwise except for fseeko call (see below). */
	    /* Include -DLITTLE_ENDIAN  -DSUN -DDEC90 in CFLAGS  */ 
	    /* for alphas. (jpa, 19/oct/00)                      */
typedef struct {
 int iwl;
 short ielion,ielo,igflog,igr,igs,igw;
} f90_spectral_line_data;
#else               /* Sun's don't */
typedef struct {
 int iwl;
 int ielion,ielo,igflog,igr,igs,igw;
} f90_spectral_line_data;
#endif

/*************************************************************/
/* this type is for the line list info block (unsigned's)    */
/*************************************************************/

typedef struct {
 int iwl;
 unsigned short ielion,ielo,igflog,igr,igs,igw;
} list_info_block;

typedef struct {
 int nlines;
 int iblksize,lastblk,bsu;
} list_info_block_small;

typedef struct {
 long long nlines;
 int iblksize,lastblk;
} list_info_block_big;

#endif

/*************************************************************/
/* on the Intel's, everything is little endian               */
/*************************************************************/

#if defined(LITTLE_ENDIAN)

typedef struct {
 unsigned char iwl[4];
 unsigned char ielion[2],ielo[2],igflog[2],igr[2],igs[2],igw[2];
} spectral_line_data;

#if defined(GOODF90)    /* PGI F90 stores the lines in the correct way! */
typedef struct {
 int iwl;
 short ielion,ielo,igflog,igr,igs,igw;
} f90_spectral_line_data;
#else               /* this crap doesn't */
typedef struct {
 int iwl;
 int ielion,ielo,igflog,igr,igs,igw;
} f90_spectral_line_data;
#endif

typedef struct {
 unsigned char iwl[4];
 unsigned char ielion[2],ielo[2],igflog[2],igr[2],igs[2],igw[2];
} list_info_block;


typedef struct {
 int nlines;
 int iblksize,lastblk,bsu;
} list_info_block_small;

typedef struct {
 long long nlines;
 int iblksize,lastblk;
} list_info_block_big;

#endif


/*************************************************************/
/* on the cray T3E, everything is different                  */
/*************************************************************/

#if defined(T3E)

typedef struct {
 short iwl;
 char ielion[2],ielo[2],igflog[2],igr[2],igs[2],igw[2];
} spectral_line_data;

typedef struct {
 short iwl;
 short ielion,ielo,igflog,igr,igs,igw;
} f90_spectral_line_data;

typedef struct {
 short iwl;
 char ielion[2],ielo[2],igflog[2],igr[2],igs[2],igw[2];
} list_info_block;


#define close_list_ CLOSE_LIST
#define open_list_ OPEN_LIST

#endif


/*************************************************************/
/* on HPUX boxes, the f90 compiler does not append _'s       */
/* same for XLF                                              */
/*************************************************************/


#if defined(HPUX) || defined(XLF)
 #define close_list_ close_list
 #define open_list_ open_list
 #define close_scratch_ close_scratch
 #define c_put_line_list_info_ c_put_line_list_info
 #define c_write_block_ c_write_block
 #define open_scratch_ open_scratch
 #define c_line_list_info_ c_line_list_info
 #define c_read_block_ c_read_block
 #define c_wrt_scr_blk_ c_wrt_scr_blk
 #define c_wrt_nlte_scr_blk_ c_wrt_nlte_scr_blk
 #define c_wrt_fuzz_blk_ c_wrt_fuzz_blk
 #define c_wrt_blk_ c_wrt_blk
 #define c_rd_scr_blk_ c_rd_scr_blk
 #define c_rd_nlte_scr_blk_ c_rd_nlte_scr_blk
 #define c_rd_fuzz_blk_ c_rd_fuzz_blk
 #define c_rd_list_blk_ c_rd_list_blk
 #define c_wrt_list_blk_ c_wrt_list_blk
 #define endian_check_ endian_check
 #define c_wrt_blk_o_ c_wrt_blk_o
 #define c_wrt_scr_blk_g_ c_wrt_scr_blk_g
 #define c_wrt_scr_blk_v_  c_wrt_scr_blk_v
 #define c_wrt_nlte_scr_blk_g_ c_wrt_nlte_scr_blk_g
 #define c_wrt_nlte_scr_blk_v_ c_wrt_nlte_scr_blk_v 
 #define c_rd_scr_blk_g_ c_rd_scr_blk_g
 #define c_rd_scr_blk_v_ c_rd_scr_blk_v
 #define c_rd_nlte_scr_blk_g_ c_rd_nlte_scr_blk_g 
 #define c_rd_nlte_scr_blk_v_ c_rd_nlte_scr_blk_v 
 #define c_getpid_ c_getpid
 #define c_getpeakrss_ c_getpeakrss
 #define c_getcurrentrss_ c_getcurrentrss
#endif

#if defined(HPUX)
 #define fseek fseek64
 #define open open64
 #define off_t off64_t
#endif


/*************************************************************/
/* these are the data structure for the scratch files        */
/*************************************************************/

#if !defined(PGF90)

#if defined(MACOSX)

/* the C compiler refuses to pack these structures. If you get
   weird cache errors or NaN's in the line profiles, this might be
   the solution. C and portable? Gimme a break!!
*/

typedef struct {
         char    intl[4*8];
} gauss_file_data;

typedef struct {
         char    intl[7*8];
} voigt_file_data;

typedef struct {
         char intl[3*8+4*4];
} fuzz_file_data;

#else

/* this is the way the gods of computing want it!
*/

typedef struct {
	 double lamel,gfex,chiel;
	 int    intl;
         int    ipad;
} gauss_file_data;

typedef struct {
	 double lamel,gfex,chiel,gamnat,gam4,gam6;
	 int    intl;
         int    ipad;
} voigt_file_data;

typedef struct {
	 double lamel,gfex,chiel;
	 int    intl,kucas,klcas,icnlte;
} fuzz_file_data;

#endif

#elif defined(PGF90)

	typedef struct {
	 double lamel,gfex,chiel;
	 int    intl,dummy;
	} gauss_file_data;

	typedef struct {
	 double lamel,gfex,chiel,gamnat,gam4,gam6;
	 int    intl,dummy;
	} voigt_file_data;

	typedef struct {
	 double lamel,gfex,chiel;
	 int    intl,kucas,klcas,icnlte;
	} fuzz_file_data;
#else
#error 
#endif


typedef struct {
 double lamel,bij;
 int    intl,lolvl,uplvl,sutran;
} gauss_nlte_file_data;

typedef struct {
 double lamel,bij,gamnat,gam4,gam6;
 int    intl,lolvl,uplvl,sutran;
} voigt_nlte_file_data;

/*************************************************************/
/* these macros are doing the Little Endian conversion       */
/*************************************************************/

#define convert16(slot,WHAT)  ((input[slot].WHAT[0]<<8  | input[slot].WHAT[1]))
#define convert32(slot,WHAT)  ((input[slot].WHAT[0]<<24 | input[slot].WHAT[1]<<16 | \
                                input[slot].WHAT[2]<<8  | input[slot].WHAT[3]))


unsigned short  endian_swap16(unsigned short x)
{
 return (x>>8) | 
        (x<<8);
}

unsigned int endian_swap32(unsigned int x)
{
 return (x>>24) | 
        ((x<<8) & 0x00FF0000) |
        ((x>>8) & 0x0000FF00) |
        (x<<24);
}

unsigned long long endian_swap64(unsigned long long x)
{
 return (x>>56) | 
        ((x<<40) & 0x00FF000000000000LL) |
        ((x<<24) & 0x0000FF0000000000LL) |
        ((x<<8)  & 0x000000FF00000000LL) |
        ((x>>8)  & 0x00000000FF000000LL) |
        ((x>>24) & 0x0000000000FF0000LL) |
        ((x>>40) & 0x000000000000FF00LL) |
        (x<<56);
}


/*************************************************************/
/* this macro is doing the Cray conversion                   */
/*************************************************************/

#define convert(slot,WHAT)  (input[slot].WHAT[0] <= 0x7f \
                              ? (short) (input[slot].WHAT[0]<<8 | input[slot].WHAT[1]) \
                              : (short) (0xffff0000 | input[slot].WHAT[0]<<8 | input[slot].WHAT[1]))
                         


/*************************************************************/
/* the C IO will be done through this malloc'ed pointer:     */
/*************************************************************/

spectral_line_data *input;



/*************************************************************/
/* global variables for various (commented) purposes         */
/*************************************************************/


int global_blocksize = 0; /* for error checking */
int opened_files = 0; /* to keep track of open files, for T3E */

/*************************************************************/
/* the file descriptor and the stream for the line list file */
/*************************************************************/

int fdes;
FILE *fd;

#if defined(T3E)
struct ffsw swstat;
#endif


/*************************************************************/
/* convert a single block for the T3E                        */
/*************************************************************/

#if defined(T3E)

int CONVERT_CRAY_BLOCK(input,f90_output,blocksize)    
short *blocksize;                          /* the size of the blocks  */
spectral_line_data *input;                 /* the input structure     */
f90_spectral_line_data *f90_output;        /* the output structure    */
{

  register int i;

  if(DEBUG) printf("blocksize: %d\n",*blocksize);
  for(i=0;i<*blocksize;i++) {
   f90_output[i].iwl    = input[i].iwl;
   f90_output[i].ielion = convert(i,ielion);
   f90_output[i].ielo   = convert(i,ielo);
   f90_output[i].igflog = convert(i,igflog);
   f90_output[i].igr    = convert(i,igr);
   f90_output[i].igs    = convert(i,igs);
   f90_output[i].igw    = convert(i,igw);
   if(DEBUG) {
    if(i == 1) {
    printf("slot: %d\n",i);
    printf("input[i].iwl = %d\n",input[i].iwl);
    printf("input[i].ielion[0] = %#x\n",input[i].ielion[0]);
    printf("input[i].ielion[1] = %#x\n",input[i].ielion[1]);
    printf("conver.ielion = %#x\n",f90_output[i].ielion);
    printf("input[i].ielo[0] = %#x\n",input[i].ielo[0]);
    printf("input[i].ielo[1] = %#x\n",input[i].ielo[1]);
    printf("conver.ielo = %#x\n",f90_output[i].ielo);
    }; 
   };
  }
  return 0;
}

#elif defined(SUN)

int convert_cray_block_(input,f90_output,blocksize)    
short *blocksize;                          /* the size of the blocks  */
spectral_line_data *input;                 /* the input structure     */
f90_spectral_line_data *f90_output;        /* the output structure    */

{ exit(1);
}

#endif


/*************************************************************/
/*************************************************************/
/* opening and closing various file typs                     */
/*************************************************************/
/*************************************************************/


/*************************************************************/
/* open the list file "name" and return 0 if OK, 1 if not    */
/*************************************************************/

#if defined(SUN)

int open_list_(name,blocksize,length)    
char *name;                   /* full path name to the file, \0 terminated */
int  *blocksize;              /* the size of the records we are using */
int   length;                 /* f90 passes the length of the string here */

#elif defined(T3E)

int OPEN_LIST(name,length,blocksize)    
char *name;                   /* full path name to the file, \0 terminated */
short  *blocksize;              /* the size of the records we are using */
short   length;                 /* f90 passes the length of the string here */

#endif
{
  spectral_line_data single_line;
  size_t sizzle;

  sizzle = sizeof(single_line);

  errno = 0;

#if defined(T3E)

  fdes = ffopen(name,O_RDONLY);
  if (fdes == -1) return errno;

#elif defined(BGP)

  /* open the file */
  fdes = open(name,O_RDONLY);
  if(fdes < 0) return errno;        /* fixed bug, 21/nov/2000, PHH */
  /* fd = fdopen(fdes,"rb"); */
  /* if(fd == NULL) return errno; */
#else

  /* open the file */
  fdes = open(name,O_RDONLY|O_LARGEFILE);
  if(fdes < 0) return errno;        /* fixed bug, 21/nov/2000, PHH */
  /* fd = fdopen(fdes,"rb"); */
  /* if(fd == NULL) return errno; */

#endif

/* allocate the buffer for the data to be read */

 input = (spectral_line_data *) calloc(*blocksize,sizzle);
 if (input == NULL) return -666;
 if(DEBUG) printf("allocate the buffer: global_blocksize=%d, *blocksize=%d\n",global_blocksize,*blocksize); 
 global_blocksize = *blocksize; /* to catch errors later on */

/* fprintf(stderr,"open_list called for %s, open files=%d\n",name,++opened_files); */

 return errno;

}

/*************************************************************/
/* close the list file "name" and return 0 if OK, 1 if not   */
/*************************************************************/

int close_list_()    
{

 free(input);

/* fprintf(stderr,"close_list called, open files=%d\n",--opened_files); */

#if defined(T3E)

 return ffclose(fdes);

#else

 /* return fclose(fd); */
 return close(fdes);

#endif

}

/*************************************************************/
/* open the file "name" and return 0 if OK, 1 if not         */
/*************************************************************/

#if defined(SUN)

int open_scratch_(name,flag,fdes,length)    
char *name;                   /* full path name to the file, \0 terminated */
int   length;                 /* f90 passes the length of the string here */
int  *flag;                   /* 0: write, 1: read */
int  *fdes;                   /* the file descriptor that we will need later */

#elif defined(T3E)

int OPEN_SCRATCH(name,length,flag,fdes)    
char *name;                   /* full path name to the file, \0 terminated */
short   length;                 /* f90 passes the length of the string here */
int  *flag;                   /* 0: write, 1: read */
int  *fdes;                   /* the file descriptor that we will need later */

#endif
{

/* this sync is required on the RRZ machines to make the data     */
/* available across the nodes                                     */

 sync();

 /* open the file as a stream */
 errno = 0;

 if(*flag == 0) 
  #if defined(SUN) 
   #if defined(BGP) 
    *fdes = open(name,O_WRONLY|O_CREAT,0666);
   #else
    *fdes = open(name,O_WRONLY|O_CREAT|O_LARGEFILE,0666);
   #endif
  #elif defined(T3E)
   *fdes = ffopen(name,O_WRONLY|O_CREAT|O_BIG,0666);
  #endif
 else if(*flag == 1) 
  #if defined(SUN) 
   #if defined(BGP) 
    *fdes = open(name,O_RDONLY,0);
   #else
    *fdes = open(name,O_RDONLY|O_LARGEFILE,0);
   #endif
  #elif defined(T3E)
   *fdes = ffopen(name,O_RDONLY|O_BIG,0);
  #endif
 else if(*flag == 2) 
  #if defined(SUN) 
   #if defined(BGP) 
    *fdes = open(name,O_RDWR|O_CREAT,0666);
   #else
    *fdes = open(name,O_RDWR|O_CREAT|O_LARGEFILE,0666);
   #endif
  #elif defined(T3E)
   *fdes = ffopen(name,O_RDWR|O_CREAT|O_BIG,0666);
  #endif
 else if(*flag == 3) 
  #if defined(SUN) 
   #if defined(BGP) 
    *fdes = open(name,O_RDWR,0666);
   #else
    *fdes = open(name,O_RDWR|O_LARGEFILE,0666);
   #endif
  #elif defined(T3E)
   *fdes = ffopen(name,O_RDWR|O_BIG,0666);
  #endif
 else
  return -666;

 /* fprintf(stderr,"open_scratch called for %s,fdes=%d, flag=%d, open files=%d\n",name,*fdes,*flag,++opened_files); */
 if(*fdes < 0) {
  fprintf(stderr,"open_scratch: error for %s\n",name);
  return -errno;
 };

 return *fdes;

}

/*************************************************************/
/* close the file "name" and return 0 if OK, 1 if not        */
/*************************************************************/

#if defined(SUN)

int close_scratch_(int *fd)
{

/* fprintf(stderr,"close_scratch called for %d, open files=%d\n",*fd,--opened_files); */
 return close(*fd);

}

#elif defined(T3E)

int CLOSE_SCRATCH(int *fd)
{

/*  fprintf(stderr,"close_scratch called for %d, open files=%d\n",*fd,--opened_files); */
 return ffclose(*fd);

}

#endif


/*************************************************************/
/*************************************************************/
/* reading/writing various file types                        */
/*************************************************************/
/*************************************************************/



/*************************************************************/
/* get a single block from the line list file and convert    */
/* the data to the f90 format                                */
/*************************************************************/

#if defined(SUN)

int c_read_block_(nrecord,f90_output,blocksize)    
long long *nrecord;                         /* the fortran 90 record number */
int *blocksize;                       /* the size of the blocks       */
f90_spectral_line_data *f90_output;   /* the output structure         */

#elif defined(T3E)

int C_READ_BLOCK(nrecord,f90_output,blocksize)    
long long *nrecord;                       /* the fortran 90 record number */
short *blocksize;                     /* the size of the blocks       */
f90_spectral_line_data *f90_output;   /* the output structure         */

#endif

{

  register int i;
  int ierr,attempts=10;
  off_t offset;
  size_t read_size = *blocksize*sizeof(*input);
  size_t actual;

  if(DEBUG) printf("global_blocksize=%d, *blocksize=%d\n",global_blocksize,*blocksize); 
  if(global_blocksize != *blocksize) return -666;  /* blocksize mismatch! */

  offset = (long long) (*nrecord-1)* (long long) read_size;
   ierr = lseek(fdes,offset,SEEK_SET);
  if(DEBUG) printf("%lld %lld -> %lld\n",*nrecord,read_size,(long long)offset); 
  if(DEBUG) printf("seek to block %lld at %lld (%lld MB): %d %d\n",*nrecord-1,offset,offset/(1024*1024),ierr,errno);

#if defined(SUN)

/* sometimes we have problems with large files and heavy loads */
  for(i=1;i<=attempts;i++) {
   ierr = lseek(fdes,offset,SEEK_SET);
   if(ierr != -1) break;     /* no error! */
   sleep(i);
  }
  if(ierr == -1) return -errno;
  ierr = 0; /* clear the code for later (old fseek test) */

  #if defined(DEC90)
  /* there's no fseeko on OSF1 V4.0 alphas, but fseek seems to work */
  /* (jpa, 19/oct/00) */
  /* ierr = fseek(fd,offset,SEEK_SET); */
  #else
  /* ierr = fseeko(fd,offset,SEEK_SET); */
  #endif

#elif defined(T3E)
  ierr = ffseek(fdes,offset,SEEK_SET);
  if (ierr == -1) {
    ierr = errno;
  } else {
    ierr = 0;
  }
#endif

  if(ierr) return ierr;

#if defined(SUN)

/* sometimes we have problems with large files and heavy loads */
  for(i=1;i<=attempts;i++) {
   errno = 0;
   actual = read(fdes,input,read_size);
   if(actual == read_size) break;
   sleep(i);
  }

#elif defined(T3E)

  ffread(fdes,input,read_size,&swstat);
  actual = swstat.sw_count / read_size;

#endif

  if(DEBUG) printf(" read data: %lld\n",(long long)actual); 
  if(actual != read_size) {
   printf("c_read_block: read error! actual=%lld, errno=%d\n",actual,errno);
   return -666;
  }
    
  if(DEBUG) printf("blocksize= %d, %lld\n",*blocksize,read_size); 

#if defined(SUN) && !defined(LITTLE_ENDIAN)

  for(i=0;i<*blocksize;i++) {
   f90_output[i].iwl    = input[i].iwl;
   f90_output[i].ielion = input[i].ielion;
   f90_output[i].ielo   = input[i].ielo;
   f90_output[i].igflog = input[i].igflog;
   f90_output[i].igr    = input[i].igr;
   f90_output[i].igs    = input[i].igs;
   f90_output[i].igw    = input[i].igw;
   if(DEBUG) {
    if(i == 1) { 
    printf("BIG: slot: %d\n",i);
    printf("input[i].iwl = %d\n",(int) input[i].iwl);
    printf("input[i].ielion = %#x\n",(short)input[i].ielion);
    printf("input[i].ielo = %#x\n",(short)input[i].ielo);
    };
   };
  }
#elif defined(T3E) 
  for(i=0;i<*blocksize;i++) {
   f90_output[i].iwl    = input[i].iwl;
   f90_output[i].ielion = convert(i,ielion);
   f90_output[i].ielo   = convert(i,ielo);
   f90_output[i].igflog = convert(i,igflog);
   f90_output[i].igr    = convert(i,igr);
   f90_output[i].igs    = convert(i,igs);
   f90_output[i].igw    = convert(i,igw);
   if(DEBUG) {
    if(i == 1) {
    printf("slot: %d\n",i);
    printf("input[i].iwl = %d\n",input[i].iwl);
    printf("input[i].ielion[0] = %#x\n",input[i].ielion[0]);
    printf("input[i].ielion[1] = %#x\n",input[i].ielion[1]);
    printf("conver.ielion = %#x\n",f90_output[i].ielion);
    printf("input[i].ielo[0] = %#x\n",input[i].ielo[0]);
    printf("input[i].ielo[1] = %#x\n",input[i].ielo[1]);
    printf("conver.ielo = %#x\n",f90_output[i].ielo);
    }; 
   };
  }
#elif defined(LITTLE_ENDIAN) 
  for(i=0;i<*blocksize;i++) {
   f90_output[i].iwl    = convert32(i,iwl);
   f90_output[i].ielion = convert16(i,ielion);
   f90_output[i].ielo   = convert16(i,ielo);
   f90_output[i].igflog = convert16(i,igflog);
   f90_output[i].igr    = convert16(i,igr);
   f90_output[i].igs    = convert16(i,igs);
   f90_output[i].igw    = convert16(i,igw);
/*   if(DEBUG) {
    if(i == 1) {
    printf("LITTLE_ENDIAN: slot: %d\n",i);
    printf("input[i].iwl = %d\n",(int) input[i].iwl);
    printf("input[i].ielion[0] = %#x\n",(short)input[i].ielion[0]);
    printf("input[i].ielion[1] = %#x\n",(short)input[i].ielion[1]);
    printf("conver.ielion = %#x\n",f90_output[i].ielion);
    printf("input[i].ielo[0] = %#x\n",(short)input[i].ielo[0]);
    printf("input[i].ielo[1] = %#x\n",(short)input[i].ielo[1]);
    printf("conver.ielo = %#x\n",f90_output[i].ielo);
    }; 
   }; */
  }
#endif
  return 0;
}


/*************************************************************/
/* write a f90 block to a line list file by converting f90   */
/* format to line list format                                */
/*************************************************************/

#if defined(SUN)

int c_write_block_(fd,nrecord,f90_output,blocksize,output)    
int *fd;                              /* the file descriptor for the file */
long long *nrecord;                         /* the fortran 90 record number */
int *blocksize;                       /* the size of the blocks       */
f90_spectral_line_data *f90_output;   /* the output structure         */
spectral_line_data *output;           /* the output structure         */

#elif defined(T3E)

int C_WRITE_BLOCK(fd,nrecord,f90_output,blocksize,output)    
int *fd;                              /* the file descriptor for the file */
long long *nrecord;                       /* the fortran 90 record number */
short *blocksize;                     /* the size of the blocks       */
f90_spectral_line_data *f90_output;   /* the output structure         */
spectral_line_data *output;           /* the output structure         */

#endif

{

  register int i;
  long long ierr;
  off_t offset;
  size_t write_size = *blocksize*sizeof(*output);
  size_t actual;

  offset = (long long) (*nrecord-1)* (long long) write_size;
  if(DEBUG) printf("%lld %lld -> %lld\n",*nrecord,write_size,offset); 

#if defined(SUN) && !defined(LITTLE_ENDIAN)
  for(i=0;i<*blocksize;i++) {
   output[i].iwl    = f90_output[i].iwl;
   output[i].ielion = (short) f90_output[i].ielion;
   output[i].ielo   = (short) f90_output[i].ielo;
   output[i].igflog = (short) f90_output[i].igflog;
   output[i].igr    = (short) f90_output[i].igr;
   output[i].igs    = (short) f90_output[i].igs;
   output[i].igw    = (short) f90_output[i].igw;
  }
#else
  printf("T3E/LITTLE_ENDIAN: not implemented!\n");
  exit(1);
#endif
  /* if(DEBUG1) {
   i = 0;
   if(i == 0) {
    printf("slot: %d\n",i);
    printf("output[i].ielion = %#x\n",output[i].ielion);
    printf("f90_output[i].ielion = %#x\n",f90_output[i].ielion);
    printf("output[i].ielo = %#x\n",output[i].ielo);
    printf("f90_output[i].ielo = %#x\n",f90_output[i].ielo);
   }; 
  };  &*/
   
  errno = 0;

#if defined(SUN)

  ierr = (long long) lseek(*fd,offset,SEEK_SET);
  if(DEBUG) printf("seek to block %lld at %lld (%lld MB): %lld %d\n",*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr == (long long) -1) return errno;
  actual  = (int) write(*fd,output,write_size);

#elif defined(T3E)

  ierr = (long long) ffseek(*fd,offset,SEEK_SET);
  if(DEBUG) printf("seek to block %d at %lld (%lld MB): %lld %d\n",*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr == (long long) -1) return errno;
  ffwrite(*fd,output,write_size,&swstat);
  actual = swstat.sw_count;

#endif

  if(DEBUG) printf(" write data: %lld, should be: %lld\n",actual,write_size);
  if(actual != (int) write_size) return actual;
    
  if(DEBUG) printf("blocksize= %d, %lld\n",*blocksize,write_size); 

  return 0;
}




/*************************************************************/
/* write a single block to the line list file, no conversion */
/*************************************************************/

#if defined(SUN)

int c_wrt_list_blk_(fd,nrecord,list_buffer,ndata)    

#elif defined(T3E)

int C_WRT_LIST_BLK(fd,nrecord,list_buffer,ndata)    

#endif
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *list_buffer;                  /* the data structure               */

{

  long long ierr=0;
  off_t offset;
  int actual;
  spectral_line_data dummy;
  int blocksize;

  blocksize = sizeof(dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

/* we do not need this: the file is written sequentially, so the 
   pointer location will do just fine if offset is > 0, but we need
   to 'rewind' the file for offset = 0!
*/

#if defined(SUN)
    ierr = (long long) lseek(*fd,offset,SEEK_SET);
#elif defined(T3E)
    ierr = (long long) ffseek(*fd,offset,SEEK_SET);
#endif

  if(DEBUG) printf("seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
#if defined(SUN)
    actual = write(*fd,list_buffer,blocksize);
#elif defined(T3E)
    ffwrite(*fd,list_buffer,blocksize,&swstat);
    actual = swstat.sw_count;
#endif
  if(DEBUG) printf(" write data: %d\n",actual); 
  if(actual != blocksize) return -actual;

  return 0;
}




/*************************************************************/
/* read a single block from a line list file, no conversion  */
/*************************************************************/

#if defined(SUN)

int c_rd_list_blk_(fd,nrecord,list_buffer,ndata)    

#elif defined(T3E)

int C_RD_LIST_BLK(fd,nrecord,list_buffer,ndata)    

#endif
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *list_buffer;                  /* the data structure               */

{

  long long ierr=0;
  off_t offset;
  int actual;
  spectral_line_data dummy;
  int blocksize;

  blocksize = sizeof(dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

#if defined(SUN)
    ierr = (long long) lseek(*fd,offset,SEEK_SET);
#elif defined(T3E)
    ierr = (long long) ffseek(*fd,offset,SEEK_SET);
#endif

  if(DEBUG) printf("c_rd_list_blk seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
#if defined(SUN)
  actual = read(*fd,list_buffer,blocksize);
#elif defined(T3E)
  ffread(*fd,list_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
#endif
  if(DEBUG) printf(" c_rd_list_blk write data: %d\n",actual); 
  if(actual != blocksize) return -666;

  return 0;
}



/*************************************************************/
/* get a single block from the scratch file                  */
/*************************************************************/

#if defined(SUN)

int c_rd_scr_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_RD_SCR_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr;
  off_t offset;
  int actual;
  gauss_file_data gauss_dummy;
  voigt_file_data voigt_dummy;
  int blocksize;

  if(*ndata < 0) blocksize = sizeof(voigt_dummy)* -*ndata; /* negative ndata indicates Voigt lines */
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

  #if defined(SUN)
   ierr = (long long) lseek(*fd,offset,SEEK_SET);
  #elif defined(T3E)
   ierr = (long long) ffseek(*fd,offset,SEEK_SET);
  #endif
  if(DEBUG) printf("c_rd_scr_blk seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  if(DEBUG) printf("c_rd_scr_blk after ierr test\n");
  errno = 0;
  #if defined(SUN)
  actual = read(*fd,f90_buffer,blocksize);
  if(errno != 0) return -abs(errno);
  if(actual == 0) return -999;
  if(DEBUG) printf("c_rd_scr_blk actual: %d\n",actual);
  #elif defined(T3E)
  ffread(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
  #endif
  if(DEBUG) printf(" read scratch data: %d\n",actual); 
  if(actual != blocksize) return -abs(actual);

  return 0;
}



/*************************************************************/
/* write a single block to the scratch file                  */
/*************************************************************/

#if defined(SUN)

int c_wrt_scr_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                       /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_WRT_SCR_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                       /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr=0;
  off_t offset;
  int actual;
  gauss_file_data gauss_dummy;
  voigt_file_data voigt_dummy;
  int blocksize;

  if(*ndata < 0) blocksize = sizeof(voigt_dummy)* -*ndata;  /* negative ndata indicates Voigt lines */
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

/* we do not need this: the file is written sequentially, so the 
   pointer location will do just fine if offset is > 0, but we need
   to 'rewind' the file for offset = 0!
*/

  if(offset == 0) {
   #if defined(SUN)
     ierr = (long long) lseek(*fd,offset,SEEK_SET);
   #elif defined(T3E)
     ierr = (long long) ffseek(*fd,offset,SEEK_SET);
   #endif
  };

  if(DEBUG) printf("c_wrt_scr_blk seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  errno = 0;
#if defined(SUN)
  actual = write(*fd,f90_buffer,blocksize);
#elif defined(T3E)
  ffwrite(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
#endif
  if(DEBUG) printf(" c_wrt_scr_blk write data: %d, %d\n",actual,errno); 
  if(actual != blocksize) return -errno;

  return 0;
}

/*************************************************************/
/* get a single block from a file                            */
/*************************************************************/

#if defined(SUN)

int c_rd_blk_(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */

#elif defined(T3E)

int C_RD_BLK(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */

#endif

{

  long long ierr;
  off_t offset;
  int actual;
  int blocksize;

  blocksize = *ndata * *recsize;
  offset = (long long) (*nrecord-1)* (long long) blocksize;

  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

  #if defined(SUN)
   ierr = (long long) lseek(*fd,offset,SEEK_SET);
  #elif defined(T3E)
   ierr = (long long) ffseek(*fd,offset,SEEK_SET);
  #endif
  if(DEBUG) printf("c_rd_scr_blk seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  if(DEBUG) printf("c_rd_scr_blk after ierr test\n");
  errno = 0;
  #if defined(SUN)
  actual = read(*fd,f90_buffer,blocksize);
  if(errno != 0) return -abs(errno);
  if(actual == 0) return -999;
  if(DEBUG) printf("c_rd_scr_blk actual: %d\n",actual);
  #elif defined(T3E)
  ffread(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
  #endif
  if(DEBUG) printf(" read scratch data: %d\n",actual); 
  if(actual != blocksize) return -abs(actual);

  return 0;
}



/*************************************************************/
/* write a single block to a file                            */
/*************************************************************/

#if defined(SUN)

int c_wrt_blk_(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */

#elif defined(T3E)

int C_WRT_BLK(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */

#endif

{

  long long ierr=0;
  off_t offset;
  int actual;
  int blocksize;

  blocksize = *ndata * *recsize;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

/*  if(offset == 0) { */
   #if defined(SUN)
     ierr = (long long) lseek(*fd,offset,SEEK_SET);
   #elif defined(T3E)
     ierr = (long long) ffseek(*fd,offset,SEEK_SET);
   #endif
/*  }; */

  if(DEBUG) printf("c_wrt_scr_blk seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  errno = 0;
#if defined(SUN)
  actual = write(*fd,f90_buffer,blocksize);
#elif defined(T3E)
  ffwrite(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
#endif
  if(DEBUG) printf(" c_wrt_scr_blk write data: %d, %d\n",actual,errno); 
  if(actual != blocksize) return -errno;

  return 0;
}

/*************************************************************/
/* get a single block from the fuzz scratch file             */
/*************************************************************/

#if defined(SUN)

int c_rd_fuzz_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_RD_FUZZ_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr;
  off_t offset;
  int actual;
  fuzz_file_data gauss_dummy;
  int blocksize;

  if(*ndata < 0) return -667;
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

  #if defined(SUN)
   ierr = (long long) lseek(*fd,offset,SEEK_SET);
  #elif defined(T3E)
   ierr = (long long) ffseek(*fd,offset,SEEK_SET);
  #endif
  if(DEBUG) printf("seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  #if defined(SUN)
  actual = read(*fd,f90_buffer,blocksize);
  #elif defined(T3E)
  ffread(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
  #endif
  if(DEBUG) printf(" write data: %d\n",actual); 
  if(actual != blocksize) return -actual;

  return 0;
}




/*************************************************************/
/* write a single block to the fuzz scratch file             */
/*************************************************************/

#if defined(SUN)

int c_wrt_fuzz_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_WRT_FUZZ_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr=0;
  off_t offset;
  int actual;
  fuzz_file_data gauss_dummy;
  int blocksize;

  if(*ndata < 0) return -667;
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

/* we do not need this: the file is written sequentially, so the 
   pointer location will do just fine if offset is > 0, but we need
   to 'rewind' the file for offset = 0!
*/

  if(offset == 0) {
   #if defined(SUN)
     ierr = (long long) lseek(*fd,offset,SEEK_SET);
   #elif defined(T3E)
     ierr = (long long) ffseek(*fd,offset,SEEK_SET);
   #endif
  };

  if(DEBUG) printf("seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
   #if defined(SUN)
  actual = write(*fd,f90_buffer,blocksize);
   #elif defined(T3E)
  ffwrite(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
   #endif
  if(DEBUG) printf(" write data: %d\n",actual); 
  if(actual != blocksize) return -actual;

  return 0;
}



/*************************************************************/
/* get a single block from the molecular NLTE scratch file   */
/*************************************************************/

#if defined(SUN)

int c_rd_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_RD_NLTE_SCR_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr;
  off_t offset;
  int actual;
  gauss_nlte_file_data gauss_dummy;
  voigt_nlte_file_data voigt_dummy;
  int blocksize;

  if(*ndata < 0) blocksize = sizeof(voigt_dummy)* -*ndata; /* negative ndata indicates Voigt lines */
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

  #if defined(SUN)
   ierr = (long long) lseek(*fd,offset,SEEK_SET);
  #elif defined(T3E)
   ierr = (long long) ffseek(*fd,offset,SEEK_SET);
  #endif
  if(DEBUG) printf("seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
  #if defined(SUN)
  actual = read(*fd,f90_buffer,blocksize);
  #elif defined(T3E)
  ffread(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
  #endif
  if(DEBUG) printf(" write data: %d\n",actual); 
  if(actual != blocksize) return -actual;

  return 0;
}




/*************************************************************/
/* write a single block to the molecular NLTE scratch file   */
/*************************************************************/

#if defined(SUN)

int c_wrt_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#elif defined(T3E)

int C_WRT_NLTE_SCR_BLK(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */

#endif

{

  long long ierr=0;
  off_t offset;
  int actual;
  gauss_nlte_file_data gauss_dummy;
  voigt_nlte_file_data voigt_dummy;
  int blocksize;

  if(*ndata < 0) blocksize = sizeof(voigt_dummy)* -*ndata;  /* negative ndata indicates Voigt lines */
  else blocksize = sizeof(gauss_dummy)* *ndata;

  offset = (long long) (*nrecord-1)* (long long) blocksize;
  if(DEBUG) printf("%lld %d -> %lld\n",*nrecord,blocksize,offset); 

/* we do not need this: the file is written sequentially, so the 
   pointer location will do just fine if offset is > 0, but we need
   to 'rewind' the file for offset = 0!
*/

  if(offset == 0) {
   #if defined(SUN)
     ierr = (long long) lseek(*fd,offset,SEEK_SET);
   #elif defined(T3E)
     ierr = (long long) ffseek(*fd,offset,SEEK_SET);
   #endif
  };

  if(DEBUG) printf("seek %d to block %lld at %lld (%lld MB): %lld %d\n",*fd,*nrecord-1,offset,offset/(1024*1024),ierr,errno);
  if(ierr < 0) return ierr;
   #if defined(SUN)
  actual = write(*fd,f90_buffer,blocksize);
   #elif defined(T3E)
  ffwrite(*fd,f90_buffer,blocksize,&swstat);
  actual = swstat.sw_count;
   #endif
  if(DEBUG) printf(" write data: %d\n",actual); 
  if(actual != blocksize) return -actual;

  return 0;
}



/*************************************************************/
/*************************************************************/
/* get/put list list info blocks                             */
/*************************************************************/
/*************************************************************/


/*************************************************************/
/* get info on the line list (blocksizes and such)           */
/*************************************************************/

#if defined(SUN)

int c_line_list_info_(name,nlines,iblksize,lastblk,bsu,length)    
char *name;                             /* filename, must be \0 terminated */
int length;
unsigned int *iblksize, *lastblk, *bsu;
long long *nlines;

#elif defined(T3E)

int C_LINE_LIST_INFO(name,length,nlines,iblksize,lastblk,bsu)    
char *name;                             /* filename, must be \0 terminated */
short length;
unsigned short *iblksize, *lastblk, *bsu;
long long *nlines;

#endif

{

 list_info_block input[1];
 list_info_block_small *small_input;
 list_info_block_big   *big_input;
 size_t read_size;
 size_t actual;
 
 read_size = sizeof(input);

 *nlines = 0;
 *iblksize = 16384;
 *lastblk = 1;
 *bsu = 16384;

 if(DEBUG) printf("c_line_list_info: ->%s<-, %d\n",name,length);

#if defined(BGP)
/* open the file as a stream */
 errno = 0;
 fdes = open(name,O_RDONLY);
 if(fdes == -1) return errno;
 fd = fdopen(fdes,"rb");
 if(fd == NULL) return errno;

/* get the info from the first record */
  actual = fread(input,read_size,1,fd);
#elif defined(SUN)
/* open the file as a stream */
 errno = 0;
 fdes = open(name,O_RDONLY|O_LARGEFILE);
 if(fdes == -1) return errno;
 fd = fdopen(fdes,"rb");
 if(fd == NULL) return errno;

/* get the info from the first record */
  actual = fread(input,read_size,1,fd);
#elif defined(T3E)
 errno = 0;
 fdes = ffopen(name,O_RDONLY);
 if(fdes == -1) return errno;
/* get the info from the first record */
  ffread(fdes,input,read_size,&swstat);
  actual = swstat.sw_count / read_size;
#endif
  if(DEBUG) printf("read data: %lld\n",actual);
  if(actual != 1) return (-666);

 /* close the file */
#if defined(SUN)
 errno =  fclose(fd);
#elif defined(T3E)
 errno =  ffclose(fdes);
#endif

 if(errno !=0) return errno;

/*
//
// this code assumes the old line list info block 
// valid for less than 2G lines (32 bit signed integer)
//
*/
#if defined(LITTLE_ENDIAN)
  *nlines = convert32(0,iwl);
#else
  *nlines = input[0].iwl;
#endif

#if defined(SUN) && !defined(LITTLE_ENDIAN)
  *iblksize =  input[0].ielion*(64*1024)+input[0].ielo;
  *lastblk = input[0].igr;
#elif defined(T3E) 
  *iblksize =  (unsigned short) convert(0,ielo);
  *lastblk = (unsigned short) convert(0,igr);
#elif defined(LITTLE_ENDIAN) 
  *iblksize =  (convert16(0,ielion))*(64*1024)+convert16(0,ielo);
  *lastblk = convert16(0,igr);
#endif
  *bsu = read_size* *iblksize;


/*
   printf("number of lines:       %lld\n",*nlines); 
   printf("file blocksize:        %d\n",*iblksize);
   printf("file number of blocks: %d\n",*lastblk);
   printf("bsu: before            %d\n",*bsu);
  printf("\nAuto-detection of line list format disabled for conversion/tests!\n");
  return 0; 
*/
/*
//
// now check for the new line list format
// heuristics: if the bsu from the file 
//  is not 1048576 we assume it's the big format
// this may need to be extended!
//
*/
   small_input = (list_info_block_small *) input;
   big_input = (list_info_block_big *) input;


#if !defined(XLF) && defined(LITTLE_ENDIAN) /* XLF on OSX defines LITTLE_ENDIAN! */
  *bsu = endian_swap32((*small_input).bsu);
#else
  *bsu = (*small_input).bsu;
#endif

   if(*bsu == 1048576) {
    printf("c_line_list_info: old style line database (small list) detected!\n");
   } else {
    printf("c_line_list_info: new style line database (big list) detected!\n");
    *bsu = 0; /* flag for new list format, just for info */

#if !defined(XLF) && defined(LITTLE_ENDIAN) /* XLF on OSX defines LITTLE_ENDIAN! */
   *nlines   = endian_swap64((*big_input).nlines); 
   *iblksize = endian_swap32((*big_input).iblksize);
   *lastblk  = endian_swap32((*big_input).lastblk);
#else
   *nlines   = ((*big_input).nlines); 
   *iblksize = ((*big_input).iblksize);
   *lastblk  = ((*big_input).lastblk);
#endif
   }


/* 
   printf("iwl, nlines %x%x%x%x %d\n",input[0].iwl[0],input[0].iwl[1],input[0].iwl[2],input[0].iwl[3], *nlines);
   printf("ielion   %x%x\n",input[0].ielion[0],input[0].ielion[1]);
   printf("ielo     %x%x\n",input[0].ielo[0],input[0].ielo[1]);
   printf("igflog   %x%x\n",input[0].igflog[0],input[0].igflog[1]);
   printf("igr   %x%x\n",input[0].igr[0],input[0].igr[1]);
   printf("igs   %x%x\n",input[0].igs[0],input[0].igs[1]);
   printf("file blocksize:        %d\n",*iblksize);
   printf("file number of blocks: %d\n",*lastblk);
   printf("bsu:                   %d\n",*bsu);
   printf("single line size: %d\n",read_size);

   printf("new format small:\n");
   printf("number of lines:       %d\n",endian_swap32((*small_input).nlines)); 
   printf("file blocksize:        %d\n",endian_swap32((*small_input).iblksize));
   printf("file number of blocks: %d\n",endian_swap32((*small_input).lastblk));
   printf("bsu:                   %d\n",endian_swap32((*small_input).bsu));
   

   printf("new format big:\n");
   printf("number of lines:       %lld\n",endian_swap64((*big_input).nlines)); 
   printf("file blocksize:        %d\n",endian_swap32((*big_input).iblksize));
   printf("file number of blocks: %d\n",endian_swap32((*big_input).lastblk));
   
*/

   printf("number of lines:       %lld\n",*nlines); 
   printf("file blocksize:        %d\n",*iblksize);
   printf("file number of blocks: %d\n",*lastblk);
   printf("bsu:                   %d\n",*bsu);

return 0;

}


/*************************************************************/
/* put info block in the line list (blocksizes and such)     */
/* the new version always write a big list format (64 bit)   */
/*************************************************************/

#if defined(SUN)

int c_put_line_list_info_(name,nlines,iblksize,lastblk,bsu,length)    
char *name;                             /* filename, must be \0 terminated */
int length;
long long *nlines;
unsigned int *iblksize, *lastblk, *bsu;

#elif defined(T3E)

int C_PUT_LINE_LIST_INFO(name,length,nlines,iblksize,lastblk,bsu)    
char *name;                             /* filename, must be \0 terminated */
short length;
long long *nlines;
unsigned short *iblksize, *lastblk, *bsu;

#endif

{

 int flag = 3;
 int fdes;
 int ierr = 0;
 off_t offset = 0;

 list_info_block_big output;
 size_t read_size;
 size_t actual;
 
 read_size = sizeof(input);

/* convert the data into the line list format structure: */

#if defined(LITTLE_ENDIAN)
   output.nlines   = endian_swap64(*nlines); 
   output.iblksize = endian_swap32(*iblksize);
   output.lastblk  = endian_swap32(*lastblk);
#else
   output.nlines = *nlines; 
   output.iblksize = *iblksize;
   output.lastblk  = *lastblk; 
#endif

 /* open the file as a stream */
 errno = 0;

 if(flag == 0) 
  #if defined(BGP) 
   fdes = open(name,O_WRONLY|O_CREAT,0666);
  #elif defined(SUN) 
   fdes = open(name,O_WRONLY|O_CREAT|O_LARGEFILE,0666);
  #elif defined(T3E)
   fdes = ffopen(name,O_WRONLY|O_CREAT|O_BIG,0666);
  #endif
 else if(flag == 1) 
  #if defined(BGP) 
   fdes = open(name,O_RDONLY,0);
  #elif defined(SUN) 
   fdes = open(name,O_RDONLY|O_LARGEFILE,0);
  #elif defined(T3E)
   fdes = ffopen(name,O_RDONLY|O_BIG,0);
  #endif
 else if(flag == 2) 
  #if defined(BGP) 
   fdes = open(name,O_RDWR|O_CREAT,0666);
  #elif defined(SUN) 
   fdes = open(name,O_RDWR|O_CREAT|O_LARGEFILE,0666);
  #elif defined(T3E)
   fdes = ffopen(name,O_RDWR|O_CREAT|O_BIG,0666);
  #endif
 else if(flag == 3) 
  #if defined(BGP) 
   fdes = open(name,O_RDWR,0666);
  #elif defined(SUN) 
   fdes = open(name,O_RDWR|O_LARGEFILE,0666);
  #elif defined(T3E)
   fdes = ffopen(name,O_RDWR|O_BIG,0666);
  #endif
 else
  return -666;

#if defined(SUN)||defined(BGP) 
  if(!fdes) return -errno;
#elif defined(T3E)
  if(fdes == -1) return -errno;
#endif

 #if defined(SUN)
  ierr = (long long) lseek(fdes,offset,SEEK_SET);
  if(ierr < 0) return ierr;
  ierr = write(fdes,&output,sizeof(output));
 #elif defined(T3E)
  ierr = (long long) ffseek(fdes,offset,SEEK_SET);
  if(ierr < 0) return ierr;
  ierr = ffwrite(fdes, &output,sizeof(output));
 #endif


   printf("\nc_put_line_list_info   \n");
   printf("number of lines:       %lld\n",*nlines);
   printf("file blocksize:        %d\n",*iblksize);
   printf("file number of blocks: %d\n",*lastblk);
   printf("bsu:                   %d\n",*bsu);
   

 /* close the file */
 return close(fdes);

}




/*************************************************************/
/* main program for testing  purposes                        */
/*************************************************************/

#ifdef TESTING

void main()
{
   int ierr;
   register int i;
   off_t offset;

   const char* name="/Volumes/DATA/DATA/lists/molec.20110418.bin";

   long long nlines;
   int file_blocksize,file_nblocks,bsu,length=0;

   spectral_line_data input[BLOCKSIZE];
   f90_spectral_line_data f90_output[BLOCKSIZE];
   size_t read_size = sizeof(input);
   size_t actual;

   c_line_list_info_(name,&nlines,&file_blocksize,&file_nblocks,&bsu,length);
   printf("number of lines:        %lld\n",nlines);
   printf("file blocksize:        %d\n",file_blocksize);
   printf("file number of blocks: %d\n",file_nblocks);
   printf("bsu:                   %d\n",bsu);
   printf("length:                %d\n",length);
   printf("single line size: %d\n",(int)read_size);

   fdes = open("/Volumes/DATA/DATA/lists/molec.20110418.bin",O_RDONLY);
   printf("file is open: %d, %d\n",fdes,errno);
   fd = fdopen(fdes,"rb");
   printf("file is fdopen: %d\n",errno);

   actual = fread(input,read_size,1,fd);
   printf("read data: %d\n",(int)actual);
 
   nlines = input[0].iwl;
   file_blocksize =  input[0].ielo;
   file_nblocks = input[0].igr;

   printf("number of lines:        %ld\n",nlines);
   printf("file blocksize:        %d\n",file_blocksize);
   printf("file number of blocks: %d\n",file_nblocks);
   printf("single line size: %d\n",read_size);
   
   //for(i=file_nblocks-11; i<file_nblocks; i++) {
   for(i=0; i<1; i++) {
    offset = (long long) i* (long long) read_size;

    ierr = fseeko(fd,offset,SEEK_SET);
/*    printf("seek to block %d at %lld (%lld MB): %d %d",i,offset,offset/(1024*1024),ierr,errno); */

    actual = fread(input,read_size,1,fd);
/*    printf(" read data: %d\n",actual); */
    
    printf("%d %d %d\n",i+1,input[0].iwl,input[BLOCKSIZE-1].iwl);


    printf("dump of data slot as BigEndian %d:\n",0);
    printf("iwl    = %d\n",input[0].iwl);
    printf("ielion = %d\n",input[0].ielion);
    printf("ielo   = %d\n",input[0].ielo);
    printf("igflog = %d\n",input[0].igflog);
    printf("igr    = %d\n",input[0].igr);
    printf("igs    = %d\n",input[0].igs);
    printf("igw    = %d\n\n",input[0].igw);

    printf("dump of data slot as LittleEndian %d:\n",0);
    printf("iwl    = %d\n",convert32(0,iwl));
    printf("ielion = %d\n",convert16(0,ielion));
    printf("ielo   = %d\n",convert16(0,ielo));
    printf("igflog = %d\n",convert16(0,igflog));
    printf("igr    = %d\n",convert16(0,igr));
    printf("igs    = %d\n",convert16(0,igs));
    printf("igw    = %d\n\n",convert16(0,igw));

   }
   
/* 
   for(i=0; i<10; i++) {
    printf("\ndump of data slot %d:\n",i);
    printf("iwl    = %d\n",input[i].iwl);
    printf("ielion = %d\n",input[i].ielion);
    printf("ielo   = %d\n",input[i].ielo);
    printf("igflog = %d\n",input[i].igflog);
    printf("igr    = %d\n",input[i].igr);
    printf("igs    = %d\n",input[i].igs);
    printf("igw    = %d\n",input[i].igw);
   }
*/

   ierr = fclose(fd);
   printf("file fclose'd: %d\n",ierr);
}
#endif

#ifdef AIX43

#include <sys/thread.h>
#include <sys/processor.h>
#include <sys/systemcfg.h>
/* takes no. of MPI process, MPI proc id, no. of OMP threads/proc and OMP thread id
   and binds each thread to a separate processor to get best timing */

void thread_cpu_bind(int* nproc,int* proc_id,int* nthrds, int* thread_id)
{
   short int a, ncpus, nused;
   unsigned char my_name[8];

   /* only do this if we have monopolised the node(s)! */
   ncpus = _system_configuration.ncpus;
   if (*nproc >= ncpus)
      bindprocessor(BINDTHREAD, thread_self(),(*nproc)*(*thread_id)+ *proc_id);

   /* mycpu() will give the cpu logical id on which a thread is running
	uncomment the line below if you are not sure all of your threads are running on unique CPUs */
   /* a=mycpu();
      printf ("cpu_id=%d, proc_id=%d, thread_id=%d, n_cpu=%d, n_proc=%d\n",
      a, *proc_id, *thread_id, ncpus, *nproc); */
   /* thread_self() call gives kernel thread id which is diff. from OMP thread id  uncomment the line below to see
     printf("thread_self=%d, thread_id=%d\n", thread_self(), *thread_id);
   */
}

void get_cpu_id(int* cpu_id)
{
   *cpu_id = (int) mycpu();
}

#else

/* just a dummy */

void thread_cpu_bind(int* nproc,int* proc_id,int* nthrds, int* thread_id)
{ };

void get_cpu_id(int* cpu_id)
{ }

#endif

/* here's a fix for the Mac OS X code!     */
/* (without this, csppress won't compile!) */
/* thanks to Tony Goelz from Absoft        */

#ifdef MACOSX

     void _objcInit()
     {
     }

     void _carbon_init()
     {
     }
#endif

/* Interface routines to OSX vector functions */

#ifdef OSX_ALTIVEC

#include   <Accelerate/Accelerate.h>
 v_vexp1( length, x, result )
 int *length;
 float x[*length], result[*length];
 {
    int i;
    {
       int j1, j2, j3, j4, j5, j6, j7;
       __vector float x2u, x3u;
       __vector unsigned char x4u = vec_lvsl(0, &x[0]);
       __vector float resu2u, resu4u, resu3u;
       __vector unsigned char resu5u = vec_lvsr(0, &result[0]);
       __vector unsigned char resu6u = vec_lvsl(0, &result[0]);
       __vector float x5u, x6u, resu7u, resu8u;
       __vector float x7u, x8u, resu9u, resu10u;
       __vector float x9u, x10u, resu11u, resu12u;
       int j8;
       __vector float resu13u, resu14u;
       static __vector unsigned long j1v[3] =  { (__vector unsigned long )
       (0, 0XFFFFFFFF, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(
       0, 0, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(0, 0, 0, 
       0XFFFFFFFF) } ;
       __vector signed short k1v = (__vector signed short )(0, 0, 0, 0, 0,
        0, 1, 0);
       __vector float x1u, resu1u;
       vec_mtvscr( k1v );
       j6 = *length;
       x2u = vec_ldl(0, &x[0]);
       resu3u = vec_ldl(0, &result[0]);
       resu2u = vec_perm(resu3u, resu3u, resu6u);
       for ( j1 = 0; j1 < (j6 - 4 * 4) + 1; j1 += 4 * 4 )
       {
          j3 = j1 * sizeof(int );
          j2 = j3 + 4 * sizeof(int );
          x3u = vec_ldl(j2, &x[0]);
          x5u = vec_ldl(j2 + 16, &x[0]);
          x7u = vec_ldl(j2 + 32, &x[0]);
          x1u = vec_perm(x2u, x3u, x4u);
          x2u = vec_ldl(j2 + 48, &x[0]);
          resu1u = vexpf(x1u);
          resu4u = vec_perm(resu2u, resu1u, resu5u);
          vec_stl(resu4u, j3, &result[0]);
          x6u = vec_perm(x3u, x5u, x4u);
          resu7u = vexpf(x6u);
          resu8u = vec_perm(resu1u, resu7u, resu5u);
          vec_stl(resu8u, j3 + 16, &result[0]);
          x8u = vec_perm(x5u, x7u, x4u);
          resu9u = vexpf(x8u);
          resu10u = vec_perm(resu7u, resu9u, resu5u);
          vec_stl(resu10u, j3 + 32, &result[0]);
          x10u = vec_perm(x7u, x2u, x4u);
          resu11u = vexpf(x10u);
          resu12u = vec_perm(resu9u, resu11u, resu5u);
          vec_stl(resu12u, j3 + 48, &result[0]);
          resu2u = resu11u;
       }
       if ( (j6 & (4 * 4 - 1)) == 0 )
          resu1u = resu2u;
          else
          {
             for ( ; j1 < (j6 - 4) + 1; j1 += 4 )
             {
                j3 = j1 * sizeof(int );
                j2 = j3 + 4 * sizeof(int );
                x3u = vec_ldl(j2, &x[0]);
                x1u = vec_perm(x2u, x3u, x4u);
                x2u = x3u;
                resu1u = vexpf(x1u);
                resu4u = vec_perm(resu2u, resu1u, resu5u);
                vec_stl(resu4u, j3, &result[0]);
                resu2u = resu1u;
             }
       }
       j8 = ((int )&result[0] & 15) >> 2;
       j4 = j6 & 3;
       if ( j4 > 0 )
       {
          j3 = j1 * sizeof(int );
          j2 = j3 + 4 * sizeof(int );
          x3u = vec_ldl(j2, &x[0]);
          x1u = vec_perm(x2u, x3u, x4u);
          resu1u = vexpf(x1u);
          resu4u = vec_perm(resu2u, resu1u, resu5u);
          j7 = j8 + j4;
          if ( j7 >= 4 )
          {
             vec_stl(resu4u, j3, &result[0]);
             if ( j7 > 4 )
             {
                resu13u = vec_ldl(j3 + 16, &result[0]);
                resu4u = vec_perm(resu1u, resu1u, resu5u);
                resu14u = vec_sel(resu4u, resu13u, j1v[j7-5]);
                vec_stl(resu14u, j3 + 16, &result[0]);
             }
          }
          else
          {
             resu13u = vec_ldl(j3, &result[0]);
             resu14u = vec_sel(resu4u, resu13u, j1v[j7-1]);
             vec_stl(resu14u, j3, &result[0]);
          }
       }
       else
       {
          j3 = j1 * sizeof(int );
          if ( j8 )
          {
             resu4u = vec_perm(resu2u, resu2u, resu5u);
             for ( j5 = 0; j5 < j8; j5++ )
                vec_ste(resu4u, j3 + (j5 - j8) * sizeof(float ), &result[0
                ]);
          }
       }
    }
 }
 v_vexp2( length, x, result )
 int *length;
 float x[*length], result[*length];
 {
    int i;
    {
       int j9, j10, j11, j12, j13, j14, j15;
       __vector float x12u, resu16u;
       __vector float x13u, resu17u;
       __vector float x14u, resu18u;
       __vector float resu19u;
       static __vector unsigned long j2v[3] =  { (__vector unsigned long )
       (0, 0XFFFFFFFF, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(
       0, 0, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(0, 0, 0, 
       0XFFFFFFFF) } ;
       __vector signed short k2v = (__vector signed short )(0, 0, 0, 0, 0,
        0, 1, 0);
       __vector float x11u, resu15u;
       vec_mtvscr( k2v );
       j14 = *length;
       for ( j9 = 0; j9 < (j14 - 4 * 4) + 1; j9 += 4 * 4 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          x11u = vec_ldl(j11, &x[0]);
          x12u = vec_ldl(j11 + 16, &x[0]);
          x13u = vec_ldl(j11 + 32, &x[0]);
          x14u = vec_ldl(j11 + 48, &x[0]);
          resu15u = vexpf(x11u);
          vec_stl(resu15u, j11, &result[0]);
          resu16u = vexpf(x12u);
          vec_stl(resu16u, j11 + 16, &result[0]);
          resu17u = vexpf(x13u);
          vec_stl(resu17u, j11 + 32, &result[0]);
          resu18u = vexpf(x14u);
          vec_stl(resu18u, j11 + 48, &result[0]);
       }
       for ( ; j9 < (j14 - 4) + 1; j9 += 4 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          x11u = vec_ldl(j11, &x[0]);
          resu15u = vexpf(x11u);
          vec_stl(resu15u, j11, &result[0]);
       }
       j12 = j14 & 3;
       if ( j12 > 0 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          x11u = vec_ldl(j11, &x[0]);
          resu15u = vexpf(x11u);
          resu19u = vec_ldl(j11, &result[0]);
          resu15u = vec_sel(resu15u, resu19u, j2v[j12-1]);
          vec_stl(resu15u, j11, &result[0]);
       }
    }
 }

 /* Translated by Veridian Systems VAST-F AltiVec 7.5 R8 20:48:16 1/31/04    */
 v_vlorentz1( length, alpha, y, lorentz )
 int *length;
 float alpha[*length], y[*length], lorentz[*length];
 {
    int i;
    {
       int j1, j2, j3, j4, j5, j6, j7;
       __vector float alph1u, y1u, lore1u, r3v, r4v, r5v, r7v, r8v;
       __vector float r6v = (__vector float )(1.);
       __vector float r2v = (__vector float )(0.);
       __vector float alph2u, alph3u;
       __vector unsigned char alph4u = vec_lvsl(0, &alpha[0]);
       __vector float y2u, y3u;
       __vector unsigned char y4u = vec_lvsl(0, &y[0]);
       __vector float lore2u, lore4u, lore3u;
       __vector unsigned char lore5u = vec_lvsr(0, &lorentz[0]);
       __vector unsigned char lore6u = vec_lvsl(0, &lorentz[0]);
       int j8;
       __vector float lore7u, lore8u;
       static __vector unsigned long j1v[3] =  { (__vector unsigned long )
       (0, 0XFFFFFFFF, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(
       0, 0, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(0, 0, 0, 
       0XFFFFFFFF) } ;
       __vector signed short k1v = (__vector signed short )(0, 0, 0, 0, 0,
        0, 1, 0);
       __vector float r1v = (__vector float )(0.564189583546E0);
       vec_mtvscr( k1v );
       j6 = *length;
       alph2u = vec_ldl(0, &alpha[0]);
       y2u = vec_ldl(0, &y[0]);
       lore3u = vec_ldl(0, &lorentz[0]);
       lore2u = vec_perm(lore3u, lore3u, lore6u);
       for ( j1 = 0; j1 < (j6 - 4) + 1; j1 += 4 )
       {
          j3 = j1 * sizeof(int );
          j2 = j3 + 4 * sizeof(int );
          alph3u = vec_ldl(j2, &alpha[0]);
          y3u = vec_ldl(j2, &y[0]);
          alph1u = vec_perm(alph2u, alph3u, alph4u);
          alph2u = alph3u;
          y1u = vec_perm(y2u, y3u, y4u);
          y2u = y3u;
          r3v = vec_madd(r1v, alph1u, r2v);
          r4v = vec_madd(alph1u, alph1u, r2v);
          r4v = vec_madd(y1u, y1u, r4v);
          r5v = vec_re(r4v);
          r7v = vec_nmsub(r5v, r4v, r6v);
          r8v = vec_madd(r5v, r7v, r5v);
          lore1u = vec_madd(r3v, r8v, r2v);
          lore4u = vec_perm(lore2u, lore1u, lore5u);
          vec_stl(lore4u, j3, &lorentz[0]);
          lore2u = lore1u;
       }
       j8 = ((int )&lorentz[0] & 15) >> 2;
       j4 = j6 & 3;
       if ( j4 > 0 )
       {
          j3 = j1 * sizeof(int );
          j2 = j3 + 4 * sizeof(int );
          alph3u = vec_ldl(j2, &alpha[0]);
          y3u = vec_ldl(j2, &y[0]);
          alph1u = vec_perm(alph2u, alph3u, alph4u);
          y1u = vec_perm(y2u, y3u, y4u);
          r3v = vec_madd(r1v, alph1u, r2v);
          r4v = vec_madd(alph1u, alph1u, r2v);
          r4v = vec_madd(y1u, y1u, r4v);
          r5v = vec_re(r4v);
          r7v = vec_nmsub(r5v, r4v, r6v);
          r8v = vec_madd(r5v, r7v, r5v);
          lore1u = vec_madd(r3v, r8v, r2v);
          lore4u = vec_perm(lore2u, lore1u, lore5u);
          j7 = j8 + j4;
          if ( j7 >= 4 )
          {
             vec_stl(lore4u, j3, &lorentz[0]);
             if ( j7 > 4 )
             {
                lore7u = vec_ldl(j3 + 16, &lorentz[0]);
                lore4u = vec_perm(lore1u, lore1u, lore5u);
                lore8u = vec_sel(lore4u, lore7u, j1v[j7-5]);
                vec_stl(lore8u, j3 + 16, &lorentz[0]);
             }
          }
          else
          {
             lore7u = vec_ldl(j3, &lorentz[0]);
             lore8u = vec_sel(lore4u, lore7u, j1v[j7-1]);
             vec_stl(lore8u, j3, &lorentz[0]);
          }
       }
       else
       {
          j3 = j1 * sizeof(int );
          if ( j8 )
          {
             lore4u = vec_perm(lore2u, lore2u, lore5u);
             for ( j5 = 0; j5 < j8; j5++ )
                vec_ste(lore4u, j3 + (j5 - j8) * sizeof(float ), &lorentz[
                0]);
          }
       }
    }
 }
 v_vlorentz2( length, alpha, y, lorentz )
 int *length;
 float alpha[*length], y[*length], lorentz[*length];
 {
    int i;
    {
       int j9, j10, j11, j12, j13, j14, j15;
       __vector float alph5u, y5u, lore9u, r11v, r12v, r13v, r15v, r16v;
       __vector float r14v = (__vector float )(1.);
       __vector float r10v = (__vector float )(0.);
       __vector float alph6u, y6u, r17v, r18v, r19v, r20v, r21v, lore10u;
       __vector float lore11u;
       static __vector unsigned long j2v[3] =  { (__vector unsigned long )
       (0, 0XFFFFFFFF, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(
       0, 0, 0XFFFFFFFF, 0XFFFFFFFF), (__vector unsigned long )(0, 0, 0, 
       0XFFFFFFFF) } ;
       __vector signed short k2v = (__vector signed short )(0, 0, 0, 0, 0,
        0, 1, 0);
       __vector float r9v = (__vector float )(0.564189583546E0);
       vec_mtvscr( k2v );
       j14 = *length;
       for ( j9 = 0; j9 < (j14 - 4 * 2) + 1; j9 += 4 * 2 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          alph5u = vec_ldl(j11, &alpha[0]);
          y5u = vec_ldl(j11, &y[0]);
          alph6u = vec_ldl(j11 + 16, &alpha[0]);
          y6u = vec_ldl(j11 + 16, &y[0]);
          r11v = vec_madd(r9v, alph5u, r10v);
          r12v = vec_madd(alph5u, alph5u, r10v);
          r12v = vec_madd(y5u, y5u, r12v);
          r13v = vec_re(r12v);
          r15v = vec_nmsub(r13v, r12v, r14v);
          r16v = vec_madd(r13v, r15v, r13v);
          lore9u = vec_madd(r11v, r16v, r10v);
          vec_stl(lore9u, j11, &lorentz[0]);
          r17v = vec_madd(r9v, alph6u, r10v);
          r18v = vec_madd(alph6u, alph6u, r10v);
          r18v = vec_madd(y6u, y6u, r18v);
          r19v = vec_re(r18v);
          r20v = vec_nmsub(r19v, r18v, r14v);
          r21v = vec_madd(r19v, r20v, r19v);
          lore10u = vec_madd(r17v, r21v, r10v);
          vec_stl(lore10u, j11 + 16, &lorentz[0]);
       }
       for ( ; j9 < (j14 - 4) + 1; j9 += 4 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          alph5u = vec_ldl(j11, &alpha[0]);
          y5u = vec_ldl(j11, &y[0]);
          r11v = vec_madd(r9v, alph5u, r10v);
          r12v = vec_madd(alph5u, alph5u, r10v);
          r12v = vec_madd(y5u, y5u, r12v);
          r13v = vec_re(r12v);
          r15v = vec_nmsub(r13v, r12v, r14v);
          r16v = vec_madd(r13v, r15v, r13v);
          lore9u = vec_madd(r11v, r16v, r10v);
          vec_stl(lore9u, j11, &lorentz[0]);
       }
       j12 = j14 & 3;
       if ( j12 > 0 )
       {
          j11 = j9 * sizeof(int );
          j10 = j11;
          alph5u = vec_ldl(j11, &alpha[0]);
          y5u = vec_ldl(j11, &y[0]);
          r11v = vec_madd(r9v, alph5u, r10v);
          r12v = vec_madd(alph5u, alph5u, r10v);
          r12v = vec_madd(y5u, y5u, r12v);
          r13v = vec_re(r12v);
          r15v = vec_nmsub(r13v, r12v, r14v);
          r16v = vec_madd(r13v, r15v, r13v);
          lore9u = vec_madd(r11v, r16v, r10v);
          lore11u = vec_ldl(j11, &lorentz[0]);
          lore9u = vec_sel(lore9u, lore11u, j2v[j12-1]);
          vec_stl(lore9u, j11, &lorentz[0]);
       }
    }
 }
#endif

/* this for gfortran on a CBE, tested on a PS3 */

#ifdef GFORTRAN_CBE

#include <sys/times.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

double walltime_()
{
  struct tms buf;
  clock_t ticks;
  double CLK_TCK= 1.0l/(double) sysconf(_SC_CLK_TCK);


    /* Function Body */
  ticks=times(&buf);

  /* printf("tick, CLK_TCK: %ld, %lf, %lf\n",ticks,CLK_TCK, ((double) ticks)*CLK_TCK); */
  return ((double) ticks)*CLK_TCK;
}
#endif


/*******************************************************************/
/*  check if little endian or big endian and convert if necessary  */
/*******************************************************************/

#if defined(SUN)

int endian_check_(c)

#elif defined(T3E)

int ENDIAN_CHECK(c)

#endif
char *c;
{
#if defined(LITTLE_ENDIAN)
double i;
    char *p = (char *)&i;
        p[0] = c[7];
        p[1] = c[6];
        p[2] = c[5];
        p[3] = c[4];
        p[4] = c[3];
        p[5] = c[2];
        p[6] = c[1];
        p[7] = c[0];
// 
        c[0] = p[0];
        c[1] = p[1];
        c[2] = p[2];
        c[3] = p[3];
        c[4] = p[4];
        c[5] = p[5];
        c[6] = p[6];
        c[7] = p[7];
#endif 
    return 0;
}


/*************************************************************/
/* function aliases  (no T3E support anymore)
/*************************************************************/

int c_rd_list_blk_g_(fd,nrecord,list_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *list_buffer;                  /* the data structure               */
{return c_rd_list_blk_(fd,nrecord,list_buffer,ndata);}


int c_rd_list_blk_v_(fd,nrecord,list_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *list_buffer;                  /* the data structure               */
{return c_rd_list_blk_(fd,nrecord,list_buffer,ndata);}


int c_rd_scr_blk_g_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_rd_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_rd_scr_blk_v_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_rd_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_wrt_nlte_scr_blk_g_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_wrt_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata);}


int c_wrt_nlte_scr_blk_v_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_wrt_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_rd_nlte_scr_blk_g_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_rd_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_rd_nlte_scr_blk_v_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_rd_nlte_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_wrt_blk_s_(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */
{return c_wrt_blk_(fd,nrecord,f90_buffer,ndata,recsize);}

int c_wrt_blk_o_(fd,nrecord,f90_buffer,ndata,recsize)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                 /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
int *recsize;                       /* size of the structure (bytes)    */
{return c_wrt_blk_(fd,nrecord,f90_buffer,ndata,recsize);}

int c_wrt_scr_blk_g_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                       /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_wrt_scr_blk_(fd,nrecord,f90_buffer,ndata);}

int c_wrt_scr_blk_v_(fd,nrecord,f90_buffer,ndata)    
int *fd;                            /* the file descriptor for the file */
long long *nrecord;                       /* the fortran 90 record number     */
int *ndata;                         /* the size of the blocks (records) */
void *f90_buffer;                   /* the data structure               */
{return c_wrt_scr_blk_(fd,nrecord,f90_buffer,ndata);}

/*************************************************************/
/* helper function  to return the unix process ID
/*************************************************************/
int c_getpid_()
{return (int) getpid();}

/*************************************************************/
/* helper function  to get RSS etc.
/*************************************************************/
/*
 * Author:  David Robert Nadeau
 * Site:    http://NadeauSoftware.com/
 * License: Creative Commons Attribution 3.0 Unported License
 *          http://creativecommons.org/licenses/by/3.0/deed.en_US
 */

#if defined(_WIN32)
#include <windows.h>
#include <psapi.h>

#elif defined(BGQ)

#include <spi/include/kernel/memory.h>

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
#include <unistd.h>
#include <sys/resource.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>

#elif (defined(_AIX) || defined(__TOS__AIX__))
#include <fcntl.h>
#include <sys/procfs.h>

#elif (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
#include <fcntl.h>
#include <procfs.h>

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#include <stdio.h>

#endif

#else
/* #error "Cannot define getPeakRSS( ) or getCurrentRSS( ) for an unknown OS." */
#endif



/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t getPeakRSS( )
{
#if defined(_WIN32)
	/* Windows -------------------------------------------------- */
	PROCESS_MEMORY_COUNTERS info;
	GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
	return (size_t)info.PeakWorkingSetSize;

#elif defined(BGQ)
	/* BlueGene/Q ----------------------------------------------- */
   uint64_t shared, persist, heapavail, stackavail, stack, heap, guard, mmap;

   Kernel_GetMemorySize(KERNEL_MEMSIZE_SHARED, &shared);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_PERSIST, &persist);
   // Kernel_GetMemorySize(KERNEL_MEMSIZE_HEAPAVAIL, &heapavail);
   // Kernel_GetMemorySize(KERNEL_MEMSIZE_STACKAVAIL, &stackavail);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_STACK, &stack);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_HEAP, &heap);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_GUARD, &guard);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_MMAP, &mmap);

   return (size_t) mmap+guard+heap+stack+persist+shared;

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
	/* AIX and Solaris ------------------------------------------ */
	struct psinfo psinfo;
	int fd = -1;
        long pid = (long) getpid();
        char string[64];
        snprintf(string,sizeof(string),"/proc/%ld/psinfo",pid);
/*        fprintf(stderr,string); */
/*	if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 ) */
	if ( (fd = open( string, O_RDONLY )) == -1 )
		return (size_t)0L;		/* Can't open? */
	if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
	{
		close( fd );
		return (size_t)0L;		/* Can't read? */
	}
	close( fd );
	return (size_t)(psinfo.pr_rssize * 1024L);

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
	/* BSD, Linux, and OSX -------------------------------------- */
	struct rusage rusage;
	getrusage( RUSAGE_SELF, &rusage );
#if defined(__APPLE__) && defined(__MACH__)
	return (size_t)rusage.ru_maxrss;
#else
	return (size_t)(rusage.ru_maxrss * 1024L);
#endif

#else
	/* Unknown OS ----------------------------------------------- */
	return (size_t)0L;			/* Unsupported. */
#endif
}


/**
 * Returns the current resident set size (physical memory use) measured
 * in bytes, or zero if the value cannot be determined on this OS.
 */
size_t getCurrentRSS( )
{
#if defined(_WIN32)
	/* Windows -------------------------------------------------- */
	PROCESS_MEMORY_COUNTERS info;
	GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
	return (size_t)info.WorkingSetSize;

#elif defined(BGQ)
	/* BlueGene/Q ----------------------------------------------- */
   uint64_t shared, persist, heapavail, stackavail, stack, heap, guard, mmap;

   Kernel_GetMemorySize(KERNEL_MEMSIZE_SHARED, &shared);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_PERSIST, &persist);
   // Kernel_GetMemorySize(KERNEL_MEMSIZE_HEAPAVAIL, &heapavail);
   // Kernel_GetMemorySize(KERNEL_MEMSIZE_STACKAVAIL, &stackavail);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_STACK, &stack);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_HEAP, &heap);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_GUARD, &guard);
   Kernel_GetMemorySize(KERNEL_MEMSIZE_MMAP, &mmap);

   return (size_t) mmap+guard+heap+stack+persist+shared;

#elif defined(__APPLE__) && defined(__MACH__)
	/* OSX ------------------------------------------------------ */
	struct task_basic_info info;
	mach_msg_type_number_t infoCount = TASK_BASIC_INFO_COUNT;
	if ( task_info( mach_task_self( ), TASK_BASIC_INFO,
		(task_info_t)&info, &infoCount ) != KERN_SUCCESS )
		return (size_t)0L;		/* Can't access? */
	return (size_t)info.resident_size;

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
	/* Linux ---------------------------------------------------- */
	long rss = 0L;
	FILE* fp = NULL;
	if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
		return (size_t)0L;		/* Can't open? */
	if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
	{
		fclose( fp );
		return (size_t)0L;		/* Can't read? */
	}
	fclose( fp );
	return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);

#else
	/* AIX, BSD, Solaris, and Unknown OS ------------------------ */
	return (size_t)0L;			/* Unsupported. */
#endif
}

/**
 * interfaces for FORTRAN
 * this was made by PHH
 */

long c_getpeakrss_()
{return (long) getPeakRSS();}

long c_getcurrentrss_()
{return (long) getCurrentRSS();}
