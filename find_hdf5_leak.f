!*********************************************************************
! Filename:      find_hdf5_leak.f
! Author:        Eddie Baron <baron@ou.edu>
! Created at:    Tue Jun  9 17:01:43 2015
! Modified at:   Fri Jun 12 16:13:13 2015
! Modified by:   Eddie Baron <baron@ou.edu>
! Description:   test code for leak
!*********************************************************************
c--
c--
c--
      program find_hdf5_leak
      use linecom
      use hdftagsmod
      use iso_c_binding
      use dddrt_module, only: DD_WL_COMM_rank,FS_rank
      implicit none
      include 'param.inc'
      include 'physconst.inc'
      include "phx-mpi.inc"
      integer :: ierr,nsynch,id_jwrk,imaster_rt,isource,isend,irt_task
      integer :: i
      character (len=255) :: cpuname=' '
      real (kind=r_k8),allocatable :: xran(:)
      INTEGER :: hdferr                                                 !HDF5
      TYPE(C_PTR) :: f_ptr                                              !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: maxdims_in,maxdims_out          !HDF5
      
      INTEGER(HSIZE_T), DIMENSION(1) :: offset                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: counts                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: stride                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: blocks                          !HDF5
      INTEGER(HID_T) :: dataspace ! Dataspace identifier                !HDF5
      INTEGER(HID_T) :: memspace ! Memory dataspace identifier          !HDF5
      INTEGER(HID_T) :: crp_list ! Dataset creation property identifier !HDF5
      INTEGER(hsize_t) :: data_dims_out(1) ! Dataspace dimensions       !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: chunk_dims,set_size             !HDF5
      integer :: rank_out=1,rank_in
      logical :: ezl,ezlmol
      integer :: imolnlte
      integer (int_k8) :: lnendmax=3882
      integer :: icache
      integer (int_k8) :: ilong,iilong
c--
c--
c--  setup defaults for mpi task distribution
c--
c--
c-- MPI stuff
c-- before we do ANYTHING, we fire up the MPI stuff:
c--
      call mpi_init(ierr)                                           !MPI
      call mpi_comm_rank(MPI_COMM_WORLD, taskid_world, ierr)        !MPI
      call mpi_comm_size(MPI_COMM_WORLD, numtasks_world, ierr)      !MPI
      call MPI_COMM_DUP(MPI_COMM_WORLD,MyMPI_COM_CMPT, ierr)        !MPI
      call mpi_comm_rank(MyMPI_COM_CMPT, taskid, ierr)              !MPI
      call mpi_comm_size(MyMPI_COM_CMPT, numtasks, ierr)            !MPI
      call MPI_Get_processor_name(cpuname,i,ierr)                   !MPI
c--                                                                 !MPI
c-- some machines return names with char(0), replace them with blanks!MPI
c--                                                                 !MPI
      do i=1,MPI_MAX_PROCESSOR_NAME                                 !MPI
       if(cpuname(i:i) .eq. char(0)) cpuname(i:i)  = ' '            !MPI
      enddo                                                         !MPI
c--
c-- Initialize the HDF5 FORTRAN interface.
c--
      CALL h5open_f(ierr)                                       !HDF5
      linmax = 4096
      if (.not. allocated(xran)) then
       allocate(xran(linmax))
      endif
      call random_seed
!--
!-- initialize default values
!-- 
      lnbend = 1
      lnend = 1
      nworkers = 1
      nwlnodes = -1
      n_lin_mol_v = -1
      n_lin_mol_g = -1
      n_lin_atm_v = -1
      n_lin_atm_g = -1
      n_nlte_rates = 1
      n_rt = 1
      nsynch = 999999
      n_selec_atm = -1
      n_selec_mol = -1
      n_selec_fuzz = 1
!--
!-- set up task distribution
!-- 
      nworkers=numtasks
      nwlnodes=1
      n_lin_mol_v=nworkers*nwlnodes
      n_lin_mol_g=nworkers*nwlnodes
      n_lin_atm_v=nworkers*nwlnodes
      n_lin_atm_g=nworkers*nwlnodes
      n_nlte_rates=nworkers
      n_nlte_opac=nworkers
      n_rt=nworkers
      nsynch=999999
      n_nlte_grp = 0 
      n_selec_mol=999
      n_selec_atm=999
!--
!-- cache sizes
!-- 
      ncache_atm_v=1
      ncache_atm_g=400
      ncache_mol_v=1
      ncache_mol_g=1
      ncache_fuzz=200
      linmaxf=4096
      linmax=4096
      linmaxm=4096
      lin_blksize_div = 64
      ezl = .true.
      ezlmol = .false.
      imolnlte = 0

      call phx1d_dd_init        !MPI
      call line_alloc(ezl,ezlmol,imolnlte)
! memory data type voigt lines                                      !MPI
!                                                                   !MPI
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5!MPI
     &    H5OFFSETOF(C_LOC(voigt_atm(1,0)),                             !HDF5!MPI
     &   C_LOC(voigt_atm(2,0))), v_atm_tid, hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "lamel_name",                         !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%lamel)), !HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "gfex_name",                          !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gfex)),  !HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "chiel_name",                         !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%chiel)), !HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "gamnat_name",                        !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gamnat)),!HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "gam4_name",                          !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gam4)),  !HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "gam6_name",                          !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gam6)),  !HDF5!MPI
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "intl_name",                          !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%intl)),  !HDF5!MPI
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5!MPI
      CALL H5Tinsert_f(v_atm_tid, "ipad_name",                          !HDF5!MPI
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%ipad)),  !HDF5!MPI
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5!MPI
!      write(0,*)"create voigt atm mem type"                        !MPI
                                                                    !MPI
      if(id_selec_atm .eq. 0) then                                  !MPI
!                                                                   !MPI
!--                                                                 !MPI
!-- open hdf5 files these are serial writes so only the master does !MPI
!-- the open                                                        !MPI
!--                                                                 !MPI
!                                                                   !MPI
!Create a new file using default properties.                        !MPI
!                                                                   !MPI
      CALL h5fcreate_f(h5_v_atm_file_name, H5F_ACC_TRUNC_F,             !HDF5!MPI
     &   v_atm_file_id, hdferr)                                         !HDF5!MPI
!       write(0,*)"open ",h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
!                                                                   !MPI
!       We set it initially to 1 to show how you                    !MPI
!       can extend the dataset at each step. You could also set it to!MPI
!       the size of the first buffer you are going to write for     !MPI
!       instance.                                                   !MPI
                                                                    !MPI
! This is weird because it uses C order for the storage irrelevant for 1 D array!MPI
                                                                    !MPI
      maxdims_out = (/H5S_UNLIMITED_F/)                                 !HDF5!MPI
                                                                    !MPI
!                                                                   !MPI
! Create the data space with unlimited dimensions.                  !MPI
!                                                                   !MPI
!                                                                   !MPI
      data_dims_out(1) = 0                                              !HDF5!MPI
      CALL H5Screate_simple_f(rank_out, data_dims_out,                  !HDF5!MPI
     &   dataspace, hdferr,maxdims_out)                                 !HDF5!MPI
! Then create a dataset creation property list. The layout of the dataset!MPI
! have to be chunked when using unlimited dimensions. The choice of the!MPI
! chunk size affects performances, both in time and disk space. If the!MPI
! chunks are very small, you will have a lot of overhead. If they are!MPI
! too large, you might allocate space that you don't need and your files!MPI
! might end up being too large. This is a toy example so we will choose!MPI
! chunks of one line.                                               !MPI
!                                                                   !MPI
!Modify dataset creation properties, i.e. enable chunking           !MPI
!                                                                   !MPI
      CALL h5pcreate_f(H5P_DATASET_CREATE_F, crp_list, hdferr)          !HDF5!MPI
      data_dims_out(1) = linmax                                         !HDF5!MPI
      chunk_dims(:) = data_dims_out(:)                                  !HDF5!MPI
!      write(0,*)"1: ",data_dims_out,chunk_dims,linmax                  !HDF5!MPI
      CALL h5pset_chunk_f(crp_list, rank_out, chunk_dims, hdferr)       !HDF5!MPI
                                                                    !MPI
                                                                    !MPI
!                                                                   !MPI
! Create the dataset.                                               !MPI
!                                                                   !MPI
      CALL H5Dcreate_f(v_atm_file_id, v_atm_dsetname, v_atm_tid,        !HDF5!MPI
     &   dataspace, v_atm_dset_id, hdferr,crp_list)                     !HDF5!MPI
                                                                    !MPI
! Close resources. The dataset is now created so we don't need the  !MPI
! property list anymore. We don't need the file dataspace anymore because!MPI
! when the dataset will be extended, it will become invalid as it will!MPI
! still hold the previous extent. So we will have to grab the updated!MPI
! file dataspace anyway.                                            !MPI
      call h5pclose_f(crp_list,hdferr)                                  !HDF5!MPI
      CALL h5sclose_f(dataspace, hdferr)                                !HDF5!MPI
      call mpi_barrier(MPI_COMM_SELEC_ATM,ierr)                     !MPI
      else                                                          !MPI
      call mpi_barrier(MPI_COMM_SELEC_ATM,ierr)                     !MPI
      endif ! master task                                           !MPI
      if(id_selec_atm .eq. 0) then !MPI
       do lnbend=1,lnendmax
       call random_number(xran)
       voigt_atm(:linmax,0)%lamel = xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%gfex =  xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%chiel = xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%gamnat = xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%gam4 = xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%gam6 = xran !MPI
       call random_number(xran)
       voigt_atm(:linmax,0)%intl = int(xran*200.) !MPI

! We create a memory dataspace to indicate the size of our buffer in!MPI
! memory.                                                           !MPI
! We now need to extend the dataset.                                !MPI
! We create a file dataspace to indicate the size of our buffer in  !MPI
! memory.                                                           !MPI
      data_dims_out(1) = linmax                                         !HDF5!MPI
      CALL h5screate_simple_f(rank_out,data_dims_out,dataspace,hdferr)  !HDF5!MPI
      set_size(1) = lnbend*linmax                                       !HDF5!MPI
      CALL h5dset_extent_f(v_atm_dset_id,set_size,hdferr)               !HDF5!MPI
      data_dims_out(1) = linmax                                         !HDF5!MPI
      CALL h5screate_simple_f (rank_out,data_dims_out,memspace,hdferr)  !HDF5!MPI
      CALL h5dget_space_f(v_atm_dset_id,dataspace,hdferr)               !HDF5!MPI
      offset(1) = (lnbend-1)*linmax ! This is in elements of the array  !HDF5!MPI
      counts(1)  = linmax                                               !HDF5!MPI
                                                                    !MPI
      CALL h5sselect_hyperslab_f(dataspace,H5S_SELECT_SET_F,            !HDF5!MPI
     &   offset,counts,hdferr)                                          !HDF5!MPI
!                                                                   !MPI
!Write data array to dataset                                        !MPI
!                                                                   !MPI
      f_ptr = C_LOC(voigt_atm(1,0))                                     !HDF5!MPI
      CALL H5Dwrite_f(v_atm_dset_id,v_atm_tid,f_ptr,hdferr,             !HDF5!MPI
     &   memspace,dataspace)                                            !HDF5!MPI
      enddo
!--
!      call h5tclose_f(v_atm_tid,hdferr) ! can not close this for the read
      CALL h5sclose_f(dataspace,hdferr)                                 !HDF5!MPI
      CALL h5sclose_f(memspace,hdferr)                                  !HDF5!MPI
      CALL h5dclose_f(v_atm_dset_id,hdferr)                          !HDF5!MPI
      call h5fclose_f(v_atm_file_id,hdferr)
      call mpi_barrier(MPI_COMM_SELEC_ATM,ierr)                     !MPI
      else                                                          !MPI
      call mpi_barrier(MPI_COMM_SELEC_ATM,ierr)                     !MPI
      endif ! master task                                           !MPI
!-- wrote file
!-- now read it
      call meminfo("after line writes")
      lnend = lnendmax
      if(allocated(n_atm_v_block_read)) deallocate(n_atm_v_block_read)
      if(allocated(n_atm_v_block_used)) deallocate(n_atm_v_block_used)
      allocate(n_atm_v_block_used(lnend))
      allocate(n_atm_v_block_read(lnend))
      n_atm_v_block_read(:) = 0 ! how often a scratch block was read
      n_atm_v_block_used(:) = 0 ! how often was it used?
!      write(0,*)"main: bounds: ",taskid,
!     &   ubound(array=n_atm_v_block_read,dim=1)
c
      if(.not. allocated(n_atm_v_cache_read)) then
       allocate(n_atm_v_cache_used(0:ncache_atm_v-1))
       allocate(n_atm_v_cache_read(0:ncache_atm_v-1))
      endif
      n_atm_v_cache_read(:) = 0 ! how often a cache line was read
      n_atm_v_cache_used(:) = 0 ! how often was it used?
c
      v_atm_file_id = -1
      do iilong=1,10*lnendmax
       ilong = max(1_8,mod(iilong,lnendmax))
       call read_atm_v_h5(ilong,icache)
       call meminfo("after h5 read")
      enddo
      call mpi_barrier(MPI_COMM_WORLD,ierr)
      call close_v_atm_h5
      call stop_exit(0,"all done")
      
      end program find_hdf5_leak
