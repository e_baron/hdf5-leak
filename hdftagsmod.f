!*********************************************************************
! Filename:      hdftagsmod.f
! Author:        Eddie Baron <baron@ou.edu>
! Created at:    Fri Jun 28 05:43:38 2013
! Modified at:   Mon Mar 24 10:00:02 2014
! Modified by:   Eddie Baron <baron@ou.edu>
! Description:   module for hdf5 stuff
!*********************************************************************
      module hdftagsmod
      use hdf5 !HDF5

      implicit none
! KIND parameters

      INTEGER, PARAMETER :: int_k1 = SELECTED_INT_KIND(1) ! This should map to INTEGER*1 on most modern processors
      INTEGER, PARAMETER :: int_k2 = SELECTED_INT_KIND(4) ! This should map to INTEGER*2 on most modern processors
      INTEGER, PARAMETER :: int_k4 = SELECTED_INT_KIND(8) ! This should map to INTEGER*4 on most modern processors
      INTEGER, PARAMETER :: int_k8 = SELECTED_INT_KIND(16) ! This should map to INTEGER*8 on most modern processors
      
      INTEGER, PARAMETER :: r_k4 = SELECTED_REAL_KIND(5) ! This should map to REAL*4 on most modern processors
      INTEGER, PARAMETER :: r_k8 = SELECTED_REAL_KIND(10) ! This should map to REAL*8 on most modern processors
!
!      
      character(len=*), parameter ::
     &   h5_mol_filename="molec_lines_database.h5"
      character(len=*), parameter :: h5_mol_dsetname = "MolecLineList"
!
! test list for debugging
!
!      character(len=*), parameter ::
!     &   h5_mol_filename="molec_test_list.h5"
!      character(len=*), parameter ::
!     &   h5_mol_dsetname = "TEST_MolecLineList"
!      
      character(len=*), parameter ::
     &   h5_gm_file_name="gm_mollines.h5"
      character(len=*), parameter :: gm_dsetname = "GMLineList"
      character(len=*), parameter ::
     &   h5_vm_file_name="vm_mollines.h5"
      character(len=*), parameter :: vm_dsetname = "VMLineList"
      character(len=*), parameter ::
     &   h5_gm_nlte_file_name="gm_nlte_mollines.h5"
      character(len=*), parameter ::
     &   gm_nlte_dsetname = "GM_NLTE_LineList"
      character(len=*), parameter ::
     &   h5_vm_nlte_file_name="vm_nlte_mollines.h5"
      character(len=*), parameter ::
     &   vm_nlte_dsetname = "VM_NLTE_LineList"
!--
!-- atomic versions
!--
      character(len=*), parameter ::
     &   h5_atm_filename="atomic_lines_database.h5"
      character(len=*), parameter :: h5_atm_dsetname = "AtomicLineList"
!      
      character(len=*), parameter ::
     &   h5_g_atm_file_name="g_atm_lines.h5"
      character(len=*), parameter :: g_atm_dsetname = "G_ATM_LineList"
      character(len=*), parameter ::
     &   h5_v_atm_file_name="v_atm_lines.h5"
      character(len=*), parameter :: v_atm_dsetname = "V_ATM_LineList"
!      
      INTEGER(hid_t) :: s1_tid  ! datatype identifier !HDF5
      INTEGER(hid_t) :: gm_tid,vm_tid,gm_nlte_tid,vm_nlte_tid !HDF5
      INTEGER(HID_T) :: h5_mol_file_id=-1 ! File identifier !HDF5
      INTEGER(HID_T) :: gm_file_id=-1,vm_file_id=-1 !HDF5
      INTEGER(HID_T) :: gm_nlte_file_id=-1,vm_nlte_file_id=-1 !HDF5
      INTEGER(HID_T) :: gm_dset_id=-1,vm_dset_id=-1 !HDF5
      INTEGER(HID_T) :: gm_nlte_dset_id=-1,vm_nlte_dset_id=-1 !HDF5
      INTEGER(HID_T) :: h5_vm_dataspace ! Dataspace identifier !HDF5
      INTEGER(HID_T) :: h5_gm_dataspace ! Dataspace identifier !HDF5

!--
!-- atomic versions
!--
!      
      INTEGER(hid_t) :: s1_atm_tid  ! datatype identifier !HDF5
      INTEGER(hid_t) :: g_atm_tid,v_atm_tid,g_atm_nlte_tid,v_atm_nlte_tid !HDF5
      INTEGER(HID_T) :: h5_atm_file_id=-1 ! File identifier !HDF5
      INTEGER(HID_T) :: g_atm_file_id=-1,v_atm_file_id=-1 !HDF5
      INTEGER(HID_T) :: g_atm_nlte_file_id=-1,v_atm_nlte_file_id=-1 !HDF5
      INTEGER(HID_T) :: g_atm_dset_id=-1,v_atm_dset_id=-1 !HDF5
      INTEGER(HID_T) :: g_atm_nlte_dset_id=-1,v_atm_nlte_dset_id=-1 !HDF5
      INTEGER(HID_T) :: h5_v_atm_dataspace ! Dataspace identifier !HDF5
      INTEGER(HID_T) :: h5_g_atm_dataspace ! Dataspace identifier !HDF5
      
      contains
!
!
!
      subroutine close_vm_h5
!---------------------------
      integer :: hdferr
      if(vm_file_id .gt. 0) then !HDF5
      CALL h5sclose_f(h5_vm_dataspace,hdferr) !HDF5
      CALL h5dclose_f(vm_dset_id,hdferr) !HDF5
      CALL h5fclose_f(vm_file_id,hdferr) !HDF5
      vm_file_id = -1                     !HDF5
      endif                                  !HDF5
      return 
      end subroutine close_vm_h5
!
!
!
      subroutine close_gm_h5
!---------------------------
      integer :: hdferr
      if(vm_file_id .gt. 0) then !HDF5
      CALL h5sclose_f(h5_gm_dataspace,hdferr) !HDF5
      CALL h5dclose_f(gm_dset_id,hdferr)!HDF5
      CALL h5fclose_f(gm_file_id,hdferr) !HDF5
      gm_file_id = -1                     !HDF5
      endif                                  !HDF5
      return 
      end subroutine close_gm_h5
!
!
!
      subroutine close_v_atm_h5
!------------------------------
      integer :: hdferr
      if(v_atm_file_id .gt. 0) then !HDF5
       CALL h5sclose_f(h5_v_atm_dataspace,hdferr) !HDF5
       CALL h5dclose_f(v_atm_dset_id,hdferr) !HDF5
       CALL h5fclose_f(v_atm_file_id,hdferr) !HDF5
      v_atm_file_id = -1                     !HDF5
      endif                                  !HDF5
      return 
      end subroutine close_v_atm_h5
!
!
!
      subroutine close_g_atm_h5
!---------------------------
      integer :: hdferr
      if(g_atm_file_id .gt. 0) then !HDF5
      CALL h5sclose_f(h5_g_atm_dataspace,hdferr) !HDF5
      CALL h5dclose_f(g_atm_dset_id,hdferr)!HDF5
      CALL h5fclose_f(g_atm_file_id,hdferr) !HDF5
      g_atm_file_id = -1                     !HDF5
      endif                                  !HDF5
      return 
      end subroutine close_g_atm_h5

      subroutine create_atm_g_data_types
!-------------------------------------
      use iso_c_binding
      use linecom
      integer :: hdferr
!
! Create the memory data type.
! gauss lines
!
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(gauss_atm(1,0)),                             !HDF5
     &   C_LOC(gauss_atm(2,0))), g_atm_tid, hdferr)                     !HDF5
      CALL H5Tinsert_f(g_atm_tid, "lamel_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(gauss_atm(1,0)),C_LOC(gauss_atm(1,0)%lamel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(g_atm_tid, "gfex_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(gauss_atm(1,0)),C_LOC(gauss_atm(1,0)%gfex)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(g_atm_tid, "chiel_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(gauss_atm(1,0)),C_LOC(gauss_atm(1,0)%chiel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(g_atm_tid, "intl_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(gauss_atm(1,0)),C_LOC(gauss_atm(1,0)%intl)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(g_atm_tid, "ipad_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(gauss_atm(1,0)),C_LOC(gauss_atm(1,0)%ipad)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5

!      write(0,*)"create gauss atm mem type"
      return
      end subroutine create_atm_g_data_types

      subroutine create_atm_v_data_types
!-------------------------------------
      use iso_c_binding
      use linecom
      integer :: hdferr
!
! Create the memory data type.

! memory data type voigt lines
!
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(voigt_atm(1,0)),                             !HDF5
     &   C_LOC(voigt_atm(2,0))), v_atm_tid, hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "lamel_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%lamel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "gfex_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gfex)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "chiel_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%chiel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "gamnat_name",                        !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gamnat)),!HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "gam4_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gam4)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "gam6_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%gam6)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(v_atm_tid, "intl_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%intl)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(v_atm_tid, "ipad_name",                          !HDF5
     &   H5OFFSETOF(C_LOC(voigt_atm(1,0)),C_LOC(voigt_atm(1,0)%ipad)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      return
      end subroutine create_atm_v_data_types

      subroutine create_mol_g_data_types
!-------------------------------------
      use iso_c_binding
      use linecom
      integer :: hdferr
c--
c-- maybe I could include this common block in sybcom.inc
c-- However, if I do, I have to delete the following line, since
c-- sybcom.inc is also included a few lines further down ....
c-- the other "lonely" sybflag common blocks ar OK - no sybcom.inc
c-- is included where sybflag is needed.
c--
      integer :: imolnlte
      common /sybflag/ imolnlte
!
! Create the memory data type.
! gauss lines
!
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(gauss_mol(1,0)),                             !HDF5
     &   C_LOC(gauss_mol(2,0))), gm_tid, hdferr)                        !HDF5
      CALL H5Tinsert_f(gm_tid, "lamel_name",                            !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol(1,0)),C_LOC(gauss_mol(1,0)%lamel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(gm_tid, "gfex_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol(1,0)),C_LOC(gauss_mol(1,0)%gfex)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(gm_tid, "chiel_name",                            !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol(1,0)),C_LOC(gauss_mol(1,0)%chiel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(gm_tid, "intl_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol(1,0)),C_LOC(gauss_mol(1,0)%intl)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(gm_tid, "ipad_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol(1,0)),C_LOC(gauss_mol(1,0)%ipad)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5

!      write(0,*)"create gauss mol mem type"

! gauss nlte lines
!
      if(imolnlte .gt. 0) then
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                        !HDF5
     &   C_LOC(gauss_mol_nlte(2,0))), gm_nlte_tid, hdferr)              !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "lamel_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(gauss_mol_nlte(1,0)%lamel)),                             !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "big_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(gauss_mol_nlte(1,0)%bij)),                               !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "intl_name",                        !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(gauss_mol_nlte(1,0)%intl)),                              !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "lolvl_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(gauss_mol_nlte(1,0)%lolvl)),                             !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "uplvl_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5 
     &   C_LOC(gauss_mol_nlte(1,0)%uplvl)),                             !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(gm_nlte_tid, "sutran_name",                      !HDF5
     &   H5OFFSETOF(C_LOC(gauss_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(gauss_mol_nlte(1,0)%sutran)),                            !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5

      endif ! imolnlte > 0
      return
      end subroutine create_mol_g_data_types      
      subroutine create_mol_v_data_types
!-------------------------------------
      use iso_c_binding
      use linecom
      integer :: hdferr
c--
c-- maybe I could include this common block in sybcom.inc
c-- However, if I do, I have to delete the following line, since
c-- sybcom.inc is also included a few lines further down ....
c-- the other "lonely" sybflag common blocks ar OK - no sybcom.inc
c-- is included where sybflag is needed.
c--
      integer :: imolnlte
      common /sybflag/ imolnlte
!
! Create the memory data type.
! memory data type voigt lines
!
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(voigt_mol(1,0)),                             !HDF5
     &   C_LOC(voigt_mol(2,0))), vm_tid, hdferr)                        !HDF5
      CALL H5Tinsert_f(vm_tid, "lamel_name",                            !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%lamel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "gfex_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%gfex)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "chiel_name",                            !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%chiel)), !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "gamnat_name",                           !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%gamnat)),!HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "gam4_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%gam4)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "gam6_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%gam6)),  !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_tid, "intl_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%intl)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(vm_tid, "ipad_name",                             !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol(1,0)),C_LOC(voigt_mol(1,0)%ipad)),  !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
!      write(0,*)"create voigt mol mem type" 

! voigt nlte lines
!
      if(imolnlte .gt. 0) then

! memory data type voigt nlte lines
!
      CALL H5Tcreate_f(H5T_COMPOUND_F,                                  !HDF5
     &    H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                        !HDF5
     &   C_LOC(voigt_mol_nlte(2,0))), vm_nlte_tid, hdferr)              !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "lamel_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%lamel)),                             !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "bij_name",                         !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%bij)),                               !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "gamnat_name",                      !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%gamnat)),                            !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "gam4_name",                        !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%gam4)),                              !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "gam6_name",                        !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%gam6)),                              !HDF5
     &   h5kind_to_type(r_k8,H5_REAL_KIND), hdferr)                     !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "intl_name",                        !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%intl)),                              !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "lolvl_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%lolvl)),                             !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "uplvl_name",                       !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%uplvl)),                             !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      CALL H5Tinsert_f(vm_nlte_tid, "sutran_name",                      !HDF5
     &   H5OFFSETOF(C_LOC(voigt_mol_nlte(1,0)),                         !HDF5
     &   C_LOC(voigt_mol_nlte(1,0)%sutran)),                            !HDF5
     &   h5kind_to_type(int_k4,H5_INTEGER_KIND), hdferr)                !HDF5
      endif ! imolnlte > 0
      return
      end subroutine create_mol_v_data_types      
      end module hdftagsmod
