c Time-stamp: <2014/03/24 07:39 baron>
      module linecom
      implicit real*8 (a-h,o-z)
      include 'param.inc'
      private layer, nvemx, nionmx, nmolmx, npdmx, nelmx, nopmax, nopc,
     & nrest, ntab, nkmax, nrall, nsrt, nvsrt, nthdim, npgdim, mist,
     & mxst, maxel, nbmax, nbmol, maxpol, muemax, lmax, np, iw, np2,
     & maxcm, maxobs, maxapp, ntl, nlimax,layerend,z_max,
     & tma,nvemx1,nvionmx,ndust,ndust_opacities,npoints,ndust_wave
c--
c-- the infamous correction factor for C6. Well, France uses
c-- it, so do I... (the value is 10.d0**(1.8))
c-- it can be changed via input, this is the default:
c-- Jacob Bean found that 2.5 is much better in Mdwarfs!
c--
cold      real*8 :: c6corr = 63.09573448d0
      real*8 :: c6corr = 2.5d0            ! this value is better, new v16 default
c--
c-- declarations for allocatable arrays.
c--
      integer linmax,linmaxm,linmaxf
      integer ncache_mol_v,ncache_atm_g,ncache_atm_v,ncache_mol_g
      integer ncache_fuzz
c--
c-- parameters for the line lists
c--
      integer aeel
      integer*4 lin_blksize_div
c
      parameter(nbl=1024*4)
      parameter(nbl_f90=1024*16)
      parameter(iaeel=84+840,iaeold=84,aeel=iaeel)
      parameter(nmols=391)
c--
c-- type of prefac caching to use:
c-- 0: none 
c-- 1: on read of line data by read_
c-- 2: on first use by line opacity routines
c--
      integer :: prefac_cache_mode=2
      integer :: prefac_cache_mode_mol=0
c
c-- relative wordsize: 1 for Cray's (64 bit words)
c--                    2 for 32bit word machines (IBM, Sun, etc)
      parameter(iwsiz=2)
c--
c-- parameter to select standard NLTE line lambda grid (linear)
c-- or direct input grid (uses lamnlte points in data statement,
c-- see below)
c-- lingrid: .true.: use special grid (tuned for Voigt)
c--          .false.: use standard linear grid (good for Gauss)
c--
      logical lingrid
      parameter(lingrid=.false.)
c--
c-- decide if we use hdf5 for LTE lines
c-- the default is NO
c--
      logical :: use_hdf5
c--
c-- decide if we use hdf5 collective or independent calls
c-- for LTE line cache reads
c-- the default is to use collective calls (often/always faster)
c--
      logical :: line_caches_use_hdf5_collective=.true.
c--
c-- max. relative te-change for skipping line selection
c-- set to negative value to disable this feature.
c--
      parameter(skipval=0.1d0)
c--
c-- max. search and computation window for Gaussian
c-- profiles in line routines (Voigt profiles will
c-- use dlilam in A)
c--
      real*8 gausswin
c--
c-- variable to deactivate He I C6 computation. The computed
c-- value of C6 for He I is ludicrous, therefore vdW broadening 
c-- can be turned off (C6=0) for He I lines (this is the new default)
c-- This variable is also used in Cassandra (if He I is in NLTE)
c--
      logical :: He1_C6_fix=.true.
c
      parameter(maxnuc=110)
c     -- number and separation (doppler) of nlte-line points in transfer
c     parameter(npplin=25,sepn=0.25d0)
      parameter(npplin=25,sepn=0.50d0)
c     parameter(npplin=7,sepn=1.00d0)
c     -- number and separation (doppler) of lte-line points in transfer
      parameter(lpplin=0,sepl=1.00d0)
c--
c-- common for bulk lines
c-- description of data:
c-- lamel: wavelength of the line in angstroem
c-- chiel: excitation energy (lower level) * (-ln(10)), so that
c--        exp(chiel*theta) = boltzmann factor
c-- gfex: gf value of the line, multiplied with (pi*e**2)/(me*c**2),
c--       (lamel**2/c) and 1/(sqrt(pi)*lamel) (lamel in cm). The
c--       first factor is the conversion from f-values to cross-section,
c--       the second factor is the conversion from nu to lambda,
c--       the third factor the depth independent part of the normalization
c--       factor of the profile function
c-- gamnatl : the natural damping constant for the Voigt profile
c--          in the classical approximation 8pi/3*pi*e**2/(me*c)/(wl**2)
c-- gam4l   : the layer independent part of the quadratic Stark broadening
c--          damping constant : 1d-8*sqrt(dneffsq**3)
c--          where dneffsq=(chi(H I)/(Eion-Eupper))*(Zeff+1)**2
c--        to get gammastark = gam4*nelec(l)  l=layer
c-- gam6l   : the layer independent part of the Van der Waals broadening
c--          damping constant : 8.08d0*vv1*c6**(0.4)
c--          where c6=c6cons*(1.d0/(chi-chiup)**2-1.d0/(chi-chilow)**2)
c--          and vv1=0.958d0*(2.d0/rmass)**(0.3d0)
c--        to get gammavdw = gam6*kt03(l)*pertvdw(l)  l=layer
c-- zethlp: ground state population of the ion * 1/(vdoppler/c), the last
c--         part is the depth dependent part of the profile normalization
c-- vdpsqi: (1/(vdoppler/c)**2)
c-- intl: listel: code of the line (like coden)
c-- intl: pointer to the ion-number in zethlp,vdpsqi arrays
c-- nlines: number of lines stored
c-- kstart: start index for ltelin
c--
c-- The variables ending in "g" are the same but for the weak gauss lines
c--                             in the extra gauss-lines-unit
c--
c     integer listel,nlines,kstart,kstartg,intl,intlg
      integer intl,intlg
      real*8 lamel,chiel,gfex,gamnatl,gam4l,gam6l
      real*8 lamelg,chielg,gfexg
      real*8, allocatable ::  zethlp(:,:),vdpsqi(:,:)
      real*8 :: vdopmaxatm
      integer*8 :: nlines
      integer :: listel(aeel)
      integer*8 :: kstart,kstartg
      common
     &/ezlflg/ anf,ende,step,dlilam,dlimol,ielmin,ielmax,linout
c--
c-- common for the layer dependent factors that have to be multiplied
c-- to the layer independent parts of the damping constants for quadratic
c-- stark effect gam4l and the Van der Waals broadening gam6l
c--
      parameter(npert=8)
      real*8, allocatable ::  pert4l(:),pert6l(:)
      real*8, allocatable ::  pert4m(:),pert6m(:)
c--
c-- data for molecular lines:
c-- zetmol: zethlp for molecules
c-- vdsqml: vdpsqi for molecules
c-- lstmol: listel for molecules
c-- mlbegin: pointer to begin of molecular lines
c--          (should be nlines+1)
c-- mllast: index of last molecular line
c-- mlstart: kstart for molecules
c--
c     real*8, allocatable :: zetmol(:,:),vdsqml(:,:)
      real*4, allocatable :: zetmol(:,:),vdsqml(:,:)
      real*8 :: vdopmaxmol
      integer :: lstmol(nmols),mlbegin,mllast,mlstart
c
      real*8 molmas,qnuc
      common
     &/linmol / molmas(nmols),chimol(nmols),qnuc(nmols)
     &/linmol / molstg(nmols),ifacod(nmols),ibkcod(nmols)
c--
c-- common with Bob's (or France's) Molcodes and names (for ID)
c-- must follow same numbers as previous data!
c--
      parameter(nmolbob=nmols)
      dimension molcodes(nmolbob)
      character*22 molnames(nmolbob)
c
      common /bobmol/ molcodes
      common /bobmol_char/ molnames
c--
c-- flag for profile functions to use:
c-- 0: Gauss (std before 4.4)
c-- 1: Voigt, damping a' la France (4.4 and later)
c-- 2: Faster Voigt + threshold for Voigt-profiles in
c--     grelvoigt or gremvoigt respectively
c-- 3: same as 2 but distinguishes between npert different perturbers
c-- iwhprof: atomic lines, molprof: molecular lines
c-- h2olin: 0: Ludwig (default for ezlmol=.false.)
c--         1: H2O lines (only for ezlmol = .true.) either Miller
c--            or HITRAN, according to uselin
c-- hcnlin: 0: use OS tables for HCN, 1: use lines
c-- c2h2lin: 0: use OS tables for C2H2, 1: use lines
c-- gamnatvet,gam4ver,gam6ver : flags to decide which gammas to use (for iwhprof .ge. 2):
c--  0: use the approximation
c--                (classical,Kurucz/Peytremann,Unsold respectivley)
c--             1: use the provided constants in the lists (by Kurucz)
c--                gammanat,gamma4/6 per perturber and gamma6 at 10000K
c--
      integer iwhprof,molprof,h2olin,jh2o,uselin,
     &        hcnlin,c2h2lin,tioeps
      integer gamnatver,gam4ver,gam6ver
      common
     &/prfflg/ iwhprof,molprof,h2olin,jh2o,uselin(nmols)
     &/prfflg/ hcnlin,c2h2lin,tioeps
     &/prfflg/ gamnatver,gam4ver,gam6ver
c--
c-- pointers and limits if the blocked line algorithm
c-- is used
c-- lnbstart: first block number of metal lines
c-- lnstart: index of first metal line in block lnbstart
c-- mnbstart: first block number of molecular lines
c-- mnstart: index of first molecular line in block mnbstart
c-- lnbend: last block number of metal lines
c-- lnend: index of last metal line in block lnbend
c-- mnbend: last block number of molecular lines
c-- mnend: index of last molecular line in block mnbend
c--
c-- pointers for current line search window:
c-- lcbstart: first block number of metal lines
c-- lcstart: index of first metal line in block lcbstart
c-- mcbstart: first block number of molecular lines
c-- mcstart: index of first molecular line in block mcbstart
c-- lcbend: last block number of metal lines
c-- lcend: index of last metal line in block lcbend
c-- mcbend: last block number of molecular lines
c-- mcend: index of last molecular line in block mcbend
c--
c-- lcurblk: number of the current block in core memory (atoms)
c-- mcurblk: number of the current block in core memory (molecules)
c--
c-- lineunit: unit number of the buffer file (atoms)
c-- lgaussunit: unit number of the buffer file for Gauss lines (atoms)
c--             if fast Voigt lines are selected
c--
c-- pointers ending in "m" are for molecular lines
c-- pointers ending in "f" are for nlte (fuzz) lines
c-- pointers ending in "g" are for the weak gauss lines in
c--                 the extra gauss-line-unit
c--
      integer*8 :: lnbstart, lnstart, mnbstart, mnstart
      integer*8 :: lnbstartg, lnstartg, mnbstartg, mnstartg
      integer*8 :: lnbend, lnend, mnbend, mnend
      integer*8 :: lnbendg, lnendg, mnbendg, mnendg
      integer*8 :: lcbstart, lcstart, mcbstart, mcstart
      integer*8 :: lcbstartg, lcstartg, mcbstartg, mcstartg
      integer*8 :: lcbend, lcend, mcbend, mcend, lcurblk, mcurblk
      integer*8 :: lcbendg, lcendg, mcbendg, mcendg, lcurblkg, mcurblkg
      integer :: lineunit,lineunitm
      integer :: lgaussunit,lgaussunitm
      integer*8 :: lnbstartf, lnstartf
      integer*8 :: lnbendf, lnendf
      integer*8 :: lcbstartf, lcstartf
      integer*8 :: lcbendf, lcendf, lcurblkf
      integer :: lineunitf
c--
c-- common for bulk nlte (fuzz) lines
c-- description of data:
c-- lamelf: wavelenght of the line in angstroem
c-- chielf: excitation energy (lower level) * (-ln(10)), so that
c--        exp(chiel*theta) = boltzmann factor
c-- gfexf: gf value of the line, multiplied with (pi*e**2)/(me*c**2),
c--       (lamel**2/c) and 1/(sqrt(pi)*lamel) (lamel in cm). The
c--       first factor is the conversion from f-values to cross-section,
c--       the second factor is the conversion from nu to lambda,
c--       the third factor the depth independent part of the normalization
c--       factor of the profile function
c-- zethlpd: ground state population of the ion * 1/(vdoppler/c), the last
c--         part is the depth dependent part of the profile normalization
c-- intlf: listel: code of the line (like coden)
c-- intlf: pointer to the ion-number in zethlp,vdpsqi arrays
c-- nlinesf: number of lines stored
c-- kstartf: start index for ltelin
c--
      logical, allocatable ::  doit_f(:),doit_atm(:),doit_mol(:)
      integer, parameter :: doit_size=4
      integer*8 nlinesf,kstartf
      integer,allocatable,dimension(:) :: intlf
      integer*2,allocatable,dimension(:) :: klcas,kucas,icnltef
      real*8,allocatable,dimension(:) :: lamelf,chielf,gfexf
      real*8,allocatable,dimension(:,:) ::  zethlpf,vdpsqif
      real*8 :: vdopmaxatmf
      integer :: iph2
      integer, allocatable :: ipgrnd(:)
c--
c-- common with the line opacity of the individual molecular
c-- species and their respective radiative pressures.
c-- aradfac is efficiency factor for hydrostatic (input parameter)
c--
      real*8,allocatable,dimension(:,:) :: opmollin,aradmollin
      real*8,allocatable :: aradatom(:), aradmol(:),aradjola(:)
      real*8,allocatable :: aradbfff(:),aradsct(:),aradtotal(:)
      real*8,allocatable :: araddust(:)
      real*8 :: aradfac
c--
c-- common for zetel output of atoms/ions and molecules
c-- for information and/or usage for other people:
c--
      integer zetout
      common /zetout_blk/ zetout
c--
c-- counters for performance tests:
c--
      integer n_blk_mol_v_rd
      common
     &/lin_io/ n_blk_mol_v_rd,n_cache_mol_v_rd
     &/lin_io/ n_blk_mol_g_rd,n_cache_mol_g_rd
     &/lin_io/ n_blk_atm_v_rd,n_cache_atm_v_rd
     &/lin_io/ n_blk_atm_g_rd,n_cache_atm_g_rd
c--
c-- counters for Voigt function statistics:
c-- this is nonstandard, change for compilers that do not
c-- allow 64bit integers
c--
      integer*8 :: n_atm_lorentz=0 ! Lorentz wing counter, atomic lines
      integer*8 :: n_atm_polynom=0 ! polynomial branch counter, atomic lines
      integer*8 :: n_atm_complex=0 ! complex branch counter, atomic lines
      integer*8 :: n_mol_lorentz=0 ! Lorentz wing counter, molecular lines
      integer*8 :: n_mol_polynom=0 ! polynomial branch counter, molecular lines
      integer*8 :: n_mol_complex=0 ! complex branch counter, molecular lines
c--
c-- counter for scratch block usage:
c--
      integer, allocatable :: n_atm_v_block_read(:) ! how often a scratch block was read
      integer, allocatable :: n_atm_v_block_used(:) ! how often was it used?
      integer, allocatable :: n_mol_v_block_read(:) ! how often a scratch block was read
      integer, allocatable :: n_mol_v_block_used(:) ! how often was it used?
c
      integer, allocatable :: n_atm_g_block_read(:) ! how often a scratch block was read
      integer, allocatable :: n_atm_g_block_used(:) ! how often was it used?
      integer, allocatable :: n_mol_g_block_read(:) ! how often a scratch block was read
      integer, allocatable :: n_mol_g_block_used(:) ! how often was it used?
c--
c-- counter for cache line usage:
c--
      integer, allocatable :: n_atm_v_cache_read(:) ! how often a cache entry was read
      integer, allocatable :: n_atm_v_cache_used(:) ! how often was it used?
      integer, allocatable :: n_mol_v_cache_read(:) ! how often a cache entry was read
      integer, allocatable :: n_mol_v_cache_used(:) ! how often was it used?
c
      integer, allocatable :: n_atm_g_cache_read(:) ! how often a cache entry was read
      integer, allocatable :: n_atm_g_cache_used(:) ! how often was it used?
      integer, allocatable :: n_mol_g_cache_read(:) ! how often a cache entry was read
      integer, allocatable :: n_mol_g_cache_used(:) ! how often was it used?
c--
c-- parameter and common for line block caching:
c--
c-- molecular Voigt
c
      integer,allocatable :: mol_v_buf(:)
c
c-- molecular Gauss
c
      integer,allocatable ::  mol_g_buf(:)
c
c-- atomic Voigt
c
      integer,allocatable ::  atm_v_buf(:)
c
c-- atomic Gauss
c
      integer,allocatable ::  atm_g_buf(:)
c
c-- secondary NLTE lines (aka fuzz):
c
      integer,allocatable ::  fuzz_g_buf(:)
c--
c-- data structure of the line lists: (NEW)
c--
      integer(kind=4) :: nblksize,lastblk,ibsu
      integer*8 :: nstored
c--
c-- data on the files:
c--
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c--
c-- scratch space for easier line block transfers
c-- fixed to 8 word alignment (phh, 12/feb/2007)
c--
      type gauss_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl
       integer :: ipad
      end type
      integer, parameter :: gauss_line_size=3*8+2*4
c--
c-- scratch space for easier line block transfers:
c-- fixed to 8 word alignment (phh, 12/feb/2007)
c--
      type voigt_lines
       sequence
       double precision :: lamel,gfex,chiel,gamnat,gam4,gam6
       integer :: intl
       integer :: ipad
      end type
      integer, parameter :: voigt_line_size=6*8+2*4
c--
c-- store precomputed data for voigt lines here
c--
      logical, allocatable :: atm_v_prefac_valid(:,:)
      logical, allocatable :: mol_v_prefac_valid(:,:)
      integer, parameter :: prefac_valid_size=4
      integer, parameter :: prefac_size=4
      real*4,allocatable,dimension(:,:,:) :: voigt_mol_prefac
      real*4,allocatable,dimension(:,:,:) :: voigt_atm_prefac
c--
c-- scratch space for easier line block transfers
c--
      type gauss_nlte_lines
       sequence
       double precision :: lamel,bij
       integer :: intl,lolvl,uplvl,sutran
      end type
c--
c-- scratch space for easier line block transfers:
c--
      type voigt_nlte_lines
       sequence
       double precision :: lamel,bij,gamnat,gam4,gam6
       integer :: intl,lolvl,uplvl,sutran
      end type
c--
c-- scratch space for easier line block transfers:
c--
      type fuzz_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl,kucas,klcas,icnlte
      end type
      integer, parameter :: fuzz_line_size=3*8+4*4
!--
!-- these arrays have to be pointers so that c_loc
!-- can be used on them for hdf5 stuff (11/jul/2013, eab)
!-- 
c--
c-- arrays and blocks for the molecular lines:
c--
      type(gauss_lines),pointer,dimension(:,:):: gauss_mol =>null()
      type(voigt_lines),pointer,dimension(:,:):: voigt_mol =>null()
c--
c-- arrays and blocks for the atomic lines:
c--
      type(gauss_lines),pointer,dimension(:,:):: gauss_atm =>null()
      type(voigt_lines),pointer,dimension(:,:):: voigt_atm =>null()
c--
c-- arrays and blocks for the fuzz lines:
c--
      type(fuzz_lines),allocatable,dimension(:,:)::
     &   gauss_fuzz
c--
c-- data types for non grey opacity tables:
c-- (note: the opac_layer is a hardcoded 'layer', we cannot
c--  use allocatable arrays in this context with <= f95!)
c--
      integer :: wl_opac_unit = 73
      integer, parameter :: opac_layer=64
      type opac_data
       sequence
       double precision :: wl
       real, dimension(opac_layer) :: cap_cnt,sig_cnt
       real, dimension(opac_layer) :: cap_dust,sig_dust
       real, dimension(opac_layer) :: cap_atm_lin,cap_mol_lin
       real, dimension(opac_layer) :: cap_nlte
      end type
      type(opac_data) :: opacity_wl_block
c--
c-- array with the last wavelengths within each block:
c--
      parameter(maxblock_atm=100000,maxblock_mol=2 000 000)
      parameter(maxblock_atm_v=50000,maxblock_mol_v=1 000 000)
c
      real*8 last_mol_g(maxblock_mol),last_atm_g(maxblock_atm)
c-not-yet      real*8 first_mol_g(maxblock_mol),first_atm_g(maxblock_atm)
      integer :: block2cache_mol_g(maxblock_mol)
      integer block2cache_atm_g(maxblock_atm)
c
      integer :: block2cache_mol_v(maxblock_mol_v)
      integer block2cache_atm_v(maxblock_atm_v)
c
c--
c-- temporary arrays needed for parallel line selection 
c--
      type(gauss_lines), allocatable,dimension(:) :: gauss_tmp_a    !MPI
      type(gauss_lines), allocatable,dimension(:) :: gauss_tmp_m    !MPI
      type(voigt_lines), allocatable,dimension(:) :: voigt_tmp_a    !MPI
      type(voigt_lines), allocatable,dimension(:) :: voigt_tmp_m    !MPI
      type(fuzz_lines), allocatable,dimension(:) :: fuzz_tmp_g      !MPI
      type(gauss_lines) :: gauss_size                               !MPI
      type(voigt_lines) :: voigt_size                               !MPI
      type(fuzz_lines) :: fuzz_size                                 !MPI
      integer blockcounts_v(0:1),blockcounts_g(0:1)                 !MPI
      integer blockcounts_f(0:1)                                    !MPI
      integer types_v(0:1),types_g(0:1)                             !MPI
      integer types_f(0:1)                                          !MPI
c
c
c-- Stuff, needed for NLTE calculations.
c-- Logically, It should be in sybcom.inc.
c-- Semantically it is very close to some blocks in here.
c-- It uses paramters, defined in here -> therfore I don't want to
c-- put it in another file !
c--
      type(gauss_nlte_lines),pointer,dimension(:,:)::
     &   gauss_mol_nlte=>null()
      type(voigt_nlte_lines),pointer,dimension(:,:)::
     &   voigt_mol_nlte=>null()
c--
c-- The temporary ones :
c--
      type(gauss_nlte_lines),allocatable,dimension(:):: gauss_tmp_m_n!MPI
      type(voigt_nlte_lines),allocatable,dimension(:):: voigt_tmp_m_n!MPI
      type(gauss_nlte_lines) :: gauss_nlte_size                     !MPI
      type(voigt_nlte_lines) :: voigt_nlte_size                     !MPI
      integer blockcounts_v_n(0:1),blockcounts_g_n(0:1)             !MPI
      integer types_v_n(0:1),types_g_n(0:1)                         !MPI
      integer   displs_v_n(0:1),displs_g_n(0:1)                     !MPI

c      real*8,allocatable,dimension(:) :: lamelmn,bijmn
c      real*8,allocatable,dimension(:) :: gamnatmn,gam4mn,gam6mn
c      real*8,allocatable,dimension(:) :: lamelmgn,bijmgn
c      integer,allocatable,dimension(:) :: intlmn,lolvlmn
c      integer,allocatable,dimension(:) :: uplvlmn,sutranmn
c      integer,allocatable,dimension(:) :: intlmgn,lolvlmgn
c      integer,allocatable,dimension(:) :: uplvlmgn,sutranmgn
c
c     common
c    &/molstonlte/ lamelmn(linmaxm),bijmn(linmaxm)
c    &/molstonlte/ gamnatmn(linmaxm),gam4mn(linmaxm)
c    &/molstonlte/ gam6mn(linmaxm),intlmn(linmaxm)
c    &/molstonlte/ lolvlmn(linmaxm),uplvlmn(linmaxm)
c    &/molstonlte/ sutranmn(linmaxm)
c     common
c    &/molstognlte/ lamelmgn(linmaxm),bijmgn(linmaxm)
c    &/molstognlte/ intlmgn(linmaxm)                         !LIN_MOD_ON
c    &/molstognlte/ lolvlmgn(linmaxm),uplvlmgn(linmaxm)
c    &/molstognlte/ sutranmgn(linmaxm)
c--
c-- blocking of NLTE actual lines
c--
      integer :: mnbstartnlte, mnstartnlte
      integer :: mnbstartgnlte, mnstartgnlte
      integer*8 :: mnbendnlte, mnendnlte
      integer*8 :: mnbendgnlte, mnendgnlte
      integer :: mcbstartnlte, mcstartnlte
      integer :: mcbstartgnlte, mcstartgnlte
      integer :: mcbendnlte, mcendnlte, mcurblknlte
      integer :: mcbendgnlte, mcendgnlte, mcurblkgnlte
      integer :: lineunitmnlte,lgaussunitmnlte
c--
c-- counters for performance tests:
c--
      common
     &/nlte_lin_io/ n_blk_mol_v_n_rd,n_cache_mol_v_n_rd
     &/nlte_lin_io/ n_blk_mol_g_n_rd,n_cache_mol_g_n_rd
c--
c-- arrays for DKL's line handling method:
c-- dkl_line_method_used: false for standard method,
c--                       true for DKL's method
c-- wl_set(:): wavelengths point in the precomputed set
c-- opc_blk_size: number of wavelength points stored
c-- chi_mol_lin_set: extinction of the molecular lines
c--
      logical :: dkl_line_method_used=.false.
      integer :: opc_blk_size
      real*8, allocatable :: wl_set(:)
      real*8, allocatable :: chi_mol_lin_set(:,:)
c--
c-- parameter and common for line block caching:
c--
c-- molecular Voigt
      parameter(ncache_mol_v_n=3)
c
c     integer,allocatable :: intlmn_buf(:,:)
      integer,allocatable :: mol_v_n_buf(:)
c     integer,allocatable :: lolvlmn_buf(:,:)
c     integer,allocatable :: uplvlmn_buf(:,:)
c     integer,allocatable :: sutranmn_buf(:,:)
c     real*8,allocatable :: lamelmn_buf(:,:)
c     real*8,allocatable :: bijmn_buf(:,:)
c     real*8,allocatable :: gamnatmn_buf(:,:)
c     real*8,allocatable :: gam4mn_buf(:,:)
c     real*8,allocatable :: gam6mn_buf(:,:)
c     common
c    &/nlte_line_caches0/lamelmn_buf,bijmn_buf
c    &/nlte_line_caches0/gam4mn_buf,gam6mn_buf,gamnatmn_buf
c    &/nlte_line_caches1/intlmn_buf,mol_vn_buf
c    &/nlte_line_caches1/lolvlmn_buf,uplvlmn_buf
c    &/nlte_line_caches1/sutranmn_buf
c
c-- molecular Gauss
      parameter(ncache_mol_g_n=3)
c
c     integer,allocatable :: intlmgn_buf(:,:)
      integer,allocatable :: mol_g_n_buf(:)
c     integer,allocatable :: lolvlmgn_buf(:,:)
c     integer,allocatable :: uplvlmgn_buf(:,:)
c     integer,allocatable :: sutranmgn_buf(:,:)
c     real*8,allocatable :: lamelmgn_buf(:,:)
c     real*8,allocatable :: bijmgn_buf(:,:)
c     common
c    &/nlte_line_caches0/lamelmgn_buf,bijmgn_buf
c    &/nlte_line_caches1/intlmgn_buf,mol_gn_buf
c    &/nlte_line_caches1/lolvlmgn_buf,uplvlmgn_buf
c    &/nlte_line_caches1/sutranmgn_buf
c
c
c
      save
c
***********************************************************************
* interfaces for C I/O routines (cio.c) in order to catch argument 
* errors
***********************************************************************
      interface
c
      subroutine read_atm_g(irec,iunit,icache)
c     ----------------------------------------
      integer icache,iunit
      integer*8 irec
      end subroutine read_atm_g
c
      subroutine read_atm_v(irec,iunit,icache)
c     ----------------------------------------
      integer icache,iunit
      integer*8 irec
      end subroutine read_atm_v
c
      subroutine read_mol_g(irec,iunit,icache)
c     ----------------------------------------
      integer icache,iunit
      integer*8 irec
      end subroutine read_mol_g
c
      subroutine read_mol_v(irec,iunit,icache)
c     ----------------------------------------
      integer icache,iunit
      integer*8 irec
      end subroutine read_mol_v
c
      subroutine read_mol_g_h5(irec,icache)
c     -------------------------------------------
      implicit none
      integer*8 :: irec
      integer :: icache
      end subroutine read_mol_g_h5
c
      subroutine read_mol_v_h5(irec,icache)
c     -------------------------------------------
      implicit none
      integer*8 :: irec
      integer :: icache
      end subroutine read_mol_v_h5
c
      subroutine read_atm_g_h5(irec,icache)
c     ----------------------------------------
      implicit none
      integer :: icache
      integer*8 :: irec
      end subroutine read_atm_g_h5
c
      subroutine read_atm_v_h5(irec,icache)
c     ----------------------------------------
      implicit none
      integer :: icache
      integer*8 :: irec
      end subroutine read_atm_v_h5
c
c
      subroutine read_mol_g_ind_h5(irec,icache)
c     -------------------------------------------
      implicit none
      integer*8 :: irec
      integer :: icache
      end subroutine read_mol_g_ind_h5
c
      subroutine read_mol_v_ind_h5(irec,icache)
c     -------------------------------------------
      implicit none
      integer*8 :: irec
      integer :: icache
      end subroutine read_mol_v_ind_h5
c
      subroutine read_atm_g_ind_h5(irec,icache)
c     ----------------------------------------
      implicit none
      integer :: icache
      integer*8 :: irec
      end subroutine read_atm_g_ind_h5
c
      subroutine read_atm_v_ind_h5(irec,icache)
c     ----------------------------------------
      implicit none
      integer :: icache
      integer*8 :: irec
      end subroutine read_atm_v_ind_h5
c
      subroutine read_fuzz(irec,iunit,icache)
c     ---------------------------------------
      integer icache,iunit
      integer*8 irec
      end subroutine read_fuzz
c
      integer function c_read_block(nrecord,f90_output,blocksize)
c     -----------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer*8 :: nrecord
      integer(kind=4) :: blocksize
      type(spectral_line_data),dimension(*) :: f90_output
      end function c_read_block
c
c
      integer function c_write_block(fd,nrecord,f90_output,blocksize,
     &                               output)
c     ----------------------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer :: fd
      integer*8 :: nrecord
      integer :: blocksize
      type(spectral_line_data),dimension(*) :: f90_output
      type(spectral_line_data),dimension(*) :: output
      end function c_write_block
c
      integer function c_wrt_list_blk(fd,nrecord,list_buffer,ndata)
c     -------------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(spectral_line_data),dimension(*) :: list_buffer
      end function c_wrt_list_blk
c
      integer function c_rd_list_blk(fd,nrecord,list_buffer,ndata)
c     ------------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(spectral_line_data),dimension(*) :: list_buffer
      end function c_rd_list_blk
c
      integer function c_rd_blk(fd,nrecord,list_buffer,ndata,recsize)
c     ---------------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer :: fd,ndata,recsize
      integer*8 :: nrecord
      type(spectral_line_data),dimension(*) :: list_buffer
      end function c_rd_blk
c
      integer function c_rd_fuzz_blk(fd,nrecord,list_buffer,ndata)
c     ------------------------------------------------------------
      type fuzz_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl,kucas,klcas,icnlte
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(fuzz_lines),dimension(*) :: list_buffer
      end function c_rd_fuzz_blk
c
      integer function c_wrt_fuzz_blk(fd,nrecord,list_buffer,ndata)
c     ------------------------------------------------------------
      type fuzz_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl,kucas,klcas,icnlte
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(fuzz_lines),dimension(*) :: list_buffer
      end function c_wrt_fuzz_blk
c
      integer function c_line_list_info(name,nlines,iblksize,lastblk,
     &                                  bsu)
c     ------------------------------------------------------------
      character*(*) name
      integer(kind=4) :: iblksize,lastblk,bsu
      integer*8 :: nlines
      end function c_line_list_info
c
      integer function c_put_line_list_info(name,nlines,iblksize,
     &                                       lastblk,bsu)
c     ------------------------------------------------------------
      character*(*) name
      integer :: iblksize,lastblk,bsu
      integer*8 :: nlines
      end function c_put_line_list_info
c

      end interface
c
      interface c_rd_scr_blk
c
      integer function c_rd_scr_blk_g(fd,nrecord,f90_buffer,ndata)
c     ------------------------------------------------------------
      type gauss_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl
       integer :: ipad
      end type
      integer :: fd,ndata
      integer*8 :: nrecord
      type(gauss_lines),dimension(*) :: f90_buffer
      end function c_rd_scr_blk_g
c
      integer function c_rd_scr_blk_v(fd,nrecord,f90_buffer,ndata)
c     ------------------------------------------------------------
      type voigt_lines
       sequence
       double precision :: lamel,gfex,chiel,gamnat,gam4,gam6
       integer :: intl
       integer :: ipad
      end type
      integer :: fd,ndata
      integer*8 :: nrecord
      type(voigt_lines),dimension(*) :: f90_buffer
      end function c_rd_scr_blk_v
c
      end interface c_rd_scr_blk
c
      interface c_wrt_scr_blk
c
      integer function c_wrt_scr_blk_g(fd,nrecord,f90_buffer,ndata)
c     -------------------------------------------------------------
      type gauss_lines
       sequence
       double precision :: lamel,gfex,chiel
       integer :: intl
       integer :: ipad
      end type
      integer :: fd,ndata
      integer*8 :: nrecord
      type(gauss_lines),dimension(*) :: f90_buffer
      end function c_wrt_scr_blk_g
c
      integer function c_wrt_scr_blk_v(fd,nrecord,f90_buffer,ndata)
c     -------------------------------------------------------------
      type voigt_lines
       sequence
       double precision :: lamel,gfex,chiel,gamnat,gam4,gam6
       integer :: intl
       integer :: ipad
      end type
      integer :: fd,ndata
      integer*8 :: nrecord
      type(voigt_lines),dimension(*) :: f90_buffer
      end function c_wrt_scr_blk_v
c
      end interface c_wrt_scr_blk
c
      interface c_wrt_nlte_scr_blk
c
      integer function c_wrt_nlte_scr_blk_g(fd,nrecord,list_buffer,
     &                                      ndata)
c     ------------------------------------------------------------
      type gauss_nlte_lines
       sequence
       double precision :: lamel,bij
       integer :: intl,lolvl,uplvl,sutran
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(gauss_nlte_lines),dimension(*) :: list_buffer
      end function c_wrt_nlte_scr_blk_g
c
      integer function c_wrt_nlte_scr_blk_v(fd,nrecord,list_buffer,
     &                                      ndata)
c     ------------------------------------------------------------
      type voigt_nlte_lines
       sequence
       double precision :: lamel,bij,gamnat,gam4,gam6
       integer :: intl,lolvl,uplvl,sutran
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(voigt_nlte_lines),dimension(*) :: list_buffer
      end function c_wrt_nlte_scr_blk_v
c
      end interface c_wrt_nlte_scr_blk
c
      interface c_rd_nlte_scr_blk
c
      integer function c_rd_nlte_scr_blk_g(fd,nrecord,list_buffer,ndata)
c     ------------------------------------------------------------------
      type gauss_nlte_lines
       sequence
       double precision :: lamel,bij
       integer :: intl,lolvl,uplvl,sutran
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(gauss_nlte_lines),dimension(*) :: list_buffer
      end function c_rd_nlte_scr_blk_g
c
      integer function c_rd_nlte_scr_blk_v(fd,nrecord,list_buffer,ndata)
c     ------------------------------------------------------------------
      type voigt_nlte_lines
       sequence
       double precision :: lamel,bij,gamnat,gam4,gam6
       integer :: intl,lolvl,uplvl,sutran
      end type
c
      integer :: fd,ndata
      integer*8 :: nrecord
      type(voigt_nlte_lines),dimension(*) :: list_buffer
      end function c_rd_nlte_scr_blk_v
c
      end interface c_rd_nlte_scr_blk
c
      interface c_wrt_blk
c
      integer function c_wrt_blk_s(fd,nrecord,list_buffer,ndata,recsize)
c     ---------------------------------------------------------------
      type spectral_line_data
       sequence
       integer(kind=4) :: iwl
       integer(kind=2) :: ielion,ielo,igflog,igr,igs,igw
      end type 
c
      integer :: fd,ndata,recsize
      integer*8 :: nrecord
      type(spectral_line_data),dimension(*) :: list_buffer
      end function c_wrt_blk_s
c
      integer function c_wrt_blk_o(fd,nrecord,list_buffer,ndata,recsize)
c     ---------------------------------------------------------------
      integer, parameter :: opac_layer=64
      type opac_data
       sequence
       double precision :: wl
       real, dimension(opac_layer) :: cap_cnt,sig_cnt
       real, dimension(opac_layer) :: cap_dust,sig_dust
       real, dimension(opac_layer) :: cap_atm_lin,cap_mol_lin
       real, dimension(opac_layer) :: cap_nlte
      end type
c
      integer :: fd,ndata,recsize
      integer*8 :: nrecord
      type(opac_data) :: list_buffer
      end function c_wrt_blk_o
c
      end interface c_wrt_blk
c
      contains
c
      subroutine line_alloc(ezl,ezlmol,imolnlte)
c     ------------------------------------------
      implicit real*8 (a-h,o-z)
      include 'param.inc'
      include 'phx-mpi.inc'                                         !MPI
      logical ezl,ezlmol
      integer imolnlte
***********************************************************************
* allocate all arrays that lte line routines use.
* version 1.0 of 24/jan/97 by phh
* v1.1 added support for molecular NLTE (as)
*
*
***********************************************************************
      integer i,j
      real*8 :: bytes_used,total_bytes_used
c
      total_bytes_used = 0.d0
      write(*,*)
      write(*,*) 'line_alloc: allocation procedure started'
      write(*,*) 'line_alloc: parameter dump:'
      write(*,*) 'linmax=',linmax,' linmaxm=',linmaxm,
     &           ' linmaxf=',linmaxf
      write(*,*) 'ncache_atm_v=',ncache_atm_v,
     &           ' ncache_atm_g=',ncache_atm_g,
     &           ' ncache_mol_v=',ncache_mol_v,
     &           ' ncache_mol_g=',ncache_mol_g,
     &           ' ncache_fuzz=',ncache_fuzz
      if(ezl .and. linmax .gt. 0) then
       write(*,*) 'allocating and resetting atomic line arrays:'
       allocate(doit_atm(linmax))
       allocate(gauss_atm(linmax,0:ncache_atm_g-1))
       allocate(voigt_atm(linmax,0:ncache_atm_v-1))
       allocate(atm_v_buf(0:ncache_atm_v-1))
       allocate(atm_g_buf(0:ncache_atm_g-1))
c
       bytes_used = linmax*doit_size+
     &  linmax*ncache_atm_g*gauss_line_size+
     &  linmax*ncache_atm_v*voigt_line_size+
     &  4*ncache_atm_v+4*ncache_atm_g
       total_bytes_used = total_bytes_used+bytes_used
       write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'atomic line cache storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
c
       if(prefac_cache_mode .ne. 0) then
        write(*,*) 'allocating atm prefac caches ...'
        allocate(atm_v_prefac_valid(linmax,0:ncache_atm_v-1))
        allocate(voigt_atm_prefac(layer,linmax,0:ncache_atm_v-1))
        bytes_used = prefac_size*ncache_atm_v*linmax*layer+
     &               prefac_valid_size*ncache_atm_v*linmax
        total_bytes_used = total_bytes_used+bytes_used
        write(*,'(1x,a40,2(1p,d12.3,a3))')
     &   'atomic line prefac cache storage:',
     &   bytes_used/1024.d0**2,' MB',
     &   bytes_used/1024.d0**3,' GB'
       else
        write(*,*) 'atm prefac caches not needed, allocation skipped'
       endif
c
       gauss_atm%lamel = 0.d0
       gauss_atm%gfex = 0.d0
       gauss_atm%chiel = 0.d0
       gauss_atm%intl = 0
       voigt_atm%lamel = 0.d0
       voigt_atm%gfex = 0.d0
       voigt_atm%chiel = 0.d0
       voigt_atm%gamnat = 0.d0
       voigt_atm%gam4 = 0.d0
       voigt_atm%gam6 = 0.d0
       voigt_atm%intl = 0
       doit_atm = .false.
       atm_v_buf = 0
       atm_g_buf = 0
      endif
      if(ezlmol .and. linmaxm .gt. 0) then
       write(*,*) 'allocating and resetting molecular line arrays:'
       allocate(doit_mol(linmaxm))
       allocate(mol_v_buf(0:ncache_mol_v-1))
       allocate(mol_g_buf(0:ncache_mol_g-1))
       allocate(gauss_mol(linmaxm,0:ncache_mol_g-1))
       allocate(voigt_mol(linmaxm,0:ncache_mol_v-1))
c
       bytes_used = linmaxm*doit_size+
     &  linmaxm*ncache_mol_g*gauss_line_size+
     &  linmaxm*ncache_mol_v*voigt_line_size+
     &  4*ncache_mol_v+4*ncache_mol_g+
     &  4*ncache_mol_g+4*ncache_mol_v
       total_bytes_used = total_bytes_used+bytes_used
       write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'molecular line cache storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
c
       if(prefac_cache_mode_mol .ne. 0) then
        write(*,*) 'allocating mol prefac caches ...'
        allocate(mol_v_prefac_valid(linmaxm,0:ncache_mol_v-1))
        allocate(voigt_mol_prefac(layer,linmaxm,0:ncache_mol_v-1))
        bytes_used = prefac_size*ncache_mol_v*linmaxm*layer+
     &               prefac_valid_size*ncache_mol_v*linmaxm
        total_bytes_used = total_bytes_used+bytes_used
        write(*,'(1x,a40,2(1p,d12.3,a3))')
     &   'molecular line prefac cache storage:',
     &   bytes_used/1024.d0**2,' MB',
     &   bytes_used/1024.d0**3,' GB'
       else
        write(*,*) 'mol prefac caches not needed, allocation skipped'
       endif
c
       gauss_mol%lamel = 0.d0
       gauss_mol%gfex = 0.d0
       gauss_mol%chiel = 0.d0
       gauss_mol%intl = 0
       voigt_mol%lamel = 0.d0
       voigt_mol%gfex = 0.d0
       voigt_mol%chiel = 0.d0
       voigt_mol%gamnat = 0.d0
       voigt_mol%gam4 = 0.d0
       voigt_mol%gam6 = 0.d0
       voigt_mol%intl = 0
       doit_mol = .false.
       mol_v_buf = 0
       mol_g_buf = 0
      endif
      if(linmaxf .gt. 0) then
       write(*,*) 'allocating and resetting NLTE fuzz line arrays:'
       allocate(gauss_fuzz(linmaxf,0:ncache_fuzz-1))
       allocate(fuzz_g_buf(0:ncache_fuzz-1))
       allocate(doit_f(linmaxf))
       gauss_fuzz%lamel = 0.d0
       gauss_fuzz%gfex = 0.d0
       gauss_fuzz%chiel = 0.d0
       gauss_fuzz%intl = 0
       gauss_fuzz%klcas = 0
       gauss_fuzz%kucas = 0
       gauss_fuzz%icnlte = 0
       doit_f = .false.
c
       bytes_used = linmaxf*ncache_fuzz*fuzz_line_size+
     &  linmaxf*doit_size+ncache_fuzz*4
       total_bytes_used = total_bytes_used+bytes_used
       write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'fuzz line cache storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
      endif
       if(do_mpi .and. n_selec_atm .gt. 1) then                     !MPI
       allocate(gauss_tmp_a(linmax))                                !MPI
       allocate(voigt_tmp_a(linmax))                                !MPI
       endif                                                        !MPI
       if(do_mpi .and. n_selec_mol .gt. 1) then                     !MPI
       allocate(gauss_tmp_m(linmaxm))                               !MPI
       allocate(voigt_tmp_m(linmaxm))                               !MPI
       endif                                                        !MPI
c--
c-- The same for molecular NLTE stuff
c--
      if(ezlmol .and. linmaxm .gt. 0 .and.
     &   imolnlte .ne. 0) then
        write(*,*) 'allocating and resetting molecular nlte line arrays'
        allocate(gauss_mol_nlte(linmaxm,0:ncache_mol_g_n-1))
        allocate(voigt_mol_nlte(linmaxm,0:ncache_mol_v_n-1))
        if(do_mpi .and. n_selec_mol .gt. 1) then                    !MPI
        allocate(gauss_tmp_m_n(linmaxm))                            !MPI
        allocate(voigt_tmp_m_n(linmaxm))                            !MPI
        endif                                                       !MPI
        gauss_mol_nlte%lamel = 0.d0
        gauss_mol_nlte%bij = 0.d0
        gauss_mol_nlte%lolvl = 0
        gauss_mol_nlte%uplvl = 0
        gauss_mol_nlte%sutran = 0
        gauss_mol_nlte%intl = 0
        voigt_mol_nlte%lamel = 0.d0
        voigt_mol_nlte%bij = 0.d0
        voigt_mol_nlte%gamnat = 0.d0
        voigt_mol_nlte%gam4 = 0.d0
        voigt_mol_nlte%gam6 = 0.d0
        voigt_mol_nlte%lolvl = 0
        voigt_mol_nlte%uplvl = 0
        voigt_mol_nlte%sutran = 0
        voigt_mol_nlte%intl = 0
        allocate(mol_v_n_buf(0:ncache_mol_v_n-1))
        mol_v_n_buf = 0
        allocate(mol_g_n_buf(0:ncache_mol_g_n-1))
        mol_g_n_buf = 0
cSU     doit_mol = .false.
      endif
c
c   allocate vars from /ezl/
      allocate(zethlp(layer,aeel))
      allocate(vdpsqi(layer,aeel))
c  from /ezlmol/
      allocate(zetmol(layer,nmols))
      allocate(vdsqml(layer,nmols))
c
      bytes_used = layer*nmols*2*8+layer*aeel*2*8
      total_bytes_used = total_bytes_used+bytes_used
      write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'atm+mol zeta+vdpsqi storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
c  from /pert4damp/                 
      allocate(pert4l(layer))
      allocate(pert6l(layer))
      allocate(pert4m(layer))
      allocate(pert6m(layer))
c
      bytes_used = layer*8*4
      total_bytes_used = total_bytes_used+bytes_used
      write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'atm+mol perturber storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
c  from /ezlfuzz/
      allocate(zethlpf(layer,aeel))
      allocate(vdpsqif(layer,aeel))
      allocate(ipgrnd(aeel))
c
      bytes_used = layer*aeel*2*8+aeel*4
      total_bytes_used = total_bytes_used+bytes_used
      write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'fuzz zeta+vdpsqi storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
c  from /leviathan/
c allocate opmollin&aradmollin in phoenix main if necessary. 
c     allocate(opmollin(layer,nmols),aradmollin(layer,nmols))
      allocate(aradatom(layer),aradmol(layer),aradjola(layer))
      allocate(aradbfff(layer),aradsct(layer),aradtotal(layer))
      allocate(araddust(layer))
c
      bytes_used = layer*nmols*8*2+layer*8*7
      total_bytes_used = total_bytes_used+bytes_used
      write(*,'(1x,a40,2(1p,d12.3,a3))')
     &  'arad storage:',
     &  bytes_used/1024.d0**2,' MB',
     &  bytes_used/1024.d0**3,' GB'
c
      aradtotal(1:layer)=0.d0
c ejl
c--
c-- allocate arrays for DKL's line treatment method:
c--
      if(dkl_line_method_used) then
       write(*,*)
       write(*,*) "allocating DKL variables:"
       write(*,*) "opc_blk_size=",opc_blk_size
       allocate(wl_set(opc_blk_size))
       allocate(chi_mol_lin_set(layer,opc_blk_size))
      endif
c
      write(*,'(1x,a40,2(1p,d12.3,a3))')
     & 'total line storage:',
     & total_bytes_used/1024.d0**2,' MB',
     & total_bytes_used/1024.d0**3,' GB'
      write(*,*) 'line_alloc: allocation and reset complete!'
      return
      end subroutine line_alloc
CLIN-MOD-OFF      subroutine line_dummy                                 !LIN-MOD-OFF
CLIN-MOD-OFF      end subroutine line_dummy                             !LIN-MOD-OFF
      end module linecom
