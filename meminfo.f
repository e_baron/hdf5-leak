      subroutine meminfo(text)
c     ------------------------
      implicit none
      character, intent(in) :: text*(*)
      include 'phx-mpi.inc'                                         !MPI
***********************************************************************
* prints time and memory usage
***********************************************************************
c
      real*8, external :: sekund
      integer, external :: c_getpid
      integer*8, external :: c_getPeakRSS,c_getCurrentRSS
      integer*8 :: PeakRSS,CurrentRSS,PeakRSS_old=0
      integer :: pid,ncall
      real*8 :: timer,timer0
      logical :: first=.true.
      logical :: print_meminfo=.true.
c
      save
c
      if(.not. print_meminfo) then
       if(first) then
        first = .false.
        write(*,*)
        write(*,*) 'meminfo printout OFF'
        write(*,*) 'set print_meminfo=t in namelist to activate'
        write(*,*)
       endif
       return ! do nothing ....
      endif
c
      if(first) then
       first = .false.
       timer0 = -sekund()
       pid = c_getpid()
       ncall = 0
      endif
      
      timer = timer0+sekund()
      PeakRSS = c_getPeakRSS()
      CurrentRSS = c_getCurrentRSS()
c
      write(6,*)
      write(6,20) ncall,timer,
     &   1.d0*(PeakRSS-PeakRSS_old),
     &   PeakRSS/1024.d0**2,PeakRSS/1024.d0**3,        ! in MB and GB
     &   CurrentRSS/1024.d0**2,CurrentRSS/1024.d0**3,  ! in MB and GB
     &   trim(text)
coff cMPI      write(0,21) taskid_world,ncall,timer,sekund(),       !MPI
coff cMPI     &   trim(text)                                        !MPI
c
      write(6,*)
c
      ncall = ncall + 1
      timer0 = -sekund()
      PeakRSS_old = PeakRSS
      return
c
   20 format (1x,'meminfo: call no. ',i6,
     &        ' Delta t:',f14.2,
     &        ' s, mem difference:',es26.12e3,
     &        ' bytes, Peak RSS:',
     &         f10.2,' MB or ',f10.2,' GB, Current RSS:',
     &         f10.2,' MB or ',f10.2,' GB, Note: ',a)
   21 format (1x,i8,': meminfo: call no. ',i6,
     &        ' wallclock:',f14.2,
     &        ' s, clock:',f14.2,
     &        ' s, Note: ',a)
      end
c
c
c
      double precision function sekund()
c     ----------------------------------
      implicit none
      include 'phx-mpi.inc'                                         !MPI
c
      logical first
c
      integer, parameter :: long  = selected_int_kind(15)
      integer (long) istart, itime, itotal
      integer, dimension(8) :: dtarray
      integer :: icount,ihz,ilast,imax,imlen
      integer :: imonth
      real*8 :: scale
c
      save
c
      data first /.true./
      data itime, ilast, imlen, imonth/ 3*0,-1 /
c
      sekund = mpi_wtime()                                          !MPI
      return                                                        !MPI
c
      if(.not. first) then 
       call system_clock(icount)
       
      else    
       first = .false. 
       call system_clock(COUNT=icount, COUNT_RATE=ihz, COUNT_MAX=imax)
       scale = 1.d0/dble(ihz)
       call date_and_time(VALUES=dtarray)
       imlen = MAX(imlen, dtarray(3))
       istart = dtarray(7)+dtarray(6)*60+dtarray(5)*3600 
     &      + (dtarray(3)+imonth)*86400
       istart = istart*ihz+dtarray(8)*ihz/1000 - icount
      endif   
c
      if(icount .lt. ilast) then
       ilast = icount
       call date_and_time(VALUES=dtarray)
       imlen = MAX(imlen, dtarray(3))
c     imonth keeps a continuous count of days, check if we have to 
c     advance it by 1 month: 
c     if the day of the month dtarray(3) has just changed to 1, 
c     assume imlen is set to the last day of the previous month and 
c     add this to the running count of days.
c     Not that this is going to happen very often...
       if((dtarray(3) .eq. 1) .and. (imlen .gt. 27)) then 
          imonth = imonth + imlen
          imlen = 1
       endif
c     Absoft's system_clock apparently takes some time to reset after 
c     a rollover. Just calling it a second time clears it.
       call system_clock(icount)
       itime = dtarray(7)+dtarray(6)*60+dtarray(5)*3600 
     &      + (dtarray(3)+imonth)*86400
       itime = itime*ihz + dtarray(8)*ihz/1000 -istart - icount
c      write(6,19) ilast, icount, dtarray(5), dtarray(6), dtarray(7),
c    &        itime
      endif
      ilast = icount
      itotal = itime + icount 
      sekund = dble(itotal) * scale
      return
 19   format (1x,'sekund: clock rollover ->', i9, '->', i9, ' at ', 
     &        i02,':',i02,':',i02,' sekund: ', g16.0,'(',g16.0,')')
  
      end
      subroutine svexp(x,result,length)
c     -------------------------------------
***********************************************************************
* standard f90 implementation of single prec. vector exponential
* for machines without optimized library version.
* version 1.0 of 29/Jan/2004 by PHH
* interface is obvious.
***********************************************************************
      implicit none
      integer length,i
      real*4 x(length),result(length)
c
      do i=1,length
       if(x(i) .lt. -70.0) then
        result(i) = 0.0
       else
        result(i) = exp(x(i))
       endif
      enddo   
c
      return
      end
