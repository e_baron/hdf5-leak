!*********************************************************************
! Filename:      mpi_helper_routines.f
! Author:        Eddie Baron <baron@ou.edu>
! Created at:    Tue Feb 11 10:02:45 2014
! Modified at:   Tue Feb 11 10:11:45 2014
! Modified by:   Eddie Baron <baron@ou.edu>
! Description:   
!*********************************************************************
c
c
c                                                                   !MPI
      subroutine get_rank(comm,rank,ierr)                           !MPI
c     --------------------------------                              !MPI
      implicit none                                                 !MPI
      include 'mpif.h'                                              !MPI
      integer, intent(in) :: comm                                   !MPI
      integer, intent(out) :: rank,ierr                             !MPI
c                                                                   !MPI
      if(comm .ne. MPI_COMM_NULL) then                              !MPI
       call mpi_comm_rank(comm,rank,ierr)                           !MPI
      else                                                          !MPI
       rank = MPI_UNDEFINED                                         !MPI
      endif                                                         !MPI
      return                                                        !MPI
      end subroutine get_rank                                       !MPI
c                                                                   !MPI
      subroutine get_size(comm,size,ierr)                           !MPI
c     --------------------------------                              !MPI
      implicit none                                                 !MPI
      include 'mpif.h'                                              !MPI
      integer, intent(in) :: comm                                   !MPI
      integer, intent(out) :: size,ierr                             !MPI
c                                                                   !MPI
      if(comm .ne. MPI_COMM_NULL) then                              !MPI
       call mpi_comm_size(comm,size,ierr)                           !MPI
      else                                                          !MPI
       size = 0                                                     !MPI
      endif                                                         !MPI
      return                                                        !MPI
      end subroutine get_size                                       !MPI
