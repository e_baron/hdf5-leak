***********************************************************************
* include file with definitions and declarions for PHOENIX's
* MPI interface
* Time-stamp: <2008/01/18 16:51 baron>
***********************************************************************
      include 'mpif.h'
      integer, parameter :: MPI_DP=MPI_DOUBLE_PRECISION
cPSCL      real*8 :: mpi_wtime_
c
      character*255, parameter :: local_scratch='/scratch'
c
      integer master
      integer njobs
c     integer mpi_status_size
      integer job_cnt_op
      integer job_jola_op
      integer job_wlpk
      integer job_nlte_opac
      integer job_nlte_rates
      integer job_nlte_fuzz
      integer job_rt
      integer job_lin_atm_v
      integer job_lin_atm_g
      integer job_lin_mol_v
      integer job_lin_mol_g
      integer job_selec_mol
      integer job_selec_atm
      integer job_selec_fuzz
      integer itag_rtdat
      integer max_casgrp_mpi
      integer nwlnodes
      integer mpi_comm_new
      integer mpi_grp_new
      integer id_caswl
      integer n_caswl
      integer n_nlte_group
      integer id_nlte_group
      integer my_nlte_level_count
      integer id_selec_fuzz
      integer id_selec_mol
      integer id_selec_atm
      integer id_rt2rates
      integer id_rt
      integer id_nlte_rates
      integer id_nlte_opac
      integer id_lin_atm_g
      integer id_lin_atm_v
      integer id_lin_mol_v
      integer jobtask
      integer ncmwlpt
      integer jwlpt
      integer nworkers
      integer id_lin_mol_g
      integer id_slu
c--
c-- tag names for the different jobs:
c--
      integer, parameter :: TAG_cnt_op       = 1
      integer, parameter :: TAG_jola_op      = 2
      integer, parameter :: TAG_wlpk         = 3
      integer, parameter :: TAG_nlte_opac    = 4
      integer, parameter :: TAG_nlte_rates   = 5
      integer, parameter :: TAG_nlte_fuzz    = 6
      integer, parameter :: TAG_rt           = 7
      integer, parameter :: TAG_lin_atm_v    = 8
      integer, parameter :: TAG_lin_atm_g    = 9
      integer, parameter :: TAG_lin_mol_v    =10
      integer, parameter :: TAG_lin_mol_g    =11
      integer, parameter :: TAG_selec_mol    =12
      integer, parameter :: TAG_selec_atm    =13
      integer, parameter :: TAG_selec_fuzz   =14
c--
c-- flags for sending mode to use:
c-- 0: non_blocking operation
c-- 1: persistent operation
c-- 2: blocking operation
c
      integer send_mode,recv_mode
      parameter(send_mode=2,recv_mode=2)
      parameter(MASTER=0)
      parameter(njobs=14)
      integer MPI_gauss_lines,MPI_voigt_lines
      integer MPI_gauss_nlte_lines,MPI_voigt_nlte_lines
c
      logical do_mpi
      integer :: taskid_world,numtasks_world
      integer taskid,mpi_status(MPI_STATUS_SIZE,njobs,4)
      integer request(4,njobs),numtasks
      integer nratcom
      integer n_lin_mol_v
      integer n_lin_atm_v
      integer n_lin_mol_g
      integer n_lin_atm_g
      integer n_nlte_opac
      integer n_nlte_rates
      integer n_rt
      integer n_selec_atm
      integer n_selec_mol
      integer n_selec_fuzz
      common
     &/phx_mpi/taskid_world,numtasks_world
     &/phx_mpi/taskid,numtasks,request
     &/phx_mpi/nratcom
     &/phx_mpi/n_lin_mol_v
     &/phx_mpi/n_lin_atm_v
     &/phx_mpi/n_lin_mol_g
     &/phx_mpi/n_lin_atm_g
     &/phx_mpi/n_nlte_opac
     &/phx_mpi/n_nlte_rates
     &/phx_mpi/n_rt
     &/phx_mpi/n_selec_atm
     &/phx_mpi/n_selec_mol
     &/phx_mpi/n_selec_fuzz
     &/phx_mpi/MPI_gauss_lines,MPI_voigt_lines
     &/phx_mpi/MPI_gauss_nlte_lines,MPI_voigt_nlte_lines
     &/phx_mpi/do_mpi
c--
c-- some odd symbolic TAGs for various chitchat:
c--
      parameter(ITAG_RTDAT       =100)
c--
c-- declarations of the group communicator handles
c--
      integer MyMPI_COM_CMPT
      integer MPI_GROUP_WORLD,MPI_GROUP_RAT,MPI_COMM_RAT
      integer MPI_COMM_LIN_MOL_V
      integer MPI_COMM_LIN_ATM_V
      integer MPI_COMM_LIN_MOL_G
      integer MPI_COMM_LIN_ATM_G
      integer MPI_COMM_SELEC_ATM
      integer MPI_COMM_SELEC_MOL
      integer MPI_COMM_SELEC_FUZZ
      integer MPI_COMM_NLTE_OPAC
      integer MPI_COMM_NLTE_RATES
      integer MPI_COMM_RT
      integer MPI_COMM_RT2RATES
      integer MPI_COMM_CASWL
c--
c-- common with the groups (will be needed in
c-- different modules!)
c--
      common
     &/phx_mpi_grps/MPI_GROUP_WORLD
c--
c-- common with the communicators (will be needed in
c-- different modules!)
c--
      common 
     &/phx_mpi_comms/ MyMPI_COM_CMPT
     &/phx_mpi_comms/ MPI_COMM_RAT
     &/phx_mpi_comms/ MPI_COMM_LIN_MOL_V,MPI_COMM_LIN_ATM_V
     &/phx_mpi_comms/ MPI_COMM_LIN_MOL_G,MPI_COMM_LIN_ATM_G
     &/phx_mpi_comms/ MPI_COMM_NLTE_OPAC,MPI_COMM_NLTE_RATES
     &/phx_mpi_comms/ MPI_COMM_RT,MPI_COMM_RT2RATES
     &/phx_mpi_comms/ MPI_COMM_CASWL
     &/phx_mpi_comms/ MPI_COMM_SELEC_ATM,MPI_COMM_SELEC_MOL
     &/phx_mpi_comms/MPI_COMM_SELEC_FUZZ
c--
c-- common with the taskids of the different jobs
c-- 
      common
     &/task_ids/id_lin_mol_v
     &/task_ids/id_lin_atm_v
     &/task_ids/id_lin_mol_g
     &/task_ids/id_lin_atm_g
     &/task_ids/id_nlte_opac
     &/task_ids/id_nlte_rates
     &/task_ids/id_rt,id_rt2rates
     &/task_ids/id_selec_atm,id_selec_mol,id_selec_fuzz
c
      common
     &/task_ids_gbl/JOB_cnt_op,JOB_jola_op,JOB_wlpk,JOB_nlte_opac
     &/task_ids_gbl/JOB_nlte_rates,JOB_nlte_fuzz,JOB_rt
     &/task_ids_gbl/JOB_lin_atm_v,JOB_lin_atm_g,JOB_lin_mol_v
     &/task_ids_gbl/JOB_lin_mol_g,JOB_selec_mol,JOB_selec_atm
     &/task_ids_gbl/JOB_selec_fuzz
c
c--
c-- flags for line scratch file access
c-- plan9_???_? = .true. : use IBM piofs to distribute load
c--             = .false.: use standard reads and internal parallelism
c--
      logical plan9_mol_g,plan9_mol_v,plan9_atm_g,plan9_atm_v
      parameter(plan9_mol_g=.false.)
      parameter(plan9_mol_v=.false.)
      parameter(plan9_atm_g=.false.)
      parameter(plan9_atm_v=.false.)
c--
c-- flags for Gauss line parallelization:
c-- molecular lines:
c-- mollin_par_flg = .true. allow || over the lines
c--                  .false. always use || over radial points
c-- atomic lines:
c-- ltelin_par_flg = .true. allow || over the lines
c--                  .false. always use || over radial points
c
      logical ltelin_par_flg,mollin_par_flg
      parameter(ltelin_par_flg=.false.,mollin_par_flg=.false.)
c--
c-- block to distribute NLTE groups onto nodes:
c--
      parameter(max_casgrp_mpi=500)
      integer n_nlte_grp,n_nlte_grp_nodes,my_nlte_group
      common
     &/mpi_cas_dist/n_nlte_grp,n_nlte_grp_nodes(max_casgrp_mpi)
     &/mpi_cas_dist/my_nlte_group,my_nlte_level_count,id_nlte_group
     &/mpi_cas_dist/n_nlte_group
     &/mpi_cas_dist/n_caswl,id_caswl
c--
c-- blocks for wavelength loop parallelization
c--
      integer MPI_COMM_WRKRS
      integer MPI_COMM_WLP
      common 
     &/mpi_wl_grps/ MPI_COMM_WRKRS
     &/mpi_wl_grps/ MPI_COMM_WLP,MPI_GRP_NEW,MPI_COMM_NEW
      common
     &/mpi_wl_stff/ nworkers,nwlnodes,jwlpt,ncmwlpt
