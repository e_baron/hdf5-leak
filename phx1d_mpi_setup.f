      subroutine phx1d_dd_init
c     ------------------------
      implicit none
***********************************************************************
* initialized the domain decomposition and data transfer for 
* phoenix/1D. This routine distributes nRT_per_wl_cluster tasks for
* 3DRT on each 'node' (set of cores that share memory) and creates
* the necessary MPI groups etc. 
*
* version 1.0 of 08/Mar/2010 by PHH & EAB
*
* changes 
* 08/Mar/2010: cloned from phx1d_dd_init (phx/3D) to replace awful phx_mpi_init
***********************************************************************
c
      include 'phx-mpi.inc'                                         !MPI
c                                                                   !MPI
      integer i,j,ierr,color,key                                    !MPI
      integer len_name,nRT_total                                    !MPI
      integer, allocatable :: vector(:)                             !MPI
      character(len=MPI_MAX_PROCESSOR_NAME) :: cpuname              !MPI
      character(len=MPI_MAX_PROCESSOR_NAME),                        !MPI
     &               allocatable :: name_vector(:)                  !MPI
c                                                                   !MPI
      integer, allocatable :: wrk_ID_vec(:)                         !MPI
      integer, allocatable :: nworkers_vec(:)                       !MPI
      integer, allocatable :: wl_ID_vec(:)                          !MPI
      integer, allocatable :: n_WL_cluster_vec(:)                   !MPI
      integer, allocatable :: FS_rank_v(:)                          !MPI
      integer, allocatable :: FS_size_v(:)                          !MPI
      integer, allocatable :: id_lin_mol_v_vec(:)                   !MPI
      integer, allocatable :: id_lin_atm_v_vec(:)                   !MPI
      integer, allocatable :: id_lin_mol_g_vec(:)                   !MPI
      integer, allocatable :: id_lin_atm_g_vec(:)                   !MPI
      integer, allocatable :: id_nlte_opac_vec(:)                   !MPI
      integer, allocatable :: id_nlte_rates_vec(:)                  !MPI
      integer, allocatable :: id_rt_vec(:)                          !MPI
      integer, allocatable :: id_rt2rates_vec(:)                    !MPI
      integer, allocatable :: id_selec_atm_vec(:)                   !MPI
      integer, allocatable :: id_selec_mol_vec(:)                   !MPI
      integer, allocatable :: id_selec_fuzz_vec(:)                  !MPI
      integer, allocatable :: n_lin_mol_v_vec(:)                    !MPI
      integer, allocatable :: n_lin_mol_g_vec(:)                    !MPI
      integer, allocatable :: n_lin_atm_g_vec(:)                    !MPI
      integer, allocatable :: n_lin_atm_v_vec(:)                    !MPI
      integer, allocatable :: n_nlte_rates_vec(:)                   !MPI
      integer, allocatable :: n_nlte_opac_vec(:)                    !MPI
      integer, allocatable :: n_rt_vec(:)                           !MPI
c
      integer :: MyMPI_COMM_WL_CLUSTER,MyMPI_GROUP_WL_CLUSTER       !MPI
      integer :: MyMPI_COMM_FS,MyMPI_GROUP_FS                       !MPI
      integer :: MyMPI_COMM_SCRATCH                                 !MPI
c
      integer :: procs_per_wl_cluster,size_of_wl_cluster            !MPI
      integer :: my_wl_cluster,id_tst,id_jwrk,id_jwlp               !MPI
      integer :: wl_ID,fs_size,FS_rank                              !MPI
      integer :: procs_per_WL,n_WL_cluster,wrk_ID                   !MPI
      integer :: result                                             !MPI
c--
c-- initialize procs_per_WL to the (modified) input variable
c-- nworkers
c--
      procs_per_WL = nworkers                                       !MPI
c--
c-- initialize IDs (etc) to default values to catch errors easier:
c--
c--
c-- these tasks does not take much time and thus we just always run
c-- them on the master of each WL cluster:
c--
      JOB_cnt_op       = 0                                          !MPI
      JOB_jola_op      = 0                                          !MPI
      JOB_nlte_fuzz    = 0                                          !MPI
c--
c-- initialize IDs (etc) to MPI_UNDEFINED to catch bugs
c--
      JOB_wlpk         = MPI_UNDEFINED                              !MPI
      JOB_nlte_opac    = MPI_UNDEFINED                              !MPI
      JOB_nlte_rates   = MPI_UNDEFINED                              !MPI
      JOB_rt           = MPI_UNDEFINED                              !MPI
      JOB_lin_atm_v    = MPI_UNDEFINED                              !MPI
      JOB_lin_atm_g    = MPI_UNDEFINED                              !MPI
      JOB_lin_mol_v    = MPI_UNDEFINED                              !MPI
      JOB_lin_mol_g    = MPI_UNDEFINED                              !MPI
      JOB_selec_mol    = MPI_UNDEFINED                              !MPI
      JOB_selec_atm    = MPI_UNDEFINED                              !MPI
      JOB_selec_fuzz   = MPI_UNDEFINED                              !MPI
c--                                                                 !MPI
c-- allocate vectors that we will need:                             !MPI
c--                                                                 !MPI
      allocate(vector(0:numtasks-1))                                !MPI
c                                                                   !MPI
      write(*,*)                                                    !MPI
      write(*,*)                                                    !MPI
     &    'phx1d_dd_init: Domain Decomposition for PHOENIX/1D starting'!MPI
c--                                                                 !MPI
c-- these are generated for later use:                              !MPI
c--                                                                 !MPI
      call MPI_COMM_GROUP(MyMPI_COM_CMPT,MPI_GROUP_WORLD,ierr)      !MPI
c--                                                                 !MPI
c-- wavelength parallelization checks:                              !MPI
c--                                                                 !MPI
      if(procs_per_WL .le. 0) then                                  !MPI
       procs_per_WL = numtasks                                      !MPI
      endif                                                         !MPI
c--                                                                 !MPI
c-- use procs_per_WL to setup the parameters for the 1D parameterization!MPI
c--                                                                 !MPI
      if(mod(numtasks,procs_per_WL) .ne. 0) then                    !MPI
       write(*,*) 'phx1d_dd_init: mod(numtasks,procs_per_WL) .ne. 0!',!MPI
     &             numtasks,procs_per_WL                            !MPI
       call stop_exit(1,                                            !MPI
     &   'phx1d_dd_init: mod(numtasks,procs_per_WL) .ne. 0!')       !MPI
      endif                                                         !MPI
c                                                                   !MPI
      nwlnodes = numtasks/procs_per_WL                              !MPI
c                                                                   !MPI
      if(mod(numtasks,nwlnodes) .ne. 0) then                        !MPI
       write(*,*) 'phx1d_dd_init: mod(numtasks,nwlnodes) .ne. 0!',  !MPI
     &             numtasks,nwlnodes                                !MPI
       call stop_exit(1,'phx1d_dd_init: mod(numtasks,nwlnodes) .ne. 0!')!MPI
      endif                                                         !MPI
c--                                                                 !MPI
c-- compute the number of cores per wavelength (procs_per_wl_cluster)!MPI
c--                                                                 !MPI
      procs_per_wl_cluster = numtasks/nwlnodes                      !MPI
c--                                                                 !MPI
c-- adapt the phoenix namelist setup parameters to match the 3D     !MPI
c-- setup (which effectively overrules the phoenix 1D setup!)       !MPI
c--                                                                 !MPI
      nworkers = procs_per_wl_cluster                               !MPI
c--
c-- create communicator for workers with the same cluster ID across all
c-- wavelength cluster (so that, e.g., worker #2 of wl cluster 1 can talk 
c-- directly to all other worker #2 in the other wl cluster). This is used
c-- for example in collwldata to "allreduce" data across wavelength clusters
c-- for each worker to keep the DD intact.
c--
c-- MPI_COMM_WRKRS is a "horizontal" communicator so that processes
c-- with the same rank inside a cluster can talk across different 
c-- wavelength clusters.
c-- size is the number of wavelength clusters (numtasks/nworkers)
c--
c-- MPI_COMM_WLP is a "vertical" communicator so that processes
c-- inside the same wavelength cluster can talk to each other.
c-- size is the number of processes per cluster (nworkers)
c--
c--
c-- first we have to figure out our own rank in the cluster
c--
      id_jwlp = taskid/nworkers                                     !MPI 
      id_jwrk = taskid - id_jwlp*nworkers                           !MPI 
c                                                                   !MPI
c-- the color is the internal cluster ID
      color = id_jwrk                                               !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MyMPI_COM_CMPT,color,key,                 !MPI
     &                    MPI_COMM_WRKRS,ierr)                      !MPI
c--
c-- determine the WL rank (ID) of this task:
c--
      call mpi_comm_rank(MPI_COMM_WRKRS,wl_ID,ierr)                 !MPI
      call mpi_comm_size(MPI_COMM_WRKRS,n_WL_cluster,ierr)          !MPI
c--
c-- create communicator for workers within the same cluster ID to talk
c-- to each other internall (across the DD domain at a given wavelength cluster)
c--
c-- first we have to figure out our own rank in the cluster
c--
      id_jwlp = taskid/nworkers                                     !MPI 
c                                                                   !MPI
c-- the color is the wavelength ID of the cluster
      color = id_jwlp                                               !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MyMPI_COM_CMPT,color,key,                 !MPI
     &                    MPI_COMM_WLP,ierr)                        !MPI
c--
c-- determine the intra-cluster rank (ID) of this task:
c--
      call mpi_comm_rank(MPI_COMM_WLP,wrk_ID,ierr)                  !MPI
      call mpi_comm_size(MPI_COMM_WLP,size_of_wl_cluster,ierr)      !MPI
c--
c-- add communicators and IDs for odd jobs (like wavelength point generation
c--
c--
c-- assign the cluster communicator to the various communicators that
c-- phoenix/1D uses
c--
      MPI_COMM_LIN_MOL_V = MPI_COMM_WLP                             !MPI
      MPI_COMM_LIN_MOL_G = MPI_COMM_WLP                             !MPI
      MPI_COMM_LIN_ATM_V = MPI_COMM_WLP                             !MPI
      MPI_COMM_LIN_ATM_G = MPI_COMM_WLP                             !MPI
c
      MPI_COMM_NLTE_OPAC = MPI_COMM_WLP                             !MPI
      MPI_COMM_NLTE_RATES = MPI_COMM_WLP                            !MPI
      MPI_COMM_CASWL = MPI_COMM_NLTE_RATES                          !MPI
c--
c-- create the communictor for the RT tasks on each cluster:
c-- n_rt <= 0: use all workers for RT tasks
c-- PHH: this MUST be used in 1D, or the RT will crash.
c--
      write(*,*) 'setting n_rt=nworkers:',n_rt,nworkers             !MPI 
      if(n_rt .le. 0) n_rt = nworkers                               !MPI 
      n_rt = nworkers                                               !MPI 
c
      color = MPI_UNDEFINED                                         !MPI
      if(wrk_ID/n_rt .eq. 0) color = 0                              !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MPI_COMM_WLP,color,key,                   !MPI
     &                    MPI_COMM_RT,ierr)                         !MPI
c--
c-- create RT2RATES communicator: it includes all RT and NLTE_RATES
c-- tasks to bcast Lstar etc data from RT task to RATE tasks.
c-- both MPI_COMM_RT and MPI_COMM_NLTE_RATES are subsets of
c-- MPI_COMM_WLP (vertical).
c--
      color = MPI_UNDEFINED                                         !MPI
      call get_rank(MPI_COMM_RT,id_tst,ierr)                        !MPI
      if(id_tst .ne. MPI_UNDEFINED) color = 0                       !MPI
      call get_rank(MPI_COMM_NLTE_RATES,id_tst,ierr)                !MPI
      if(id_tst .ne. MPI_UNDEFINED) color = 0                       !MPI
c                                                                   !MPI
      key = 0                                                       !MPI
      call MPI_COMM_SPLIT(MPI_COMM_WLP,color,key,                   !MPI
     &                    MPI_COMM_RT2RATES,ierr)                   !MPI
c--
c-- create communicators for all line selection procedures
c-- sequentially fill available processes with line selection tasks
c-- if full start anew at global taskid 0. data distribution in 
c-- phx1d_line_data_bcast has to be done in MyMPI_COM_CMPT, thus
c-- we store the global taskids of the selections masters
c-- here for all processes (goes into phx-mpi.inc common block).
c--
c-- check and set the defaults:
c--
          if(n_selec_atm .le. 0) n_selec_atm = numtasks             !MPI
          if(n_selec_mol .le. 0) n_selec_mol = numtasks             !MPI
          if(n_selec_fuzz .le. 0) n_selec_fuzz = numtasks           !MPI
          n_selec_atm  = min(numtasks,n_selec_atm)                  !MPI
          n_selec_mol  = min(numtasks,n_selec_mol)                  !MPI
          n_selec_fuzz = min(numtasks,n_selec_fuzz)                 !MPI
c
c-- 1.: fuzz lines, expected to use the least number of procs due
c--     to small linelist:
c--
      color = MPI_UNDEFINED                                         !MPI
      JOB_selec_fuzz = 0                                            !MPI
      if(taskid .lt. n_selec_fuzz) then                             !MPI
       color = 0                                                    !MPI
      endif                                                         !MPI
c                                                                   !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MyMPI_COM_CMPT,color,key,                 !MPI
     &                    MPI_COMM_SELEC_FUZZ,ierr)                 !MPI
c--
c-- 2.: atomic lines, expected to use next larger number of procs due
c--     to linelist expected larger than fuzz:
c--
      color = MPI_UNDEFINED                                         !MPI
      if(JOB_selec_fuzz+n_selec_fuzz+n_selec_atm .le. numtasks) then!MPI
       JOB_selec_atm = JOB_selec_fuzz+n_selec_fuzz                  !MPI
      else                                                          !MPI
       JOB_selec_atm = 0                                            !MPI
      endif                                                         !MPI
c
      if(taskid .ge. JOB_selec_atm .and.                            !MPI
     &   taskid .lt. JOB_selec_atm+n_selec_atm) then                !MPI
       color = 0                                                    !MPI
      endif                                                         !MPI
c                                                                   !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MyMPI_COM_CMPT,color,key,                 !MPI
     &                    MPI_COMM_SELEC_ATM,ierr)                  !MPI
c--
c-- 3.: molecular lines, expected to use largest number of procs due
c--     to linelist expected to be very large compared to others
c--
      color = MPI_UNDEFINED                                         !MPI
      if(JOB_selec_atm+n_selec_atm+n_selec_mol .le. numtasks) then  !MPI
       JOB_selec_mol = JOB_selec_atm+n_selec_atm                    !MPI
      else                                                          !MPI
       JOB_selec_mol = 0                                            !MPI
      endif                                                         !MPI
c
      if(taskid .ge. JOB_selec_mol .and.                            !MPI
     &   taskid .lt. JOB_selec_mol+n_selec_mol) then                !MPI
       color = 0                                                    !MPI
      endif                                                         !MPI
c                                                                   !MPI
      key = 0                                                       !MPI
c                                                                   !MPI
      call MPI_COMM_SPLIT(MyMPI_COM_CMPT,color,key,                 !MPI
     &                    MPI_COMM_SELEC_MOL,ierr)                  !MPI
c--
c-- create the local task IDs for the various tasks that phoenix/1D 
c-- uses:
c--
      call get_rank(MPI_COMM_LIN_MOL_V,id_lin_mol_v,ierr)           !MPI
      call get_rank(MPI_COMM_LIN_ATM_V,id_lin_atm_v,ierr)           !MPI
      call get_rank(MPI_COMM_LIN_MOL_G,id_lin_mol_g,ierr)           !MPI
      call get_rank(MPI_COMM_LIN_ATM_G,id_lin_atm_g,ierr)           !MPI
      call get_rank(MPI_COMM_NLTE_OPAC,id_nlte_opac,ierr)           !MPI
      call get_rank(MPI_COMM_NLTE_RATES,id_nlte_rates,ierr)         !MPI
      call get_rank(MPI_COMM_RT,id_rt,ierr)                         !MPI
      call get_rank(MPI_COMM_RT2RATES,id_rt2rates,ierr)             !MPI
      call get_rank(MPI_COMM_SELEC_ATM,id_selec_atm,ierr)           !MPI
      call get_rank(MPI_COMM_SELEC_MOL,id_selec_mol,ierr)           !MPI
      call get_rank(MPI_COMM_SELEC_FUZZ,id_selec_fuzz,ierr)         !MPI
      FS_rank = id_rt                                               !MPI
c
c--
c-- get the task sizes for the various tasks that phoenix/1D 
c-- uses:
c--
      call get_size(MPI_COMM_RT,FS_size,ierr)                       !MPI
      call get_size(MPI_COMM_LIN_MOL_V,n_lin_mol_v,ierr)            !MPI
      call get_size(MPI_COMM_LIN_MOL_G,n_lin_mol_g,ierr)            !MPI
      call get_size(MPI_COMM_LIN_ATM_V,n_lin_atm_g,ierr)            !MPI
      call get_size(MPI_COMM_LIN_ATM_G,n_lin_atm_v,ierr)            !MPI
      call get_size(MPI_COMM_NLTE_RATES,n_nlte_rates,ierr)          !MPI
      call get_size(MPI_COMM_NLTE_OPAC,n_nlte_opac,ierr)            !MPI
      call get_size(MPI_COMM_RT,n_rt,ierr)                          !MPI
c--
c-- Cassandra NLTE worker distribution:
c-- just || over the layers: 
c--
       write(*,*) 'layer-only cassandra distribution'               !MPI 
       id_nlte_group = id_nlte_rates                                !MPI 
       n_nlte_group = n_nlte_rates                                  !MPI 
c--
c-- populate JOB_ variables so that the 1D opacity etc collection/
c-- distribution system is setup correctly.
c-- example: all members of a cluster need to know the intra-cluster rank
c-- of the master RT tasks in this cluster, JOB_rt. We generate
c-- this with the following code:
c--
      call assign_JOB(id_rt,JOB_rt)                                 !MPI
c--
c-- now here are the other JOB_ ids that need to be know to all
c-- cluster members
c--
      call assign_JOB(id_nlte_opac,JOB_nlte_opac)                   !MPI
      call assign_JOB(id_nlte_rates,JOB_nlte_rates)                 !MPI
      call assign_JOB(id_lin_atm_v,JOB_lin_atm_v)                   !MPI
      call assign_JOB(id_lin_atm_g,JOB_lin_atm_g)                   !MPI
      call assign_JOB(id_lin_mol_v,JOB_lin_mol_v)                   !MPI
      call assign_JOB(id_lin_mol_g,JOB_lin_mol_g)                   !MPI
c--
c-- the IDs for the various line selection tasks were determined
c-- globally (scope is COMM_WORLD) and do not need to be processed.
c--
c     JOB_selec_mol    = JOB_selec_mol                              !MPI
c     JOB_selec_atm    = JOB_selec_atm                              !MPI
c     JOB_selec_fuzz   = JOB_selec_fuzz                             !MPI
c--
c-- wavelength grid generation is serial and must run on an NLTE
c-- task
c-- we just use the master of nlte_opacity here:
c--
      call assign_JOB(id_nlte_opac,JOB_wlpk)                        !MPI
c--
c-- the next tasks are serial and hardcoded. All cluster members
c-- therefore KNOW the correct intra-cluster ID already, no action
c-- is required:
c--
c     JOB_cnt_op       = JOB_cnt_op                                 !MPI
c     JOB_jola_op      = JOB_jola_op                                !MPI
c     JOB_nlte_fuzz    = JOB_nlte_fuzz                              !MPI
c--                                                                 !MPI
c-- the next is just informational output and various checks        !MPI
c-- compute number of cores per node, this MUST be the same for     !MPI
c-- all nodes!                                                      !MPI
c--                                                                 !MPI
      allocate(name_vector(0:numtasks-1))                           !MPI
      name_vector(:) = ' '                                          !MPI
c--                                                                 !MPI
c-- informational chart, printed on each process so that all the    !MPI
c-- info is available in each stdout:                               !MPI
c-- we print for each task a number of info data:                   !MPI
c--                                                                 !MPI
      allocate(wrk_ID_vec(0:numtasks-1))                            !MPI
      allocate(nworkers_vec(0:numtasks-1))                          !MPI
      allocate(wl_ID_vec(0:numtasks-1))                             !MPI
      allocate(n_WL_cluster_vec(0:numtasks-1))                      !MPI
      allocate(FS_rank_v(0:numtasks-1))                             !MPI
      allocate(FS_size_v(0:numtasks-1))                             !MPI
c--
      allocate(id_lin_mol_v_vec(0:numtasks-1))                      !MPI
      allocate(id_lin_atm_v_vec(0:numtasks-1))                      !MPI
      allocate(id_lin_mol_g_vec(0:numtasks-1))                      !MPI
      allocate(id_lin_atm_g_vec(0:numtasks-1))                      !MPI
      allocate(id_nlte_opac_vec(0:numtasks-1))                      !MPI
      allocate(id_nlte_rates_vec(0:numtasks-1))                     !MPI
      allocate(id_rt_vec(0:numtasks-1))                             !MPI
      allocate(id_rt2rates_vec(0:numtasks-1))                       !MPI
      allocate(id_selec_atm_vec(0:numtasks-1))                      !MPI
      allocate(id_selec_mol_vec(0:numtasks-1))                      !MPI
      allocate(id_selec_fuzz_vec(0:numtasks-1))                     !MPI
c--
      allocate(n_lin_mol_v_vec(0:numtasks-1))                       !MPI
      allocate(n_lin_mol_g_vec(0:numtasks-1))                       !MPI
      allocate(n_lin_atm_g_vec(0:numtasks-1))                       !MPI
      allocate(n_lin_atm_v_vec(0:numtasks-1))                       !MPI
      allocate(n_nlte_rates_vec(0:numtasks-1))                      !MPI
      allocate(n_nlte_opac_vec(0:numtasks-1))                       !MPI
      allocate(n_rt_vec(0:numtasks-1))                              !MPI
c                                                                   !MPI
      call MPI_Get_processor_name(cpuname,len_name,ierr)            !MPI
c--                                                                 !MPI
c-- some machines return names with char(0), replace them with blanks!MPI 
c--                                                                 !MPI
      do i=1,MPI_MAX_PROCESSOR_NAME                                 !MPI
       if(cpuname(i:i) .eq. char(0)) cpuname(i:i)  = ' '            !MPI
      enddo                                                         !MPI
      call MPI_ALLGATHER(cpuname,MPI_MAX_PROCESSOR_NAME,MPI_CHARACTER,!MPI
     &      name_vector,MPI_MAX_PROCESSOR_NAME,MPI_CHARACTER,       !MPI
     &      MyMPI_COM_CMPT,ierr)                                    !MPI
c
      call gather_DATA(wrk_ID,wrk_ID_vec)                           !MPI
      call gather_DATA(size_of_wl_cluster,nworkers_vec)             !MPI
      call gather_DATA(wl_ID,wl_ID_vec)                             !MPI
      call gather_DATA(n_WL_cluster,n_WL_cluster_vec)               !MPI
      call gather_DATA(FS_rank,FS_rank_v)                           !MPI
      call gather_DATA(FS_size,FS_size_v)                           !MPI
c
      call gather_DATA(id_lin_mol_v,id_lin_mol_v_vec)               !MPI
      call gather_DATA(id_lin_mol_g,id_lin_mol_g_vec)               !MPI
      call gather_DATA(id_lin_atm_v,id_lin_atm_v_vec)               !MPI
      call gather_DATA(id_lin_atm_g,id_lin_atm_g_vec)               !MPI
      call gather_DATA(id_nlte_opac,id_nlte_opac_vec)               !MPI
      call gather_DATA(id_nlte_rates,id_nlte_rates_vec)             !MPI
      call gather_DATA(id_rt,id_rt_vec)                             !MPI
      call gather_DATA(id_rt2rates,id_rt2rates_vec)                 !MPI
      call gather_DATA(id_selec_atm,id_selec_atm_vec)               !MPI
      call gather_DATA(id_selec_mol,id_selec_mol_vec)               !MPI
      call gather_DATA(id_selec_fuzz,id_selec_fuzz_vec)             !MPI
c
      call gather_DATA(n_lin_mol_v,n_lin_mol_v_vec)                 !MPI
      call gather_DATA(n_lin_mol_g,n_lin_mol_g_vec)                 !MPI
      call gather_DATA(n_lin_atm_v,n_lin_atm_v_vec)                 !MPI
      call gather_DATA(n_lin_atm_g,n_lin_atm_g_vec)                 !MPI
c                                                                   !MPI
      write(*,*)                                                    !MPI
      write(*,*) 'global task distribution chart:'                  !MPI
      write(*,'(1x,a7,1x,25a15)') 'taskid','cpuname',               !MPI
     &  'wrk_ID','nworkers',                                        !MPI
     &  'wl_ID','n_WL_cluster',                                     !MPI
     &  'FS_size','FS_rank',                                        !MPI
     &  'n_lin_mol_v',                                              !MPI
     &  'n_lin_mol_g',                                              !MPI
     &  'n_lin_atm_v',                                              !MPI
     &  'n_lin_atm_g',                                              !MPI
     &  'id_lin_mol_v',                                             !MPI
     &  'id_lin_mol_g',                                             !MPI
     &  'id_lin_atm_v',                                             !MPI
     &  'id_lin_atm_g',                                             !MPI
     &  'id_nlte_opac',                                             !MPI
     &  'id_nlte_rates',                                            !MPI
     &  'id_rt',                                                    !MPI
     &  'id_rt2rates',                                              !MPI
     &  'id_selec_atm',                                             !MPI
     &  'id_selec_mol',                                             !MPI
     &  'id_selec_fuzz'                                             !MPI
      do i=0,numtasks-1                                             !MPI
       write(*,'(1x,i7,1x,a15,25i15)') i,trim(name_vector(i)),      !MPI
     &  wrk_ID_vec(i),nworkers_vec(i),                              !MPI
     &  wl_ID_vec(i),n_WL_cluster_vec(i),                           !MPI
     &  FS_size,FS_rank_v(i),                                       !MPI
     &  n_lin_mol_v_vec(i),                                         !MPI
     &  n_lin_mol_g_vec(i),                                         !MPI
     &  n_lin_atm_v_vec(i),                                         !MPI
     &  n_lin_atm_g_vec(i),                                         !MPI
     &  id_lin_mol_v_vec(i),                                        !MPI
     &  id_lin_mol_g_vec(i),                                        !MPI
     &  id_lin_atm_v_vec(i),                                        !MPI
     &  id_lin_atm_g_vec(i),                                        !MPI
     &  id_nlte_opac_vec(i),                                        !MPI
     &  id_nlte_rates_vec(i),                                       !MPI
     &  id_rt_vec(i),                                               !MPI
     &  id_rt2rates_vec(i),                                         !MPI
     &  id_selec_atm_vec(i),                                        !MPI
     &  id_selec_mol_vec(i),                                        !MPI
     &  id_selec_fuzz_vec(i)                                        !MPI
      enddo                                                         !MPI
c--                                                                 !MPI
c-- summary printout:                                               !MPI
c--                                                                 !MPI
      write(*,*)                                                    !MPI
      write(*,*) 'phx1d_dd_init complete. The results are:'         !MPI
      write(*,'(1x,a30,i8)') 'my taskid:',taskid                    !MPI
      write(*,'(1x,a30,i8)') 'numtasks:',numtasks                   !MPI
      write(*,'(1x,a30,a)')  'node name: ',trim(cpuname)            !MPI
      write(*,'(1x,a30,i8)') 'procs_per_WL:',procs_per_WL           !MPI
      write(*,'(1x,a30,i8)') 'procs_per_wl_cluster:',               !MPI
     &                        procs_per_wl_cluster                  !MPI
      write(*,'(1x,a30,i8)') 'nwlnodes:',nwlnodes                   !MPI
      write(*,'(1x,a30,i8)') 'nworkers:',nworkers                   !MPI
      write(*,'(1x,a30,i8)') 'n_lin_mol_v:',n_lin_mol_v             !MPI
      write(*,'(1x,a30,i8)') 'n_lin_mol_g:',n_lin_mol_g             !MPI
      write(*,'(1x,a30,i8)') 'n_lin_atm_v:',n_lin_atm_v             !MPI
      write(*,'(1x,a30,i8)') 'n_lin_atm_g:',n_lin_atm_g             !MPI
      write(*,'(1x,a30,i8)') 'n_nlte_rates:',n_nlte_rates           !MPI
      write(*,'(1x,a30,i8)') 'n_nlte_opac:',n_nlte_opac             !MPI
      write(*,'(1x,a30,i8)') 'n_selec_atm:',n_selec_atm             !MPI
      write(*,'(1x,a30,i8)') 'n_selec_mol:',n_selec_mol             !MPI
      write(*,'(1x,a30,i8)') 'n_selec_fuzz:',n_selec_fuzz           !MPI
      write(*,'(1x,a30,i8)') 'wl_ID:',wl_ID                         !MPI
      write(*,'(1x,a30,i8)') 'wrk_ID:',wrk_ID                       !MPI
      write(*,'(1x,a30,i8)') 'id_lin_mol_v:',id_lin_mol_v           !MPI
      write(*,'(1x,a30,i8)') 'id_lin_mol_g:',id_lin_mol_g           !MPI
      write(*,'(1x,a30,i8)') 'id_lin_atm_v:',id_lin_atm_v           !MPI
      write(*,'(1x,a30,i8)') 'id_lin_atm_g:',id_lin_atm_g           !MPI
      write(*,'(1x,a30,i8)') 'id_nlte_opac:',id_nlte_opac           !MPI
      write(*,'(1x,a30,i8)') 'id_nlte_rates:',id_nlte_rates         !MPI
      write(*,'(1x,a30,i8)') 'id_rt:',       id_rt                  !MPI
      write(*,'(1x,a30,i8)') 'id_rt2rates:', id_rt2rates            !MPI
      write(*,'(1x,a30,i8)') 'id_selec_atm:',id_selec_atm           !MPI
      write(*,'(1x,a30,i8)') 'id_selec_mol:',id_selec_mol           !MPI
      write(*,'(1x,a30,i8)') 'id_selec_fuzz:',id_selec_fuzz         !MPI
      write(*,'(1x,a30,i8)') 'JOB_cnt_op:    ',JOB_cnt_op           !MPI
      write(*,'(1x,a30,i8)') 'JOB_jola_op:   ',JOB_jola_op          !MPI
      write(*,'(1x,a30,i8)') 'JOB_nlte_fuzz: ',JOB_nlte_fuzz        !MPI
      write(*,'(1x,a30,i8)') 'JOB_wlpk:      ',JOB_wlpk             !MPI
      write(*,'(1x,a30,i8)') 'JOB_nlte_opac: ',JOB_nlte_opac        !MPI
      write(*,'(1x,a30,i8)') 'JOB_nlte_rates:',JOB_nlte_rates       !MPI
      write(*,'(1x,a30,i8)') 'JOB_rt:        ',JOB_rt               !MPI
      write(*,'(1x,a30,i8)') 'JOB_lin_atm_v: ',JOB_lin_atm_v        !MPI
      write(*,'(1x,a30,i8)') 'JOB_lin_atm_g: ',JOB_lin_atm_g        !MPI
      write(*,'(1x,a30,i8)') 'JOB_lin_mol_v: ',JOB_lin_mol_v        !MPI
      write(*,'(1x,a30,i8)') 'JOB_lin_mol_g: ',JOB_lin_mol_g        !MPI
      write(*,'(1x,a30,i8)') 'JOB_selec_mol: ',JOB_selec_mol        !MPI
      write(*,'(1x,a30,i8)') 'JOB_selec_atm: ',JOB_selec_atm        !MPI
      write(*,'(1x,a30,i8)') 'JOB_selec_fuzz:',JOB_selec_fuzz       !MPI
c--                                                                 !MPI
c-- get rid of allocated variables:                                 !MPI
c--                                                                 !MPI
      deallocate(wrk_ID_vec)                                        !MPI
      deallocate(nworkers_vec)                                      !MPI
      deallocate(wl_ID_vec)                                         !MPI
      deallocate(n_WL_cluster_vec)                                  !MPI
      deallocate(FS_rank_v)                                         !MPI
      deallocate(FS_size_v)                                         !MPI
      deallocate(name_vector,vector)                                !MPI
c--
      deallocate(id_lin_mol_v_vec)                                  !MPI
      deallocate(id_lin_atm_v_vec)                                  !MPI
      deallocate(id_lin_mol_g_vec)                                  !MPI
      deallocate(id_lin_atm_g_vec)                                  !MPI
      deallocate(id_nlte_opac_vec)                                  !MPI
      deallocate(id_nlte_rates_vec)                                 !MPI
      deallocate(id_rt_vec)                                         !MPI
      deallocate(id_rt2rates_vec)                                   !MPI
      deallocate(id_selec_atm_vec)                                  !MPI
      deallocate(id_selec_mol_vec)                                  !MPI
      deallocate(id_selec_fuzz_vec)                                 !MPI
      deallocate(n_lin_mol_v_vec)                                   !MPI
      deallocate(n_lin_mol_g_vec)                                   !MPI
      deallocate(n_lin_atm_g_vec)                                   !MPI
      deallocate(n_lin_atm_v_vec)                                   !MPI
      deallocate(n_nlte_rates_vec)                                  !MPI
      deallocate(n_nlte_opac_vec)                                   !MPI
      deallocate(n_rt_vec)                                          !MPI
c
c     result = 0                                                    !MPI
c      if(id_rt .eq. 0) then                                        !MPI
c      call mpi_allreduce(taskid,result,1,MPI_INTEGER,MPI_SUM,      !MPI
c    &   MPI_COMM_WRKRS,ierr)                                       !MPI
c      endif                                                        !MPI
c     write(*,*)"taskid,result",taskid,result                       !MPI
c      call stop_exit(0,'phx1d_dd_init: debug stop')
c                                                                   !MPI
      return
c
c
      contains                                                      !MPI
c                                                                   !MPI
      subroutine assign_JOB(ID,JOB_ID)                              !MPI
c     --------------------------------                              !MPI
      implicit none                                                 !MPI
      integer, intent(in) :: ID                                     !MPI
      integer, intent(out) :: JOB_ID                                !MPI
c                                                                   !MPI
      integer :: id_tst                                             !MPI
c                                                                   !MPI
      if(ID .eq. 0) then                                            !MPI
       JOB_ID = wrk_ID                                              !MPI
      else                                                          !MPI
       JOB_ID = 0                                                   !MPI
      endif                                                         !MPI
      id_tst = JOB_ID                                               !MPI
      call mpi_allreduce(id_tst,JOB_ID,1,MPI_INTEGER,MPI_SUM,       !MPI
     &                   MPI_COMM_WLP,ierr)                         !MPI
      return
      end subroutine assign_JOB                                     !MPI
c                                                                   !MPI
      subroutine gather_DATA(scalar,vector)                         !MPI
c     --------------------------------                              !MPI
      implicit none                                                 !MPI
      integer, intent(in) :: scalar                                 !MPI
      integer, intent(out) :: vector(:)                             !MPI
c                                                                   !MPI
      call MPI_ALLGATHER(scalar,1,MPI_INTEGER,                      !MPI
     &      vector,1,MPI_INTEGER,MyMPI_COM_CMPT,ierr)               !MPI
      return                                                        !MPI
      end subroutine gather_DATA                                    !MPI
c                                                                   !MPI
c
      end subroutine phx1d_dd_init
c
c
c
      subroutine phx1d_line_data_bcast(ezl,ezlmol)
c     --------------------------------------------
      use linecom
      implicit none
c
      include 'phx-mpi.inc'                                         !MPI
      include 'param.inc'
CLIN-MOD-OFF      include 'linecom.inc'                                 !LIN-MOD-OFF
      logical ezl,ezlmol
************************************************************************
* broadcast the line data from line selection process to ALL nodes
*                   version 2.0 of 08/Mar/2010 by PHH & EAB
*-- input:
* in common
*-- last changes:
* 08/Mar/2010: converted this crap to new better communicator setup (PHH + EAB)
* 21/nov/2000: fixed bcast bug of lineunitf (AS+PHH)
* 14/feb/98: added bcast of [first|last]_mol_g (PHH) 
* 01/mar/96: alpha version for molecular lines only
************************************************************************
c--
c-- local vars:
c--
      logical reopen                                                !MPI
      parameter(reopen=.false.)                                     !MPI
      real*8 :: DLDO                                                !MPI
      integer :: I                                                  !MPI
      integer :: ICOUNT                                             !MPI
      integer :: ID_JWLP                                            !MPI
      integer :: ID_JWRK                                            !MPI
      integer :: IEND                                               !MPI
      integer :: IERR                                               !MPI
      integer :: IROOT                                              !MPI
      integer :: ISTART                                             !MPI
      integer :: ITRN                                               !MPI
c--
c-- send the molecular pointer and data:
c--
      call mpi_bcast(mnbstart,1,MPI_INTEGER8,(JOB_selec_mol),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnstart,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbstartg,1,MPI_INTEGER8,(JOB_selec_mol),      !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnstartg,1,MPI_INTEGER8,(JOB_selec_mol),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbend,1,MPI_INTEGER8,(JOB_selec_mol),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnend,1,MPI_INTEGER8,(JOB_selec_mol),          !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbendg,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnendg,1,MPI_INTEGER8,(JOB_selec_mol),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbstart,1,MPI_INTEGER8,(JOB_selec_mol),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcstart,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbstartg,1,MPI_INTEGER8,(JOB_selec_mol),      !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcstartg,1,MPI_INTEGER8,(JOB_selec_mol),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbend,1,MPI_INTEGER8,(JOB_selec_mol),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcend,1,MPI_INTEGER8,(JOB_selec_mol),          !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcurblk,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbendg,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbendg,1,MPI_INTEGER8,(JOB_selec_mol),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcendg,1,MPI_INTEGER8,(JOB_selec_mol),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcurblkg,1,MPI_INTEGER8,(JOB_selec_mol),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
c-not-yet      call mpi_bcast(first_mol_g,maxblock_mol,MPI_DP,      !MPI
c-not-yet     &              (JOB_selec_mol),                       !MPI
c-not-yet     &              MyMPI_COM_CMPT,ierr)                   !MPI
      call mpi_bcast(last_mol_g,maxblock_mol,MPI_DP,                !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
c--
c-- send the zeta data etc:
c--
      call mpi_bcast(zetmol,layer*nmols,                            !MPI
     &      MPI_REAL,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)           !MPI
      call mpi_bcast(vdsqml,layer*nmols,                            !MPI
     &      MPI_REAL,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)           !MPI
      call mpi_bcast(vdopmaxmol,1,                                  !MPI
     &      MPI_DP,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(lstmol,nmols,                                  !MPI
     &      MPI_INTEGER,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)        !MPI
      call mpi_bcast(mlbegin,1,                                     !MPI
     &      MPI_INTEGER,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)        !MPI
      call mpi_bcast(mllast,1,                                      !MPI
     &      MPI_INTEGER,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)        !MPI
      call mpi_bcast(mlstart,1,                                     !MPI
     &      MPI_INTEGER,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)        !MPI
c--
c-- send the damping constant multipliers:
c--
      call mpi_bcast(pert4m,layer,                                  !MPI
     &      MPI_DP,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(pert6m,layer,                                  !MPI
     &      MPI_DP,(JOB_selec_mol),MyMPI_COM_CMPT,ierr)             !MPI
c--
c-- send the atomic pointer and data:
c--
      call mpi_bcast(lnbstart,1,MPI_INTEGER8,(JOB_selec_atm),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnstart,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnbstartg,1,MPI_INTEGER8,(JOB_selec_atm),      !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnstartg,1,MPI_INTEGER8,(JOB_selec_atm),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnbend,1,MPI_INTEGER8,(JOB_selec_atm),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnend,1,MPI_INTEGER8,(JOB_selec_atm),          !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnbendg,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lnendg,1,MPI_INTEGER8,(JOB_selec_atm),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcbstart,1,MPI_INTEGER8,(JOB_selec_atm),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcstart,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcbstartg,1,MPI_INTEGER8,(JOB_selec_atm),      !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcstartg,1,MPI_INTEGER8,(JOB_selec_atm),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcbend,1,MPI_INTEGER8,(JOB_selec_atm),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcend,1,MPI_INTEGER8,(JOB_selec_atm),          !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcurblk,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcbendg,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcbendg,1,MPI_INTEGER8,(JOB_selec_atm),        !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcendg,1,MPI_INTEGER8,(JOB_selec_atm),         !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(lcurblkg,1,MPI_INTEGER8,(JOB_selec_atm),       !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
c-not-yet      call mpi_bcast(first_atm_g,maxblock_atm,MPI_DP,      !MPI
c-not-yet     &              (JOB_selec_atm),                       !MPI
c-not-yet     &              MyMPI_COM_CMPT,ierr)                   !MPI
      call mpi_bcast(last_atm_g,maxblock_atm,MPI_DP,                !MPI
     &              (JOB_selec_atm),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
c--
c-- send the zeta data etc:
c--
      call mpi_bcast(zethlp,layer*aeel,                             !MPI
     &      MPI_DP,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(vdpsqi,layer*aeel,                             !MPI
     &      MPI_DP,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(vdopmaxatm,1,                                  !MPI
     &      MPI_DP,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(listel,aeel,                                   !MPI
     &      MPI_INTEGER,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)        !MPI
      call mpi_bcast(nlines,1,                                      !MPI
     &      MPI_INTEGER8,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)       !MPI
      call mpi_bcast(kstart,1,                                      !MPI
     &      MPI_INTEGER8,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)       !MPI
      call mpi_bcast(kstartg,1,                                     !MPI
     &      MPI_INTEGER8,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)       !MPI
c--
c-- send the damping constant multipliers:
c--
      call mpi_bcast(pert4l,layer,                                  !MPI
     &      MPI_DP,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)             !MPI
      call mpi_bcast(pert6l,layer,                                  !MPI
     &      MPI_DP,(JOB_selec_atm),MyMPI_COM_CMPT,ierr)             !MPI
c
      call mpi_bcast(lnbstartf,1,                                   !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lnstartf,1,                                    !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lnbendf,1,                                     !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lnendf,1,                                      !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lcbstartf,1,                                   !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lcstartf,1,                                    !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lcbendf,1,                                     !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lcendf,1,                                      !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(lcurblkf,1,                                    !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
cbug  call mpi_bcast(lineunitf,1,                                   !MPI
cbug &      MPI_INTEGER,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)       !MPI
      call mpi_bcast(vdopmaxatmf,1,                                 !MPI
     &      MPI_DP,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)            !MPI
      call mpi_bcast(nlinesf,1,                                     !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(kstartf,1,                                     !MPI
     &      MPI_INTEGER8,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)      !MPI
      call mpi_bcast(iph2,1,                                        !MPI
     &      MPI_INTEGER,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)       !MPI
      call mpi_bcast(zethlpf,layer*aeel,                            !MPI
     &      MPI_DP,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)            !MPI
      call mpi_bcast(vdpsqif,layer*aeel,                            !MPI
     &      MPI_DP,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)            !MPI
      call mpi_bcast(ipgrnd,aeel,                                   !MPI
     &      MPI_INTEGER,(JOB_selec_fuzz),MyMPI_COM_CMPT,ierr)       !MPI
c--
c-- And now the molecular NLTE pointers etc.
c--
      call mpi_bcast(mnbstartnlte,1,MPI_INTEGER,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnstartnlte,1,MPI_INTEGER,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbstartgnlte,1,MPI_INTEGER,                   !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnstartgnlte,1,MPI_INTEGER,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbendnlte,1,MPI_INTEGER8,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnendnlte,1,MPI_INTEGER8,                      !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnbendgnlte,1,MPI_INTEGER8,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mnendgnlte,1,MPI_INTEGER8,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbstartnlte,1,MPI_INTEGER,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcstartnlte,1,MPI_INTEGER,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbstartgnlte,1,MPI_INTEGER,                   !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcstartgnlte,1,MPI_INTEGER,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbendnlte,1,MPI_INTEGER,                      !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcendnlte,1,MPI_INTEGER,                       !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcurblknlte,1,MPI_INTEGER,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbendgnlte,1,MPI_INTEGER,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcbendgnlte,1,MPI_INTEGER,                     !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcendgnlte,1,MPI_INTEGER,                      !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
      call mpi_bcast(mcurblkgnlte,1,MPI_INTEGER,                    !MPI
     &              (JOB_selec_mol),                                !MPI
     &              MyMPI_COM_CMPT,ierr)                            !MPI
c
!-- this kills the NERSC machines, don't know why 2014.09.24 (eab)
coffcMPI      if(reopen) call re_open(ezl,ezlmol)                   !MPI
c--
c-- reset caches:
c--
coff      if(allocated(atm_v_buf)) atm_v_buf = -1
coff      if(allocated(atm_g_buf)) atm_g_buf = -1
coff      if(allocated(mol_v_buf)) mol_v_buf = -1
coff      if(allocated(mol_g_buf)) mol_g_buf = -1
coff      if(allocated(fuzz_g_buf)) fuzz_g_buf = -1
coff      if(allocated(mol_v_n_buf)) mol_v_n_buf = -1
coff      if(allocated(mol_g_n_buf)) mol_g_n_buf = -1
      return
      end
c
c
c
      subroutine phx1d_collwldata
c     ---------------------------
      use casscom                                                   !MPI
      implicit none
************************************************************************
* this routine collects all rates and R* operators from the wavelength
* clusters. It would be fairly easy if mpi_allreduce would actually
* work on all machines (Hello IBM!)... 
*     version 1.2 of 08/Mar/2010 by EAB and PHH 
*
* 1.2: changed MPI_COMM_WRKRS to scalar for simplicity (phh+eab, 09/Mar/2010)
* 1.1: fixed array out-of-bounds bug (phh, 19/Oct/2000)
* 1.0: 08/May/1997 by EAB and PHH 
************************************************************************
c                                                                   !MPI
      integer chunky
      parameter(chunky=100)
      real*8 :: DLDO                                                !MPI
      integer :: I                                                  !MPI
      integer :: ICOUNT                                             !MPI
      integer :: ID_JWLP                                            !MPI
      integer :: ID_JWRK                                            !MPI
      integer :: IEND                                               !MPI
      integer :: IERR                                               !MPI
      integer :: IROOT                                              !MPI
      integer :: ISTART                                             !MPI
      integer :: ITRN                                               !MPI
      integer :: LAYER                                              !MPI
c mpi commons                                                       !MPI
      include 'phx-mpi.inc'                                         !MPI
c--                                                                 !MPI
c-- Cassandra parameter and commons:                                !MPI
c--                                                                 !MPI
c
c
c                                                                   !MPI
      logical docastri                                              !MPI
      common /casoptype/ docastri                                   !MPI
c                                                                   !MPI
      real*8, allocatable :: scrmat(:,:)                            !MPI
c                                                                   !MPI
      allocate(scrmat(lc1:lc2,chunky))                              !MPI
c--
c-- first we have to figure out our own rank in the cluster
c--
      iroot = 0                                                     !MPI
      id_jwlp = taskid/nworkers                                     !MPI
      id_jwrk = taskid - id_jwlp*nworkers                           !MPI
c     print *,"mpi debug collwldata : ",iroot,taskid                !MPI
c--
c-- now we reduce the line and contimuum rates etc from all clusters
c-- to the master cluster (zero) and then bcast them back to all
c-- members of our cluster. (to avoid bugs on mpi_allreduce!)
c-- we do this in chunks in order to save memory (T3E!)
c--
      reduce_rates: do i=0,maxtrnb-1,chunky                         !MPI
       istart = i                                                   !MPI
       iend = min(i+chunky-1,maxtrnb-1)                             !MPI
       icount = (lc2-lc1+1)*(iend-istart+1)                         !MPI
       itrn = iend-istart+1                                         !MPI
c      write(*,*) 'reduce_rates:',taskid,istart,iend,icount,itrn    !MPI
      call mpi_reduce(rabs(lc1:lc2,istart:iend),scrmat,icount,      !MPI
     &                MPI_DP,MPI_SUM,                               !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)           !MPI
      endif                                                         !MPI
      call mpi_bcast(rabs(lc1:lc2,istart:iend),icount,MPI_DP,       !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rem(lc1:lc2,istart:iend),scrmat,icount,       !MPI
     &                MPI_DP,MPI_SUM,                               !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)            !MPI
      endif                                                         !MPI
      call mpi_bcast(rem(lc1:lc2,istart:iend),icount,MPI_DP,        !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rstabs(lc1:lc2,istart:iend),scrmat,icount,    !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
      endif                                                         !MPI
      call mpi_bcast(rstabs(lc1:lc2,istart:iend),icount,MPI_DP,     !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rstem(lc1:lc2,istart:iend),scrmat,icount,     !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)          !MPI
      endif                                                         !MPI
      call mpi_bcast(rstem(lc1:lc2,istart:iend),icount,MPI_DP,      !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(pnrem(lc1:lc2,istart:iend),scrmat,icount,     !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       pnrem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)          !MPI
      endif                                                         !MPI
      call mpi_bcast(pnrem(lc1:lc2,istart:iend),icount,MPI_DP,      !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
CRD   call mpi_reduce(pnrabs(lc1:lc2,istart:iend),scrmat,icount,    !MPI
CRD  &                MPI_DP,                                       !MPI
CRD  &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
CRD   if(id_jwrk .eq. taskid) then                                  !MPI
CRD    pnrabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
CRD   endif                                                         !MPI
CRD      call mpi_bcast(pnrabs(lc1:lc2,istart:iend),icount,MPI_DP,  !MPI
CRD     &                iroot,MPI_COMM_WRKRS,ierr)                 !MPI
         enddo reduce_rates                                         !MPI
c                                                                   !MPI
c     if(iroot .eq. taskid) then                                    !MPI
c      print  '("chkrates: ",1p,15e8.1)',rabs(5,50),rem(5,50),      !MPI
c    &    rstabs(5,50),rstem(5,50),pnrem(5,50)                      !MPI
c     endif                                                         !MPI
c--
c-- we do this in chunks in order to save memory (T3E!)
c--
      reduce_rate_star: do i=0,maxtrnbc-1,chunky                    !MPI
       istart = i                                                   !MPI
       iend = min(i+chunky-1,maxtrnbc-1)                            !MPI
       icount = (lc2-lc1+1)*(iend-istart+1)                         !MPI
       itrn = iend-istart+1                                         !MPI
c      write(*,*) 'reduce_rate_star:',taskid,istart,iend,icount,itrn!MPI
      call mpi_reduce(rabsc(lc1:lc2,istart:iend),scrmat,icount,     !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rabsc(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)          !MPI
      endif                                                         !MPI
      call mpi_bcast(rabsc(lc1:lc2,istart:iend),icount,             !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
      call mpi_reduce(remc(lc1:lc2,istart:iend),scrmat,icount,      !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       remc(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)           !MPI
      endif                                                         !MPI
      call mpi_bcast(remc(lc1:lc2,istart:iend),icount,              !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
      call mpi_reduce(rstcabs(lc1:lc2,istart:iend),scrmat,icount,   !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstcabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)        !MPI
      endif                                                         !MPI
      call mpi_bcast(rstcabs(lc1:lc2,istart:iend),icount,           !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
      call mpi_reduce(rstcem(lc1:lc2,istart:iend),scrmat,icount,    !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstcem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
      endif                                                         !MPI
      call mpi_bcast(rstcem(lc1:lc2,istart:iend),icount,            !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
         enddo reduce_rate_star                                     !MPI
c                                                                   !MPI
      deallocate(scrmat)                                            !MPI
c                                                                   !MPI
c     if(docastri) then                                             !MPI
c      call stop_exit(1,"tri-diag not implemented in MPI mode")     !MPI
c     endif                                                         !MPI
c                                                                   !MPI
      return
      end
c
c
c
      subroutine phx1d_collsybwldata
c     ------------------------------
      implicit none
************************************************************************
* this routine collects all rates and R* operators from the wavelength
* clusters. It would be fairly easy if mpi_allreduce would actually
* work on all machines (Hello IBM!)... 
*
*     version 1.2 of 08/Mar/2010 by EAB and PHH 
* 1.2: changed MPI_COMM_WRKRS to scalar for simplicity (phh+eab, 09/Mar/2010)
* 1.1: fixed array out-of-bounds bug (phh, 19/Oct/2000)
* 1.0: 08/May/1997 by EAB and PHH 
************************************************************************
c                                                                   !MPI
      integer chunky
      parameter(chunky=100)
      real*8 :: DLDO                                                !MPI
      integer :: I                                                  !MPI
      integer :: ICOUNT                                             !MPI
      integer :: ID_JWLP                                            !MPI
      integer :: ID_JWRK                                            !MPI
      integer :: IEND                                               !MPI
      integer :: IERR                                               !MPI
      integer :: IROOT                                              !MPI
      integer :: ISTART                                             !MPI
      integer :: ITRN                                               !MPI
c mpi commons                                                       !MPI
      include 'phx-mpi.inc'                                         !MPI
c--                                                                 !MPI
c-- Cassandra parameter and commons:                                !MPI
c--                                                                 !MPI
      integer, parameter :: layer=64
      include 'sybcom.inc'    ! yeah yeah, modules on their way     !MPI
c                                                                   !MPI
      logical docastri                                              !MPI
      common /casoptype/ docastri                                   !MPI
c                                                                   !MPI
      real*8, allocatable :: scrmat(:,:)                            !MPI
c                                                                   !MPI
cdbg      write(0,*) lc1,lc2,chunky
cdbg      call stop_exit(1,'good')
      allocate(scrmat(lc1:lc2,chunky))                              !MPI
c--
c-- first we have to figure out our own rank in the cluster
c--
      iroot = 0                                                     !MPI
      id_jwlp = taskid/nworkers                                     !MPI
      id_jwrk = taskid - id_jwlp*nworkers                           !MPI
c     print *,"mpi debug collwldata : ",iroot,taskid                !MPI
c--
c-- now we reduce the line and contimuum rates etc from all clusters
c-- to the master cluster (zero) and then bcast them back to all
c-- members of our cluster. (to avoid bugs on mpi_allreduce!)
c-- we do this in chunks in order to save memory (T3E!)
c--
      reduce_rates: do i=0,maxsutrnb-1,chunky                       !MPI
       istart = i                                                   !MPI
       iend = min(i+chunky-1,maxsutrnb-1)                           !MPI
       icount = (lc2-lc1+1)*(iend-istart+1)                         !MPI
       itrn = iend-istart+1                                         !MPI
c      write(*,*) 'reduce_rates:',taskid,istart,iend,icount,itrn    !MPI
      call mpi_reduce(rabs(lc1:lc2,istart:iend),scrmat,icount,      !MPI
     &                MPI_DP,MPI_SUM,                               !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)           !MPI
      endif                                                         !MPI
      call mpi_bcast(rabs(lc1:lc2,istart:iend),icount,MPI_DP,       !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rem(lc1:lc2,istart:iend),scrmat,icount,       !MPI
     &                MPI_DP,MPI_SUM,                               !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)            !MPI
      endif                                                         !MPI
      call mpi_bcast(rem(lc1:lc2,istart:iend),icount,MPI_DP,        !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rstabs(lc1:lc2,istart:iend),scrmat,icount,    !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
      endif                                                         !MPI
      call mpi_bcast(rstabs(lc1:lc2,istart:iend),icount,MPI_DP,     !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(rstem(lc1:lc2,istart:iend),scrmat,icount,     !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)          !MPI
      endif                                                         !MPI
      call mpi_bcast(rstem(lc1:lc2,istart:iend),icount,MPI_DP,      !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
      call mpi_reduce(emhelp(lc1:lc2,istart:iend),scrmat,icount,    !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       emhelp(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
      endif                                                         !MPI
      call mpi_bcast(emhelp(lc1:lc2,istart:iend),icount,MPI_DP,     !MPI
     &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
c                                                                   !MPI
CRD   call mpi_reduce(abshelp(lc1:lc2,istart:iend),scrmat,icount,   !MPI
CRD  &                MPI_DP,                                       !MPI
CRD  &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
CRD   if(id_jwrk .eq. taskid) then                                  !MPI
CRD    abshelp(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)        !MPI
CRD   endif                                                         !MPI
CRD    call mpi_bcast(abshelp(lc1:lc2,istart:iend),icount,MPI_DP,   !MPI
CRD  &                iroot,MPI_COMM_WRKRS,ierr)                    !MPI
         enddo reduce_rates                                         !MPI
c                                                                   !MPI
c     if(iroot .eq. taskid) then                                    !MPI
c      print  '("chkrates: ",1p,15e8.1)',rabs(5,50),rem(5,50),      !MPI
c    &    rstabs(5,50),rstem(5,50),emhelp(5,50)                     !MPI
c     endif                                                         !MPI
c--
c-- we do this in chunks in order to save memory (T3E!)
c--
      reduce_cont_rate: do i=0,maxsutrnbc-1,chunky                  !MPI
       istart = i                                                   !MPI
       iend = min(i+chunky-1,maxsutrnbc-1)                          !MPI
       icount = (lc2-lc1+1)*(iend-istart+1)                         !MPI
       itrn = iend-istart+1                                         !MPI
       write(*,*) 'reduce_cont_rate:',taskid,istart,iend,icount,itrn!MPI
       write(*,*) 'reduce_cont_rate:',chunky,maxsutrnbc,lc1,lc2     !MPI
      call mpi_reduce(rabsc(lc1:lc2,istart:iend),scrmat,icount,     !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
cdbg       write(*,*) scrmat(lc1,1),scrmat(lc1,itrn)
cdbg       write(*,*) scrmat(lc2,1),scrmat(lc2,itrn)
      if(id_jwrk .eq. taskid) then                                  !MPI
       rabsc(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)          !MPI
      endif                                                         !MPI
      call mpi_bcast(rabsc(lc1:lc2,istart:iend),icount,             !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
cdbg        write(*,*) rabsc(lc1,istart),rabsc(lc1,iend)
cdbg        write(*,*) rabsc(lc2,istart),rabsc(lc2,iend)
c                                                                   !MPI
      call mpi_reduce(remc(lc1:lc2,istart:iend),scrmat,icount,      !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       remc(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)           !MPI
      endif                                                         !MPI
      call mpi_bcast(remc(lc1:lc2,istart:iend),icount,              !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
      call mpi_reduce(rstcabs(lc1:lc2,istart:iend),scrmat,icount,   !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstcabs(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)        !MPI
      endif                                                         !MPI
      call mpi_bcast(rstcabs(lc1:lc2,istart:iend),icount,           !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
      call mpi_reduce(rstcem(lc1:lc2,istart:iend),scrmat,icount,    !MPI
     &                MPI_DP,                                       !MPI
     &                MPI_SUM,iroot,MPI_COMM_WRKRS,ierr)            !MPI
      if(id_jwrk .eq. taskid) then                                  !MPI
       rstcem(lc1:lc2,istart:iend) = scrmat(lc1:lc2,1:itrn)         !MPI
      endif                                                         !MPI
      call mpi_bcast(rstcem(lc1:lc2,istart:iend),icount,            !MPI
     &                MPI_DP,iroot,MPI_COMM_WRKRS,ierr)             !MPI
c                                                                   !MPI
         enddo reduce_cont_rate                                     !MPI
c                                                                   !MPI
      deallocate(scrmat)                                            !MPI
c                                                                   !MPI
c     if(docastri) then                                             !MPI
c      call stop_exit(1,"tri-diag not implemented in MPI mode")     !MPI
c     endif                                                         !MPI
c                                                                   !MPI
      return
      end
c
c
c
      subroutine bicolldist
c     ---------------------
      use casscom
      implicit none
c
      include 'phx-mpi.inc'                                         !MPI
      include 'param.inc'
c
************************************************************************
* send/receive bi's computed data via MPI non-blocking messages
*                   version 1.0 of 19/mar/96 by phh+eab
*-- input:
*-- last changes:
************************************************************************
      real*8 :: DLDO                                                !MPI
      integer :: I                                                  !MPI
      integer :: ICOUNT                                             !MPI
      integer :: ID_JWLP                                            !MPI
      integer :: ID_JWRK                                            !MPI
      integer :: IEND                                               !MPI
      integer :: IERR                                               !MPI
      integer :: IROOT                                              !MPI
      integer :: ISTART                                             !MPI
      integer :: ITRN                                               !MPI
c
c--                                                                 !MPI
c-- new version:                                                    !MPI
c--                                                                 !MPI
c-- (JOB_nlte_rates) is the ABSOLUTE taskid of
c-- the master nlte rate process on the master node of the
c--  wavelength cluster (id_jwlp=0)
c--
      call mpi_bcast(bitrue,layer*maxlvl,MPI_DP,                    !MPI
     &  (JOB_nlte_rates),MyMPI_COM_CMPT,ierr)                       !MPI
      return                                                        !MPI
      end
c
c
c
      subroutine cas_lc_set(id_nlte_rates_a,n_nlte_rates_a)
c     -----------------------------------------------------
      use casscom
      implicit none
      integer id_nlte_rates_a,n_nlte_rates_a
c
      include 'phx-mpi.inc'                                         !MPI
      include 'param.inc'
c
c--
c-- Peter : I know, ypu will hate me for that :-)
c-- Sybil uses the syb_mpi common block - eventually, this will change
c-- for now, and for simplicity, I will leave this common block.
c-- However, it has to be synchronized with lc1 and lc2 out of the
c-- module.
c--
      integer lc1_4syb,lc2_4syb
      common
     &/syb_mpi/lc1_4syb,lc2_4syb
************************************************************************
* set cassandra loop limits lc1 and lc2 to parallel mode
*                   version 1.1 of 14/nov/96 by phh+eab
*-- last changes:
* 1.1: made ID and NPROCS arguments to increase flexibility
************************************************************************
       real*8 :: dldo
c
       lc1 = 1
       lc2 = layer
c
      if(id_nlte_rates_a .ne. MPI_UNDEFINED) then                   !MPI
       dldo = dble(layer)/dble(n_nlte_rates_a)                      !MPI
       lc1 = 1+floor(id_nlte_rates_a*dldo)                          !MPI
       lc2 = 1+floor((id_nlte_rates_a+1)*dldo)-1                    !MPI
       if(id_nlte_rates_a .eq. n_nlte_rates_a-1) lc2 = layer        !MPI
       print *,'cas_lc_set: rate indices: ',                        !MPI
     &         taskid,id_nlte_rates_a,lc1,lc2                       !MPI
      endif                                                         !MPI
      lc1_4syb=lc1                                                  !MPI
      lc2_4syb=lc2                                                  !MPI
c--
      write(*,*) 
      write(*,*) "cas_lc_set: lc1,lc2:",lc1,lc2
      return
c
c
      entry cas_lc_reset
c     ------------------
************************************************************************
* reset cassandra loop limits lc1 and lc2 to serial mode
*                   version 1.0 of 21/mar/96 by phh+eab
*-- last changes:
************************************************************************
c
      lc1 = 1
      lc2 = layer
      lc1_4syb=lc1
      lc2_4syb=lc2
      write(*,*)
      write(*,*) "cas_lc_reset: lc1,lc2:",lc1,lc2
      return
      end
