c
c
c
      subroutine read_atm_v_h5(irec,icache)
c     ----------------------------------------
cALLOC-ON      use s3parm                                                        !ALLOC-ON
cALLOC-ON      use useful                                                        !ALLOC-ON
cALLOC-ON      use snmod                                                         !ALLOC-ON
      use linecom, dmy=>read_atm_v_h5
      use hdftagsmod
      use iso_c_binding
      use dddrt_module, only: DD_WL_COMM_rank,FS_rank
cALLOC-ON      use opac_local, cap => cap_lte, vdpi => vdpi_atm, khi => khi_atm  !ALLOC-ON
      implicit none
      include 'param.inc'
      include 'physconst.inc'
      include 'phx-mpi.inc'                                         !MPI
      integer*8 :: irec
      integer :: icache
***********************************************************************
* does a logical read of a line buffer for atomic Voigt lines
* version 2.0 of 24/oct/13 by eab
*-input:
* irec: record number to 'read'
* iunit: unit to read from
*-output:
* icache: slot in which irec resides
* changes: added null read for matched collective hdf reads (24/oct/13 eab)
***********************************************************************
c--
c-- type declarations
c--
      integer :: localblksiz
      integer :: I,j
      integer :: IERR
      integer :: IUNIT
      integer :: KH
      integer :: NAG_MAX
      integer :: NAG_TOT
      integer :: NAV_MAX
      integer :: NAV_TOT
      integer :: NLTE_CONT_MAX
      integer :: NLTE_CONT_TOT
      integer :: NLTE_DIRTY_MAX
      integer :: NLTE_DIRTY_TOT
      integer :: NLTE_GAUSS_MAX
      integer :: NLTE_GAUSS_TOT
      integer :: NLTE_RCNT_MAX
      integer :: NLTE_RCNT_TOT
      integer :: NLTE_RLINE_MAX
      integer :: NLTE_RLINE_TOT
      integer :: NLTE_RSCNT_MAX
      integer :: NLTE_RSCNT_TOT
      integer :: NLTE_RSLINE_MAX
      integer :: NLTE_RSLINE_TOT
      integer :: NLTE_VOIGT_MAX
      integer :: NLTE_VOIGT_TOT
      integer :: NMG_MAX
      integer :: NMG_TOT
      integer :: NMV_MAX
      integer :: NMV_TOT
      real*8 :: CHIHLP
      real*8, parameter :: dlrzconst=a2cm**2/(2.d0*pi*c)
      real*4 :: GF
      real*8 :: SEKUND
      real*8 :: TC_ATM_G
      real*8 :: TC_ATM_V
      real*8 :: TC_MOL_G
      real*8 :: TC_MOL_V
      real*8 :: TS
      real*4, allocatable :: dummy(:)
      integer :: n_cache_replaced=-1
c
      logical, parameter :: small_cache=.false.
      logical :: first=.true.
      logical :: file_read
c
      integer :: iter,laus,inlte,aufg
      integer rhoexp            !ALLOC-OFF
      integer :: identyp        !ALLOC-OFF
      real*8 :: wltau,teff,rtau1,vtau1,vfold,tk,the,pg,pe,radius !ALLOC-OFF
      real*8 :: vswfac          !ALLOC-OFF
      common
     &   /flag/   aufg,iter,laus,inlte
     &   /snmod/ wltau,teff,rtau1,vtau1,vfold !ALLOC-OFF
     &   /snmod/ tk(layer),the(layer),pg(layer),pe(layer),radius(layer) !ALLOC-OFF
     &   /snmod/ rhoexp,identyp,vswfac !ALLOC-OFF
c--
c-- this block contains all the data we need for 
c-- some more or less interesting statistics...
c--
      common
     &   /phx_stat/ tc_mol_v,tc_mol_g,tc_atm_v,tc_atm_g
     &   /phx_stat/ nav_tot,nag_tot,nmv_tot,nmg_tot
     &   /phx_stat/ nav_max,nag_max,nmv_max,nmg_max
     &   /phx_stat/ nlte_dirty_tot,nlte_dirty_max
     &   /phx_stat/ nlte_voigt_tot,nlte_voigt_max
     &   /phx_stat/ nlte_gauss_tot,nlte_gauss_max
     &   /phx_stat/ nlte_cont_tot,nlte_cont_max
     &   /phx_stat/ nlte_Rcnt_tot,nlte_Rcnt_max
     &   /phx_stat/ nlte_Rscnt_tot,nlte_Rscnt_max
     &   /phx_stat/ nlte_Rline_tot,nlte_Rline_max
     &   /phx_stat/ nlte_Rsline_tot,nlte_Rsline_max
!
      INTEGER(HID_T) :: v_atm_crp_list                                  !HDF5
      INTEGER(HID_T) :: plist_id ! Dataset creation property identifier !HDF5
      INTEGER(HID_T) :: v_atm_memspace ! Memory dataspace identifier    !HDF5
      INTEGER(hsize_t) :: data_dims_in(1) ! Dataspace dimensions        !HDF5
      INTEGER(hsize_t) :: set_dims_in(1) ! set space dimensions         !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: offset                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: counts                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: stride                          !HDF5
      INTEGER(HSIZE_T), DIMENSION(1) :: blocks                          !HDF5
!
!Maximum dimensions
!
      INTEGER(HSIZE_T), DIMENSION(1) :: maxdims_in                      !HDF5
      integer :: rank_in
      INTEGER :: hdferr,info
      integer :: lastblksize
      integer :: nlines_v_atm

      type(voigt_lines),pointer,dimension(:),save ::
     &   tmp_voigt_atm_buf =>null()
      TYPE(C_PTR) :: f_ptr                                              !HDF5
      save
!--
!-- if files are empty nothing to do
!-- just return
!--
      if(lnend .eq. 0) return
!      write(0,*)"read_atm_v: bounds: ",taskid,
!     &   ubound(array=n_atm_v_block_read,dim=1)
!   
!   Setup file access property list with parallel I/O access.
!   
      file_read = .false.       ! set variable for closes
      if(v_atm_file_id .eq. -1) then                                    !HDF5
       CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdferr)            !HDF5
       if(hdferr .lt. 0) then                                           !HDF5
        write(*,*)"open fail 1",                                        !HDF5!MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid               !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail")                          !HDF5
       endif                                                            !HDF5
      info = MPI_INFO_NULL                                              !HDF5!MPI
       CALL h5pset_fapl_mpio_f(plist_id,MPI_COMM_LIN_ATM_V,info,hdferr) !HDF5!MPI
       if(hdferr .lt. 0) then                                           !HDF5
        write(*,*)"open fail 2",                                        !HDF5!MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid               !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail")                          !HDF5
       endif                                                            !HDF5

!
!read the data back
!
!Open the file.
!
       CALL h5fopen_f (h5_v_atm_file_name, H5F_ACC_RDONLY_F,            !HDF5 
     &    v_atm_file_id, hdferr,                                        !HDF5
     &    access_prp = plist_id)                                        !HDF5
       if(hdferr .lt. 0) then                    !HDF5
        write(*,*)"open fail 3",                 !HDF5         !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid       !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail")                   !HDF5
       endif                                                !HDF5
       CALL h5pclose_f(plist_id, hdferr)                                !HDF5
!   
! Create the data space for the  dataset. 
!   
!
!Open the  dataset.
!
       CALL h5dopen_f(v_atm_file_id, v_atm_dsetname,                    !HDF5
     &    v_atm_dset_id, hdferr)                                        !HDF5
       if(hdferr .lt. 0) then                               !HDF5
        write(*,*)"open fail 4",                            !HDF5!MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid   !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail")              !HDF5
       endif                                                !HDF5

!
!Get dataset's dataspace handle.
!
       CALL h5dget_space_f(v_atm_dset_id, h5_v_atm_dataspace, hdferr)   !HDF5
       if(hdferr .lt. 0) then                                 !HDF5
        write(*,*)"open fail 5",                              !HDF5!MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid     !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail")                !HDF5
       endif                                                  !HDF5

!
!Get dataspace's rank.
!
       CALL h5sget_simple_extent_ndims_f(h5_v_atm_dataspace,            !HDF5
     &    rank_in,hdferr)                                               !HDF5

!
!Get dataspace's dimensions.
! 
       CALL h5sget_simple_extent_dims_f(h5_v_atm_dataspace,             !HDF5
     &    set_dims_in, maxdims_in,                                      !HDF5
     &    hdferr)                                                       !HDF5
       nstored = set_dims_in(1)                                         !HDF5
!-- 
!-- localblksiz is implicitly set in grelcm_h5 to linmax
!-- 
       localblksiz = linmax     ! has to be for the caching algorithm   !HDF5
       lastblk = set_dims_in(1)/localblksiz                             !HDF5
       if(mod(set_dims_in(1),localblksiz) .ne. 0) then                  !HDF5
        lastblksize = mod(set_dims_in(1),localblksiz)                   !HDF5
       else                                                             !HDF5
        lastblksize = localblksiz                                       !HDF5
       endif                                                            !HDF5
      if(.not. associated(tmp_voigt_atm_buf)) then                      !HDF5
       allocate(tmp_voigt_atm_buf(linmax))                              !HDF5
      endif                                                             !HDF5
c@       write(*,*)"nstored,localblksiz,lastblk,lastblksize"
c@       write(*,*)nstored,localblksiz,lastblk,lastblksize
      endif                     ! first time through                    !HDF5
      
c--
c-- get the entry in the cache for this record:
c--
      ts = sekund()
      n_cache_atm_v_rd = n_cache_atm_v_rd+1
c--   
c-- safety checks:
c--   
      if(irec .gt. maxblock_atm_v) then
       write(*,*) "read_atm_v: error: irec .gt. maxblock_atm_v!"
       write(*,*) "irec, maxblock_atm_v=",irec, maxblock_atm_v
       call stop_exit(1,'read_atm_v: error: irec .gt. maxblock_atm_v!')
      endif    
c
      if(small_cache) then
       icache = mod(int(irec),ncache_atm_v)
      else
c--
c-- check the lookup table. If it's not found,
c-- get a new free cache or re-use an old one:
c--
       icache = block2cache_atm_v(irec)
       if(icache .lt. 0 .or. icache .ge. ncache_atm_v) then
c--
c-- not found. 
c--
        n_cache_replaced = n_cache_replaced+1
        icache = mod(n_cache_replaced,ncache_atm_v)
       endif
      endif
c
cdbg  print *,'read_atm_v : I am called'
cdbg  print *,'read_atm_v : icache',icache
c--
c-- check if the data is already in the cache
c--
      if(irec .eq. atm_v_buf(icache)) then
c-- 
c-- yes, found data. Just update the statistics.
c--
       n_atm_v_block_used(irec) = n_atm_v_block_used(irec)+1
       n_atm_v_cache_used(icache) = n_atm_v_cache_used(icache)+1
cdbg   print *,'read_atm_v : in cache'
cdbg       write(*,*) "atm_voigt found in cache:",icache,irec
cdbg       do i=0,ncache_atm_v-1
cdbg        write(*,*) voigt_atm(1,i)
cdbg       enddo
c-- 
c-- Well now we need to do a null read to have matched MPI calls
c-- So set up the null read
c--
!
!Get creation property list.
!
       CALL h5dget_create_plist_f(v_atm_dset_id,v_atm_crp_list,hdferr)  !HDF5
!
!create memory dataspace
!
       data_dims_in(1) = localblksiz                                    !HDF5
       CALL h5screate_simple_f(rank_in, data_dims_in,                   !HDF5
     &    v_atm_memspace, hdferr)                                       !HDF5
       offset(1) = 0 ! This is in elements of the array!HDF5
       counts(1) = 0            ! This is in blocks                     !HDF5
       stride(1) = localblksiz  ! This is in elements                   !HDF5
       blocks(1) = localblksiz                                          !HDF5
       call h5Sselect_none_f(v_atm_memspace,hdferr)                 !HDF5
       write(7,*)"null read"

       CALL h5sselect_hyperslab_f(h5_v_atm_dataspace, H5S_SELECT_SET_F, !HDF5
     &    offset, counts, hdferr,stride,blocks)                         !HDF5

!
!   Create property list for collective dataset read
!   
       CALL h5pcreate_f(H5P_DATASET_XFER_F,v_atm_crp_list,hdferr)       !HDF5
       CALL h5pset_dxpl_mpio_f(v_atm_crp_list,                          !HDF5!MPI
     &    H5FD_MPIO_COLLECTIVE_F,hdferr)                                !HDF5!MPI

!
!Read data 
!
       f_ptr = C_LOC(tmp_voigt_atm_buf(1))                               !HDF5
       CALL H5dread_f(v_atm_dset_id, v_atm_tid, f_ptr, hdferr,          !HDF5
     &    mem_space_id=v_atm_memspace,                                  !HDF5
     &    file_space_id=h5_v_atm_dataspace,                             !HDF5
     &    xfer_prp=v_atm_crp_list)                                      !HDF5
       file_read = .true.
      else
       write(7,*)"non-null read"
c-- 
c-- nope, not there. will need to get it from
c-- the disk.
c-- now get the new block from disk:
c--
!
!Get creation property list.
!
       CALL h5dget_create_plist_f(v_atm_dset_id,v_atm_crp_list,hdferr)  !HDF5
       if(hdferr .lt. 0) then         !HDF5
        write(*,*)"open fail 6",     !HDF5                     !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail") !HDF5
       endif !HDF5
!
!create memory dataspace
!
       data_dims_in(1) = localblksiz                                    !HDF5
       CALL h5screate_simple_f(rank_in, data_dims_in,                   !HDF5
     &    v_atm_memspace, hdferr)                                       !HDF5
       if(hdferr .lt. 0) then !HDF5
        write(*,*)"open fail 7",  !HDF5                        !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
        call stop_exit(1,"read_atm open_v_h5 fail") !HDF5
       endif   !HDF5
       offset(1) = (irec-1)*localblksiz ! This is in elements of the array!HDF5
       counts(1) = 1            ! This is in blocks                     !HDF5
       stride(1) = localblksiz  ! This is in elements                   !HDF5
       blocks(1) = localblksiz                                          !HDF5
       if(offset(1) .gt. set_dims_in(1))                                !HDF5
     &    call stop_exit(1,'read_atm_v_h5 err')                             !HDF5
       if(irec-1 == lastblk-1 .and. lastblksize .ne. localblksiz) then  !HDF5
        blocks(1) = lastblksize                                         !HDF5
       endif                                                            !HDF5
       CALL h5sselect_hyperslab_f(h5_v_atm_dataspace, H5S_SELECT_SET_F, !HDF5
     &    offset, counts, hdferr,stride,blocks)                         !HDF5
       if(hdferr .lt. 0) then     !HDF5
        write(*,*)"open fail 8", !HDF5                         !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail") !HDF5
       endif !HDF5

!
!   Create property list for collective dataset read
!   
       CALL h5pcreate_f(H5P_DATASET_XFER_F,v_atm_crp_list,hdferr)       !HDF5
       CALL h5pset_dxpl_mpio_f(v_atm_crp_list,                          !HDF5!MPI
     &    H5FD_MPIO_COLLECTIVE_F,hdferr)                                !HDF5!MPI
       if(hdferr .lt. 0) then !HDF5
        write(*,*)"open fail 9", !HDF5                         !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
        call stop_exit(1,"read_atm_v_h5 open fail") !HDF5
       endif !HDF5

!
!Read data 
!
       f_ptr = C_LOC(voigt_atm(1,icache))                               !HDF5
       CALL H5dread_f(v_atm_dset_id, v_atm_tid, f_ptr, hdferr,          !HDF5
     &    mem_space_id=v_atm_memspace,                                  !HDF5
     &    file_space_id=h5_v_atm_dataspace,                             !HDF5
     &    xfer_prp=v_atm_crp_list)                                      !HDF5
       if(hdferr .lt. 0) then !HDF5
        write(*,*)"open fail 10", !HDF5                        !MPI
     &     h5_v_atm_file_name,v_atm_file_id,hdferr,taskid !HDF5!MPI
        call h5tclose_f(v_atm_tid,hdferr)
        CALL h5sclose_f(h5_v_atm_dataspace,hdferr) !HDF5!MPI
        CALL h5sclose_f(v_atm_memspace,hdferr) !HDF5!MPI
        CALL h5pclose_f(v_atm_crp_list,hdferr) !HDF5
        CALL h5dclose_f(v_atm_dset_id,hdferr) !HDF5!MPI
        call h5fclose_f(v_atm_file_id,hdferr)
        call stop_exit(1,"read_atm_v_h5 open fail") !HDF5
       endif                    !HDF5
       file_read = .true.
cdbg        write(*,*) "atm_voigt read in cache:",icache,irec
cdbg        do i=0,ncache_atm_v-1
cdbg         write(*,*) voigt_atm(1,i)
cdbg        enddo
       n_blk_atm_v_rd = n_blk_atm_v_rd+1
       n_atm_v_block_read(irec) = n_atm_v_block_read(irec)+1
       n_atm_v_block_used(irec) = n_atm_v_block_used(irec)+1
       n_atm_v_block_used(irec) = n_atm_v_block_used(irec)+1
       n_atm_v_cache_read(icache) = n_atm_v_cache_read(icache)+1
       n_atm_v_cache_used(icache) = n_atm_v_cache_used(icache)+1
c--
c-- fill the lookup table:
c--
       block2cache_atm_v(irec) = icache
c-- 
c-- let's save it for later:
c--
       atm_v_buf(icache) = irec
c--
c-- now precompute data for use in the ltelinbvoigt:
c--
       if(prefac_cache_mode .eq. 1) then
c--
c-- this mode does the caching here:
c--
c--
        if(.not. allocated(dummy)) then
         allocate(dummy(layer))
        endif   
c
c$omp parallel do default(shared)
        do i=1,linmax
c--
c-- local scalars
c--
         gf = sngl(-voigt_atm(i,icache)%gfex)
         chihlp = voigt_atm(i,icache)%chiel
         kh = voigt_atm(i,icache)%intl
cold         voigt_atm_prefac(:,i,icache) =
cold     &    (exp(the(:)*chihlp)*gf)*zethlp(:,kh)
c
         call svexp(sngl(the(:)*chihlp),dummy,layer)
         do j=1,layer
          voigt_atm_prefac(j,i,icache) =
     &       gf*dummy(j)*sngl(zethlp(j,kh))
         enddo 
         atm_v_prefac_valid(:,icache) = .true.
        enddo 
c$omp end parallel do
c
       elseif(prefac_cache_mode .eq. 2) then
c--
c-- invalidate the cache data
c--
        atm_v_prefac_valid(:,icache) = .false.
c
       elseif(prefac_cache_mode .eq. 0) then
c--
c--  do nothing
c--
        continue
       else
        call stop_exit(1,
     &     'read_atm_v: illegal value of prefac_cache_mode')
       endif
      endif
c--
c-- that's it.
c--
      tc_atm_v = sekund()-ts+tc_atm_v
      if(file_read) then
       CALL h5sclose_f(v_atm_memspace,hdferr)                           !HDF5
       CALL h5pclose_f(v_atm_crp_list,hdferr)                           !HDF5
      endif
      return
      end subroutine read_atm_v_h5
