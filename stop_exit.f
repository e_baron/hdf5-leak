c
c
c
      subroutine stop_exit(status,message)
c     ------------------------------------
      use hdf5 !HDF5
      use linecom
      use dddrt_module
      implicit none
      include 'phx-mpi.inc'
c
      integer status
      character*(*) message
************************************************************************
*  this subroutine is used to print a message to stderr and 
*  stop the code with a return value, if possible.
*        version 1.0 of 20/may/94 by eab
*-- notes:
*  this routine is machine dependent (call to exit-routine only)
*  and this version is by far not complete ...
* exit() is for IBM RS/6000
************************************************************************
c--
c-- do the IO for the molecular line lists in C, faster on some machines
c--
      logical, parameter :: CIO=.false.
c--
c-- local files for parallel mode on machines
c-- that support it. Presently only for molecular 
c-- lines
c--
      logical local,totally_local
      common /scratch_file_setup/ local,totally_local
c
      character*255 tmpdir
      common /scratch_dir/ tmpdir
c
      integer ios
      integer open_scratch,close_scratch,ierr
      character*255 filename,string
c
cOCL      call shutdown_RT3DCL !OCL
c
      if(CIO) then
!--
!-- these close_scratch commands hang on some
!-- machines (only with multiple nodes) when the files are not already open
!-- There is no easy way to test for the files being open in C
!-- fstat wants FileDescriptor variables which are of type int (the fdes in cio.c)
!-- Whereas these variables are Streams which are of type FILE (the fd in cio.c)
!-- so we'll just comment these lines out
!-- another fix would be to just comment out the lgaussunitm line
!-- 
!       ierr = close_scratch(lineunit)
!       ierr = close_scratch(lineunitm)
!       ierr = close_scratch(lgaussunit)
!       ierr = close_scratch(lgaussunitm)
!       ierr = close_scratch(lineunitf)
c
       if(local) then
        write(string,'(a1,i9.9)') '.',taskid
       else
        string = ' '
        tmpdir = '.'
       endif
c
       write(*,*) 'stop_exit: deleting files. tmpdir=>',
     &  trim(tmpdir),'<, string=>',trim(string),'<'
c
       if(local .or. taskid .eq. 0) then
        filename = trim(tmpdir)//'/fort.3'//trim(string)
        open(3,file=trim(filename),status='unknown')
        close(3,status='delete',iostat=ios)
        filename = trim(tmpdir)//'/fort.31'//trim(string)
        open(3,file=trim(filename),status='unknown')
        close(3,status='delete',iostat=ios)
        filename = trim(tmpdir)//'/fort.32'//trim(string)//char(0)
        open(3,file=trim(filename),status='unknown')
        close(3,status='delete',iostat=ios)
        filename = trim(tmpdir)//'/fort.33'//trim(string)
        open(3,file=trim(filename),status='unknown')
        close(3,status='delete',iostat=ios)
        filename = trim(tmpdir)//'/fort.35'//trim(string)//char(0)
        open(3,file=trim(filename),status='unknown')
        close(3,status='delete',iostat=ios)
       endif
      else if (.not. use_hdf5) then
       close(03,status='delete',iostat=ios)
       close(31,status='delete',iostat=ios)
!       close(32,status='delete',iostat=ios)
       close(33,status='delete',iostat=ios)
       close(35,status='delete',iostat=ios)
      endif
c
      CALL h5close_f(ierr)                                       !HDF5
      write(0,'(1x,i12,":",a)') taskid_world,message
      write(6,'(1x,a)')message
      if(status .eq. 0) then
c--
c-- check from 3D mode and use_IOnodes to send
c-- termination command to I/O nodes.
c--
        if(use_IOnodes .and. IO_comm_rank .ne. 0) then 
          call MPI_SEND(IO_CMD_END,1,MPI_INTEGER,0,
     &                  IO_COMMAND,MyMPI_COMM_IO,ierr)
        endif
        call mpi_barrier(MPI_COMM_WORLD, ierr)
        call mpi_finalize(ierr)
       else
        call mpi_abort(MPI_COMM_WORLD,status,ierr)
       endif
       stop 'MPI done!'
      end
