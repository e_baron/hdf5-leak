********************************************************************
* 
*
      include 'sybcomgen.inc'
*
*
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   c-- Description
c moved to sybcomgen.inc c   c------------------------------------------------------------------
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   c  lvpmol	: maximum number of levels per molecule
c moved to sybcomgen.inc c   c  maxsulvl 	: total maximum number of Super levels
c moved to sybcomgen.inc c   c  maxmol	: total maximum number of Molecules
c moved to sybcomgen.inc c   c  maxsutrn 	: total maximum number of Super Transitions
c moved to sybcomgen.inc c   c  
c moved to sybcomgen.inc c   c
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   c-- parameter and common for sybil.
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   c-- 1. parameter:
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c         integer lvpmol,maxsulvl,maxmol,maxsutrn,maxsustr
c moved to sybcomgen.inc c         integer maxsugrp,maxlpsug,maxalvls,maxsutrnb
c moved to sybcomgen.inc c         integer maxsutrnbc
c moved to sybcomgen.inc c   c
c moved to sybcomgen.inc c   c WARNING WARNING WARNING
c moved to sybcomgen.inc c   c maxsulvl is HARD CODED INTO PHOENIX
c moved to sybcomgen.inc c   c if you change maxsulvl here, PLEASE do it also in phoenix.f
c moved to sybcomgen.inc c   c WARNING WARNING WARNING
c moved to sybcomgen.inc c   c
c moved to sybcomgen.inc c         parameter(lvpmol=100,maxsulvl=200,maxmol=5,maxsutrn=300)
c moved to sybcomgen.inc c   caddnlte maxmol --> insert code for new species before this line
c moved to sybcomgen.inc c         parameter(maxsustr=300,maxsugrp=3,maxlpsug=630)
c moved to sybcomgen.inc c         parameter(maxalvls=5000)
c moved to sybcomgen.inc c   c4direct      parameter(lvpmol=3700,maxsulvl=3700,maxmol=2,maxsutrn=23000)
c moved to sybcomgen.inc c   c4direct      parameter(maxsustr=23000,maxsugrp=3,maxlpsug=3700)
c moved to sybcomgen.inc c   c4direct      parameter(maxalvls=3800)
c     -- maximum number of levels for tri-diagonal solver
c     --   (will use diagonal if greater)
CHECK !!!!!!!!
      integer maxtri
      parameter(maxtri=40)
c moved to sybcomgen.inc c   c
c moved to sybcomgen.inc c   c--- WARNING!! Different block sizes for lines and continua
c moved to sybcomgen.inc c   c IS implemented--- In order to implement it the rate and rstar
c moved to sybcomgen.inc c   c arrays must distinguish between lines and continua
c moved to sybcomgen.inc c   c
c moved to sybcomgen.inc c   c the arrays run from 0:maxsutrnb so that the pointer to the
c moved to sybcomgen.inc c   c current position within a block is simply given by
c moved to sybcomgen.inc c   c mod(i,maxsutrnb)
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   c-- maxsutrnb: blocksize for lines
c moved to sybcomgen.inc c   c-- maxsutrnbc: blocksize for continua
c moved to sybcomgen.inc c   c--
c moved to sybcomgen.inc c   CSMALL      parameter (maxsutrnb=1000,maxsutrnbc=maxsulvl)
c moved to sybcomgen.inc c         parameter (maxsutrnb=maxsustr,maxsutrnbc=maxsulvl)
c moved to sybcomgen.inc c   c--
c-- the sizes of the various caches for Sybil
c-- experimentd with them for optimium performance.
c-- we recommend that they are just big enough so that
c-- every IO operation is done only once during the 
c-- wavelength integration, e.g.:
c--   parameter(ncache_prf=2,ncache_pnr=2,ncache_rl=2,ncache_rc=2)
c--   parameter(ncache_rsl=2,ncache_rsc=2)
c-- ATTENTION : THESE ARE THE SAME NAMES AS IN CASSCOM.INC !!
c-- BUT HAVE DIFFERENT CONTENTS (GENERALLY).
c-- casscom.inc and sybcom.inc MUST NOT be included in the same
c-- routine anyway !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c--
      integer ncache_prf,ncache_pnr,ncache_rl,ncache_rc
      integer ncache_rsl,ncache_rsc
c
CSMALL      parameter(ncache_prf=3,ncache_pnr=3,ncache_rl=3,ncache_rc=1)
CSMALL      parameter(ncache_rsl=3,ncache_rsc=1)
      parameter(ncache_prf=1,ncache_pnr=1,ncache_rl=1,ncache_rc=1)
      parameter(ncache_rsl=1,ncache_rsc=1)
c--
c-- definition and blocks for the caching alg
c-- NOTE: if maxsutrnbc = maxsulvl, you can reduce the mem. req. 
c-- by a HUGE factor by using the uncommented parameter
c-- definition below. If maxsutrnbc < maxsulvl, you MUST use the 
c-- standard definition, otherwise ... disaster in progress!
c--
c-- REMARK these names should normally start with syb... instead
c-- of cas..., but to use the same routines, I leave it.
c-- casscom.inc and sybcom.inc MUST NOT be included in the same
c-- routine anyway !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c--
      integer caslinlen,cascntlen
cstd  parameter(caslinlen=maxsutrnb*layer,cascntlen=maxsutrnbc*layer)
CSMALL      parameter(caslinlen=maxsutrnb*layer,cascntlen=1)
      parameter(caslinlen=1,cascntlen=1)
c--
c-- THIS COMMON BLOCK WILL TRUELY BE THE SAME AS THE CASSANDRA 
c-- COMMON BLOCK. THIS MAKES COMPLETE SENSE AS LONG AS THE CASSANDRA
c-- AND THE SYBIL ROUTINES RUN AS THE SAME TASK !
c--
c-- MPI ranges of optical depth loops 
c--
      integer lc1,lc2
c     parameter(lc1=1,lc2=layer) !DEFAULT
c
      common                                                        !MPI
     &/syb_mpi/lc1,lc2                                              !MPI
c--
c-- 2. common
c--
c-- block with occupation numbers and departure coeff's
c--
      real*8 octilt,octinv,ocinv,btilde,ocold,btnew,bitrue,bitrue_inv
      common 
     &/sudepdat/ octilt(layer,maxsulvl),octinv(layer,maxsulvl)
     &/sudepdat/ ocinv(layer,maxsulvl),btilde(layer,maxsulvl)
     &/sudepdat/ ocold(layer,maxsulvl),btnew(layer,maxsulvl)
     &/sudepdat/ bitrue(layer,maxsulvl),bitrue_inv(layer,maxsulvl)
c
c-- flags etc for nlte:
      integer iswtch,nsave,ichan,iapp,nlteh
      real*8 hok
      common 
     &/param / hok,iswtch,nsave,ichan,iapp,nlteh
c--
c-- nlte data commons used here:
c--
c     real*8 gi,energy,wltrn,bij,wltrn_inv,gi_inv
c--
c-- bfxsection MAY HAVE TO BE BUFFERED !
c-- it stores the bf cross section. First time it is calculated
c-- in sybcnt but it will be used in sybprerates all the time 
c-- it is too much time consuming
c-- to call the function all the time
c--
      real*8 bfxsection
      real*8 eion
      common
     &/molecules/ eion(maxmol)
     &/molecules/ bfxsection(layer,maxsulvl)
c    &/molecules/ gi(maxsulvl),energy(maxsulvl)
c    &/molecules/ wltrn(maxsutrn),bij(maxsutrn)
c    &/molecules/ wltrn_inv(maxsutrn),gi_inv(maxsulvl)
c
      real*8 gias,energyas
      integer offsetas,nalevels,maxlvldiff
      common
     &/asuper/ gias(maxalvls),energyas(maxalvls)
     &/asuper/ offsetas(maxmol)
     &/asuper/ nalevels,maxlvldiff
c
c-- some memory consuming arrays :
c
      integer aclvl2sulvl,sulvl2suline
      common
     &/sybtrans/ aclvl2sulvl(maxalvls)
     &/sybtrans/ sulvl2suline(maxsulvl,maxsulvl)
c
      logical lcont,istrns
      integer itrans,lvlid,ioncod,lvlnr,idgrp,idmat,lvpgrp
      integer nions,nlevel,ntrans,ngroup,lastcont,lastcntcoup
      integer startlvl,endlvl,mp2lvl
      common
     &/suadmin/ itrans(maxsutrn,6)
     &/suadmin/ ioncod(maxmol,2),lvlnr(lvpmol,maxmol),lvlid(maxsulvl,4)
     &/suadmin/ startlvl(maxsulvl),endlvl(maxsulvl)
     &/suadmin/ idgrp(maxsulvl),idmat(maxsulvl),lvpgrp(maxsugrp)
     &/suadmin/ mp2lvl(maxlpsug,maxsugrp)
     &/suadmin/ nions,nlevel,ntrans,ngroup,lcont(maxsutrn)
     &/suadmin/ lastcont,lastcntcoup
c moved to sybcomgen.inc c   c 
c moved to sybcomgen.inc c         integer idcosu,idcoplussu
c moved to sybcomgen.inc c         common
c moved to sybcomgen.inc c        &/idsuspec/ idcosu
c moved to sybcomgen.inc c        &/idsuspec/ idcoplussu
c moved to sybcomgen.inc c   ciscaddnlte idspec --> insert code for new species before this line
c moved to sybcomgen.inc c   c 
c     character cterm*25
c     common
c    &/cmolecules/ cterm(maxsulvl,2)
c
      real*8 rabs,rem
      real*8 rabsc,remc
      common
     &/surates/ rabs(layer,0:maxsutrnb-1),rem(layer,0:maxsutrnb-1)
     &/surates/ rabsc(layer,0:maxsutrnbc-1),remc(layer,0:maxsutrnbc-1)
c 
      integer idrstr,ncoupl
      real*8 rstabs,rstem,rstema,rstemc,rstaba,rstabc
      real*8 rstcabs,rstcem,rstcema,rstcemc,rstcaba,rstcabc
      common
     &/suratstr/ rstabs(layer,0:maxsutrnb-1),rstaba(layer,0:maxsutrnb-1)
     &/suratstr/ rstem(layer,0:maxsutrnb-1),rstema(layer,0:maxsutrnb-1)
     &/suratstr/ rstemc(layer,0:maxsutrnb-1),rstabc(layer,0:maxsutrnb-1)
     &/suratstr/ rstcabs(layer,0:maxsutrnbc-1)
     &/suratstr/ rstcaba(layer,0:maxsutrnbc-1)
     &/suratstr/ rstcem(layer,0:maxsutrnbc-1)
     &/suratstr/ rstcema(layer,0:maxsutrnbc-1)
     &/suratstr/ rstcemc(layer,0:maxsutrnbc-1)
     &/suratstr/ rstcabc(layer,0:maxsutrnbc-1)
     &/suratstr/ idrstr(maxsustr,2),ncoupl
c
      real*8 cjbar
      common
     &/sybutl/ cjbar(layer,0:maxsutrnb-1)
c
c-- in Cassandra emhelp is called pnrem (and abshelp is called pnabs). 
c-- However : we CANNOT calculate
c-- a norm anyway and also store only something similar in it.
c-- also we will abuse it for storing the continuum cross section
c-- So, it really is only a help array
c--
      real*8 ocnlte,ocstar,prfabs,abshelp,prfem,emhelp,suzi_inv
c     real*8 suzi
      common
     &/sunltdat/ ocnlte(layer,maxsulvl),ocstar(layer,maxsulvl)
     &/sunltdat/ prfem(layer,0:maxsutrnb-1),emhelp(layer,0:maxsutrnb-1)
c    &/sunltdat/ suzi(layer,maxsulvl)
     &/sunltdat/ suzi_inv(layer,maxsulvl)
CRD  &/sunltdat/ prfabs(layer,0:maxsutrnb-1),abshelp(layer,0:maxsutrnb-1)
c
      real*8 etaveksyb
      common
     &/etanltsyb/ etaveksyb(layer,maxsulvl)
c--
c-- search windows for optimized version of Cassandra:
c-- icnwins: start of continuum search window (initially 1)
c-- ilwins: start of line search window (initially lastcont+1)
c-- ilwine: end of line search window (initially lastcont+1)
c-- iccwins,iclwins,iclwine: same but for coupled transitions
c--
      integer icwins,ilwins,ilwine,iccwins,iclwins,iclwine
      integer lpfunit,lpnunit,lrcunit,lrlunit,lrscunit,lrslunit
      integer lpfcurblk,lpncurblk,lrccurblk,lrlcurblk,lrsccurblk
      integer lrslcurblk,lrpunit,lrpcurblk
      common
     &/sybsearch/ icwins,ilwins,ilwine,iccwins,iclwins,iclwine
     &/sybblk/ lpfunit,lpnunit,lrcunit,lrlunit,lrscunit,lrslunit
     &/sybblk/ lpfcurblk,lpncurblk,lrccurblk
     &/sybblk/ lrlcurblk,lrsccurblk,lrslcurblk
     &/sybblk/ lrpunit
     &/sybblk/ lrpcurblk
c--
c-- set level id array for fuzz lines
c-- idfuzz(2,maxsulvl) 1-species code (as in cassandra)
c--                  2-level id code as read in by fuzzb
c--
      integer idfuzz
      common /sybfuzz/idfuzz(2,maxsulvl)
c--
c-- threshold temperature below which the departure
c-- cofficients will be frozen, used to prevent low
c-- temperature disasters (e.g., He II at 500K).
c--
      real*8 caststop
      common /sybtemps/caststop(maxsugrp)
c
      logical lpf_dirty(0:ncache_prf-1)
      logical lpn_dirty(0:ncache_pnr-1)
      logical lrl_dirty(0:ncache_rl-1)
      logical lrc_dirty(0:ncache_rc-1)
      logical lrsl_dirty(0:ncache_rsl-1)
      logical lrsc_dirty(0:ncache_rsc-1)
      integer lpf_buf(0:ncache_prf-1)
      integer lpn_buf(0:ncache_pnr-1)
      integer lrl_buf(0:ncache_rl-1)
      integer lrc_buf(0:ncache_rc-1)
      integer lrsl_buf(0:ncache_rsl-1)
      integer lrsc_buf(0:ncache_rsc-1)
      real*8 prfem_buf(caslinlen,0:ncache_prf-1)
CRD   real*8 prfabs_buf(caslinlen,0:ncache_prf-1)
      real*8 emhelp_buf(caslinlen,0:ncache_pnr-1)
CRD   real*8 abshelp_buf(caslinlen,0:ncache_pnr-1)
      real*8 rem_buf(caslinlen,0:ncache_rl-1)
      real*8 rabs_buf(caslinlen,0:ncache_rl-1)
      real*8 rstem_buf(caslinlen,0:ncache_rsl-1)
      real*8 rstema_buf(caslinlen,0:ncache_rsl-1)
      real*8 rstemc_buf(caslinlen,0:ncache_rsl-1)
      real*8 rstabs_buf(caslinlen,0:ncache_rsl-1)
      real*8 rstaba_buf(caslinlen,0:ncache_rsl-1)
      real*8 rstabc_buf(caslinlen,0:ncache_rsl-1)
c
c-- continuum arrays
c
      real*8 remc_buf(cascntlen,0:ncache_rc-1)
      real*8 rabsc_buf(cascntlen,0:ncache_rc-1)
      real*8 rstcem_buf(cascntlen,0:ncache_rsc-1)
      real*8 rstcema_buf(cascntlen,0:ncache_rsc-1)
      real*8 rstcemc_buf(cascntlen,0:ncache_rsc-1)
      real*8 rstcabs_buf(cascntlen,0:ncache_rsc-1)
      real*8 rstcaba_buf(cascntlen,0:ncache_rsc-1)
      real*8 rstcabc_buf(cascntlen,0:ncache_rsc-1)
c--
      common 
CRD  &/syb_cache/ prfem_buf,prfabs_buf,emhelp_buf,abshelp_buf
     &/syb_cache/ prfem_buf,emhelp_buf
     &/syb_cache/ rem_buf,rabs_buf,rstem_buf,rstema_buf,rstemc_buf
     &/syb_cache/ rstabs_buf,rstaba_buf,rstabc_buf
     &/syb_cache/ remc_buf,rabsc_buf,rstcem_buf,rstcema_buf,rstcemc_buf
     &/syb_cache/ rstcabs_buf,rstcaba_buf,rstcabc_buf
c
      common
     &/syb_cache1/ lpf_buf,lpn_buf,lrl_buf,lrc_buf,lrsl_buf,lrsc_buf
     &/syb_cache1/ lpf_dirty,lpn_dirty,lrl_dirty,lrc_dirty,lrsl_dirty
     &/syb_cache1/ lrsc_dirty


c
c This is from profiles.inc - let's see if it crashes ...

      logical doprf
      common
     &/insert_suline/ doprf(maxsutrn)

c--
c-- The following is a modified part of linecom.inc :
c--
c-- I put it back in linecom.inc, because of some parameter-mismatches
c--
c-- some debug stuff can be switched on and off with that
c
      logical sybildebug
      parameter(sybildebug=.false.)
