c--
c-- Description
c------------------------------------------------------------------
c--
c  lvpmol	: maximum number of levels per molecule
c  maxsulvl 	: total maximum number of Super levels
c  maxmol	: total maximum number of Molecules
c  maxsutrn 	: total maximum number of Super Transitions
c  
c
c--
c-- parameter and common for sybil.
c--
c-- 1. parameter:
c--
      integer lvpmol,maxsulvl,maxmol,maxsutrn,maxsustr
      integer maxsugrp,maxlpsug,maxalvls,maxsutrnb
      integer maxsutrnbc
c
c WARNING WARNING WARNING
c maxsulvl is HARD CODED INTO PHOENIX
c if you change maxsulvl here, PLEASE do it also in phoenix.f
c WARNING WARNING WARNING
c
      parameter(lvpmol=100,maxsulvl=200,maxmol=5,maxsutrn=300)
caddnlte maxmol --> insert code for new species before this line
      parameter(maxsustr=300,maxsugrp=3,maxlpsug=630)
      parameter(maxalvls=5000)
c4direct      parameter(lvpmol=3700,maxsulvl=3700,maxmol=2,maxsutrn=23000)
c4direct      parameter(maxsustr=23000,maxsugrp=3,maxlpsug=3700)
c4direct      parameter(maxalvls=3800)
c     -- maximum number of levels for tri-diagonal solver
c
c--- WARNING!! Different block sizes for lines and continua
c IS implemented--- In order to implement it the rate and rstar
c arrays must distinguish between lines and continua
c
c the arrays run from 0:maxsutrnb so that the pointer to the
c current position within a block is simply given by
c mod(i,maxsutrnb)
c--
c-- maxsutrnb: blocksize for lines
c-- maxsutrnbc: blocksize for continua
c--
CSMALL      parameter (maxsutrnb=1000,maxsutrnbc=maxsulvl)
      parameter (maxsutrnb=maxsustr,maxsutrnbc=maxsulvl)
c--
c 
      integer idcosu,idcoplussu
      common
     &/idsuspec/ idcosu
     &/idsuspec/ idcoplussu
ciscaddnlte idspec --> insert code for new species before this line
c 
c
c-- CO super commons
c
      integer nlcosu,llcosu,clcosu
c4direct      parameter(nlcosu=4000) 
c4direct      parameter(llcosu=19300)              ! b-b radiative arrays
      parameter(nlcosu=50) 
      parameter(llcosu=200)              ! b-b radiative arrays
      parameter(clcosu=nlcosu*(nlcosu-1)/2) ! collisional arrays
c
c-- common with CO molecular data
c     integer cosuind,cosulevid,cosue,cosuo
c     integer cosustart,cosuend
c     real*8 fe2wl,fe2gi,fe2lvl,
c     real*8 cosuion
c     real*8 cosuzi
c     real*8 cosuobb,cosuobf
      integer ncosulvls,ncosulnes
      common
c    &/cosudat/ fe2bij(llfe2)
c    &/cosudat/ fe2wl(llfe2),fe2fij(llfe2)
c    &/cosudat/ fe2gi(nlfe2),fe2lvl(nlfe2),fe2ion
c    &/cosudat/ cosuobb(clcosu),cosuobf(nlcosu)
c    &/cosudat/ icosubf(nlcosu)
cccSU Warning : The saving of the "partition function" of every
cccSU           single superlevel MIGHT be a lot memery consuming
cccSU           Change this to the call of the function if too many
cccSU           Superlevels will get used !
c    &/cosudat/ cosuzi(layer,nlcosu)
c    &/cosudat/ cosuind(nlcosu)
c    &/cosudat/ cosulevid(maxcosu),cosue(llcosu),cosuo(llcosu)
c    &/cosudat/ cosuion
     &/cosudat/ ncosulvls
     &/cosudat/ ncosulnes
c    &/cosudat/ cosustart(nlcosu),cosuend(nlcosu)
c
c-- common with CO b-f photo cross-sections
c     common /cosucross/ fe2pi(nlfe2)
c
c-- common  with CO level label
c     character*10 cosulab
c     common /cosulabl/ cosulab(nlcosu)
c
c-- common with CO results
c     real*8 cosuocs,cosubi
c     common
c    &/cosures/ cosuocs(layer,nlcosu),cosubi(layer,nlcosu)
c--
      integer ncosu
      common
     &/flgmolnlt/ ncosu
      real*8 bicosu
      common
     &/bimolnlte/ bicosu(nlcosu)
